package next.fire.boot.swift.seata.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by daibing on 2021/7/22.
 */
@ConfigurationProperties(prefix = "swift.seata")
public class SeataProperties {
    private String[] matchPatterns = new String[]{"*.*"};

    public String[] getMatchPatterns() {
        return matchPatterns;
    }

    public void setMatchPatterns(String[] matchPatterns) {
        this.matchPatterns = matchPatterns;
    }
}
