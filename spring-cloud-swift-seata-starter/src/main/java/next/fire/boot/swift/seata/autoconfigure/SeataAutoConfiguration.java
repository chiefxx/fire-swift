package next.fire.boot.swift.seata.autoconfigure;

import next.fire.boot.swift.seata.openx.server.SeataHandlerInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by daibing on 2021/7/22.
 */
@Configuration
@EnableConfigurationProperties(SeataProperties.class)
@ConditionalOnClass({SeataProperties.class})
public class SeataAutoConfiguration {
    private final SeataProperties properties;

    public SeataAutoConfiguration(SeataProperties properties) {
        this.properties = properties;
    }

    @Bean(name = "seataHandlerInterceptor")
    public SeataHandlerInterceptor seataHandlerInterceptor() {
        List<String> matchPatternList = Arrays.asList(properties.getMatchPatterns());
        return new SeataHandlerInterceptor(matchPatternList);
    }

}
