package next.fire.boot.swift.seata.openx.client;

import com.google.common.base.Strings;
import com.talkyun.openx.RestfulHeaderManager;
import io.seata.core.context.RootContext;
import next.fire.boot.swift.openx.client.interceptor.RequestInterceptor;

/**
 * Created by daibing on 2021/7/22.
 */
public class SeataRequestInterceptor implements RequestInterceptor {
    private static final String CONTENT_TYPE = "Content-Type";

    @Override
    public void apply(RestfulHeaderManager instance) {
        if (Strings.isNullOrEmpty(instance.getOnceHead().get(CONTENT_TYPE))) {
            instance.addOnceHead(CONTENT_TYPE, "application/json; charset=utf-8");
        }
        String xid = RootContext.getXID();
        if (!Strings.isNullOrEmpty(xid)) {
            instance.addOnceHead(RootContext.KEY_XID, xid);
        }
    }
}
