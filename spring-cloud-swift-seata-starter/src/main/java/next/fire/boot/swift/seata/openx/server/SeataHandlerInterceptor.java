package next.fire.boot.swift.seata.openx.server;

import com.google.common.base.Strings;
import com.talkyun.openx.RestfulHeaderManager;
import com.talkyun.openx.server.core.*;
import io.seata.core.context.RootContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by daibing on 2021/7/22.
 */
public class SeataHandlerInterceptor extends AbstractInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SeataHandlerInterceptor.class);

    public SeataHandlerInterceptor(List<String> matchPatterns) {
        super.setMatchPattern(matchPatterns);
    }

    /**
     * 业务逻辑拷贝spring-cloud-starter-alibaba-seata的SeataHandlerInterceptor
     */

    @Override
    protected void doMessageIn(ServiceRequest sr) throws Throwable {
        String xid = RootContext.getXID();
        String rpcXid = RestfulHeaderManager.get().getOnceHead().get(RootContext.KEY_XID.toLowerCase());
        if (Strings.isNullOrEmpty(xid) && !Strings.isNullOrEmpty(rpcXid)) {
            RootContext.bind(rpcXid);
        }
    }

    @Override
    protected void doMessageOut(ServiceResponse sp) throws Throwable {
        this.afterCompletion(false);
    }

    @Override
    protected void doException(Throwable t) throws Throwable {
        this.afterCompletion(true);
    }

    private void afterCompletion(boolean error) {
        String xid = RootContext.getXID();
        String rpcXid = RestfulHeaderManager.get().getOnceHead().get(RootContext.KEY_XID.toLowerCase());
        if (Strings.isNullOrEmpty(xid) || Strings.isNullOrEmpty(rpcXid)) {
            return;
        }
        String unbindXid = RootContext.unbind();
        if (Strings.isNullOrEmpty(unbindXid)) {
            LOGGER.warn("Seata xid changed during RPC from {} to lost {}!", rpcXid, error ? "by error" : "");
            return;
        }
        if (!rpcXid.equalsIgnoreCase(unbindXid)) {
            RootContext.bind(unbindXid);
            LOGGER.warn("Seata xid changed during RPC from {} to {} {}, and bind back to RootContext!", rpcXid, unbindXid, error ? "by error" : "");
        }
    }

}
