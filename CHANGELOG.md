


## fire-swift-1.0 (2021/12/23)

#### 功能改进:
* 新增 fire-swift-utils工具类
* 新增 spring-cloud-swift-biz-parent pom
* starter改进

#### 缺陷修复:


## fire-swift-1.0 (2021/5/10)

#### spring-cloud-swift-nacos-openx-starter:
* 配置文件支持bootstrap.yml，继续兼容以前的启动参数（app_id、app_stage、app_ver、app_conf）
* 配置文件支持application.yml，可以配置spring（包括数据源、日志）、mybatis、openx（包括openx拦截器）
* Openx的Service接口Mapping支持2节和2节以上
* Openx支持透传Http Request的x-开头的参数

#### 功能改进:


#### 缺陷修复:


