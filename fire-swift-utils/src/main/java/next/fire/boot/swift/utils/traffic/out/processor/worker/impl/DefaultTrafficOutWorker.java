package next.fire.boot.swift.utils.traffic.out.processor.worker.impl;

import next.fire.boot.swift.utils.traffic.out.processor.worker.TrafficOutWorker;
import next.fire.boot.swift.utils.traffic.out.processor.model.FactorQuery;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutData;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutPolicy;

import java.util.List;

/**
 * Created by daibing on 2021/11/21.
 */
public class DefaultTrafficOutWorker implements TrafficOutWorker {

    // todo

    @Override
    public void save(TrafficOutData data) {

    }

    @Override
    public int remove(long dataId) {
        return 0;
    }

    @Override
    public int removeList(List<Long> dataIds) {
        return 0;
    }

    @Override
    public int count(FactorQuery query) {
        return 0;
    }

    @Override
    public List<TrafficOutData> listByPolicy(TrafficOutPolicy policy) {
        return null;
    }
}
