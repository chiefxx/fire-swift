package next.fire.boot.swift.utils.traffic.in.processor.worker;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * Created by daibing on 2021/12/3.
 */
public class TrafficInWorkerSpringFactory extends TrafficInWorkerFactory implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof TrafficInWorker) {
            register((TrafficInWorker) bean);
        }
        return bean;
    }
}
