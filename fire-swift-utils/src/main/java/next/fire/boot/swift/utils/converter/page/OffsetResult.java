package next.fire.boot.swift.utils.converter.page;

import java.util.List;

/**
 * Created by daibing on 2020/2/27.
 */
public class OffsetResult<E> {
    private String offsetPosition;
    private int offsetSize;
    private List<E> result;

    public OffsetResult() {
    }

    public OffsetResult(String offsetPosition, int offsetSize) {
        this.offsetPosition = offsetPosition;
        this.offsetSize = offsetSize;
    }

    public OffsetResult(String offsetPosition, int offsetSize, List<E> result) {
        this.offsetPosition = offsetPosition;
        this.offsetSize = offsetSize;
        this.result = result;
    }

    public int getOffsetSize() {
        return offsetSize;
    }

    public void setOffsetSize(int offsetSize) {
        this.offsetSize = offsetSize;
    }

    public String getOffsetPosition() {
        return offsetPosition;
    }

    public void setOffsetPosition(String offsetPosition) {
        this.offsetPosition = offsetPosition;
    }

    public List<E> getResult() {
        return result;
    }

    public void setResult(List<E> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "OffsetResult{" +
                "offsetPosition='" + offsetPosition + '\'' +
                ", offsetSize=" + offsetSize +
                ", result=" + result +
                '}';
    }
}
