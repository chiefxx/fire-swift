package next.fire.boot.swift.utils.system;

import java.util.Map;
import java.util.Properties;

public class EnvHelper {

    public static String getEnvSettingIfPresent(String... key) {
        // first from vm
        Properties p = System.getProperties();
        for (@SuppressWarnings("rawtypes") Map.Entry en : p.entrySet()) {
            for (String k : key) {
                if (k.equalsIgnoreCase(en.getKey().toString())) {
                    return en.getValue().toString();
                }
            }
        }

        // second from env
        Map<String, String> env = System.getenv();
        for (@SuppressWarnings("rawtypes") Map.Entry en : env.entrySet()) {
            for (String k : key) {
                if (k.equalsIgnoreCase(en.getKey().toString())) {
                    return en.getValue().toString();
                }
            }
        }
        return null;
    }

}
