package next.fire.boot.swift.utils.codec;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Created by daibing on 2021/11/13.
 */
public class Base64Helper {

    /**
     * 在jdk8支持基础上扩展encodeBase64
     */
    public static byte[] encodeBase64(byte[] binaryData) {
        final Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encode(binaryData);
    }

    public static String encodeBase64(String data) {
        return new String(encodeBase64(data.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * 在jdk8支持基础上扩展decodeBase64
     */
    public static byte[] decodeBase64(byte[] base64Data) {
        final Base64.Decoder decoder = Base64.getDecoder();
        return decoder.decode(base64Data);
    }

    public static String decodeBase64(String base64Data) {
        return new String(decodeBase64(base64Data.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
    }

}
