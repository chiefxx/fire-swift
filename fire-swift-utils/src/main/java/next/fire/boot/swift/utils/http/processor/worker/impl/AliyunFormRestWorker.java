package next.fire.boot.swift.utils.http.processor.worker.impl;

import com.google.common.base.Splitter;
import next.fire.boot.swift.utils.codec.HmacSHA1;
import next.fire.boot.swift.utils.codec.URLEncodeHelper;
import next.fire.boot.swift.utils.converter.date.DateHelper;
import next.fire.boot.swift.utils.http.processor.model.RequestTemplate;
import next.fire.boot.swift.utils.http.processor.model.RestSettings;
import next.fire.boot.swift.utils.generate.id.Snowflake;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by daibing on 2021/11/20.
 */
public class AliyunFormRestWorker extends AbstractRestWorker {
    private static final String ACCESS_KEY_SECRET = "AccessKeySecret";

    @Override
    public String buildBody(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams) {
        // 1.split body
        @SuppressWarnings("UnstableApiUsage")
        Map<String, String> bodyMap = new TreeMap<>(Splitter.on("&").trimResults().withKeyValueSeparator("=").split(template.getBody()));

        // 2.replace .DATA & remove null value
        Iterator<Map.Entry<String, String>> it = bodyMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> next = it.next();
            if (!next.getValue().endsWith(".DATA}}")) {
                continue;
            }
            String paramsKey = next.getValue().replace("{{", "").replace(".DATA}}", "").toLowerCase();
            Object paramsValue = getParamsValue(paramsKey, bizParams, accessParams);
            if (settings.getRequestSettings().isRemoveNullValue() && paramsValue == null) {
                it.remove();
            } else {
                next.setValue(String.valueOf(paramsValue));
            }
        }

        // 3.replace .ACTION
        boolean signature = false;
        it = bodyMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> next = it.next();
            if (!next.getValue().endsWith(".ACTION}}")) {
                continue;
            }
            switch (next.getValue()) {
                case "{{Timestamp.ACTION}}":
                    next.setValue(DateHelper.getISO8601Time(new Date()));
                    break;
                case "{{SignatureNonce.ACTION}}":
                    next.setValue(String.valueOf(Snowflake.get().nextId()));
                    break;
                case "{{Signature.ACTION}}":
                    it.remove();
                    signature = true;
                    break;
                default:
                    break;
            }
        }

        // 4.signature
        String body = map2String(bodyMap, true);
        if (!signature) {
            return body;
        }
        Object accessKeySecret = accessParams.get(ACCESS_KEY_SECRET.toLowerCase());
        if (accessKeySecret == null) {
            throw new RuntimeException("Not found access key secret from access params: " + ACCESS_KEY_SECRET.toLowerCase());
        }
        String sign = bodySign(template.getMethod().name(), body, accessKeySecret.toString());
        return body + "&" + URLEncodeHelper.percentEncode("Signature") + "=" + URLEncodeHelper.percentEncode(sign);
    }

    private String bodySign(String method, String body, String accessKeySecret) {
        StringBuilder sb = new StringBuilder();
        sb.append(method)
                .append("&")
                .append(URLEncodeHelper.percentEncode("/"))
                .append("&")
                .append(URLEncodeHelper.percentEncode(body));
        return HmacSHA1.genHMACBase64Encoded(sb.toString(), accessKeySecret + "&");
    }
}
