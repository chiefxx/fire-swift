package next.fire.boot.swift.utils.file;

import java.io.*;
import java.nio.charset.Charset;
import java.util.zip.*;

/**
 * ZIP处理类
 * created by zyh
 */
public class ZipHelper {
    private static final int BUFFER = 1024;

    /**
     * 压缩文件
     * @param srcFile    源文件
     * @param destFile   压缩文件
     */
    public static void compress(File srcFile, File destFile) {
        compress(srcFile, destFile.getPath());
    }

    /**
     * 压缩文件
     * @param srcFile    源文件
     * @param destPath   压缩路径
     */
    public static void compress(File srcFile, String destPath) {
        // 1、check file
        if (!srcFile.exists()) {
            throw new RuntimeException("srcFile does not exist, path= " + srcFile.getAbsolutePath());
        }
        File targetFile = new File(destPath);
        if (targetFile.exists()) {
            throw new RuntimeException("targetFile exist, path= " + targetFile.getAbsolutePath());
        }
        // 2、compress file
        try {
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(targetFile), Charset.defaultCharset());
            compressFile(srcFile, zipOutputStream, "", true);
            zipOutputStream.flush();
            zipOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void compressFile(File file, ZipOutputStream output, String childPath, boolean outer){
        FileInputStream input = null;
        // 不将最外层压缩进来
        childPath = outer? childPath + (childPath.length() == 0 ? "" : "/") :
                childPath + (childPath.length() == 0 ? "" : "/") + file.getName();
        try {
            // if dir
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                // if empty
                if(files == null || files.length == 0) {
                    output.putNextEntry(new ZipEntry(childPath + "/"));
                } else {
                    // if not empty
                    for (File f : files) {
                        compressFile(f, output, childPath, false);
                    }
                }
            } else {
                // if file
                output.putNextEntry(new ZipEntry(childPath));
                input = new FileInputStream(file);
                int readLen;
                byte[] buffer = new byte[BUFFER];
                while ((readLen = input.read(buffer)) != -1) {
                    output.write(buffer, 0, readLen);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * 解压文件
     * @param srcFile  源文件
     * @param destFile 解压文件
     */
    public static void decompress(File srcFile, File destFile) {
        decompress(srcFile, destFile.getPath());
    }

    /**
     * 解压文件
     * @param srcFile  源文件
     * @param destPath 解压路径
     */
    public static void decompress(File srcFile, String destPath) {
        try {
            CheckedInputStream cis = new CheckedInputStream(new FileInputStream(srcFile), new CRC32());
            ZipInputStream zis = new ZipInputStream(cis, Charset.defaultCharset());
            decompress(new File(destPath), zis);
            zis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void decompress(File destFile, ZipInputStream zis) throws Exception {
        ZipEntry entry;
        while ((entry = zis.getNextEntry()) != null) {
            // 文件
            String dir = destFile.getPath() + File.separator + entry.getName();
            File dirFile = new File(dir);
            // 文件检查
            fileProber(dirFile);
            if (entry.isDirectory()) {
                dirFile.mkdirs();
            } else {
                decompressFile(dirFile, zis);
            }
            zis.closeEntry();
        }
    }

    private static void fileProber(File dirFile) {
        File parentFile = dirFile.getParentFile();
        if (!parentFile.exists()) {
            fileProber(parentFile);
            parentFile.mkdir();
        }
    }

    private static void decompressFile(File destFile, ZipInputStream zis) throws Exception {
        BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(destFile));
        int count;
        byte[] data = new byte[BUFFER];
        while ((count = zis.read(data, 0, BUFFER)) != -1) {
            bos.write(data, 0, count);
        }
        bos.close();
    }
}