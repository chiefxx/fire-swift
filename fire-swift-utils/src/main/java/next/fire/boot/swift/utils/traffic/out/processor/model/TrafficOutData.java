package next.fire.boot.swift.utils.traffic.out.processor.model;

import java.util.Date;
import java.util.Map;

/**
 * Created by daibing on 2021/11/21.
 */
public class TrafficOutData {
    /**
     * 基础数据：注解处理器负责填值
     */
    private long id;
    private String clazz;
    private String method;
    private int argNumber;
    private String factor0;
    private String factor1;
    private String factor2;
    private Map<String, Object> argMap;

    /**
     * 扩展数据：TrafficOutWorker供应方负责填值
     */
    private PriorityEnum priority;
    private Date sendTime;

    /**
     * 发送结果数据
     */
    private boolean error = false;
    private String errorMsg;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getArgNumber() {
        return argNumber;
    }

    public void setArgNumber(int argNumber) {
        this.argNumber = argNumber;
    }

    public String getFactor0() {
        return factor0;
    }

    public void setFactor0(String factor0) {
        this.factor0 = factor0;
    }

    public String getFactor1() {
        return factor1;
    }

    public void setFactor1(String factor1) {
        this.factor1 = factor1;
    }

    public String getFactor2() {
        return factor2;
    }

    public void setFactor2(String factor2) {
        this.factor2 = factor2;
    }

    public Map<String, Object> getArgMap() {
        return argMap;
    }

    public void setArgMap(Map<String, Object> argMap) {
        this.argMap = argMap;
    }

    public PriorityEnum getPriority() {
        return priority;
    }

    public void setPriority(PriorityEnum priority) {
        this.priority = priority;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
