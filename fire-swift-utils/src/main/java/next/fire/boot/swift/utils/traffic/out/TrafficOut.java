package next.fire.boot.swift.utils.traffic.out;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 出口流量
 * Created by daibing on 2021/11/21.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface TrafficOut {
    String factor0Key() default "";
    String factor1Key() default "";
    String factor2Key() default "";
}
