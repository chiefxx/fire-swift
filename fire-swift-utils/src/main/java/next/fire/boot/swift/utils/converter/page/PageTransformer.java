package next.fire.boot.swift.utils.converter.page;

import com.github.pagehelper.Page;
import next.framework.page.PageResult;

import java.util.Arrays;
import java.util.List;

/**
 * 分页对象转换
 * Created by daibing on 2021/11/21.
 */
public class PageTransformer {

    public static <T> PageResult<T> transform(Page<T> page) {
        PageResult<T> result = new PageResult<>(page.getPageNum(), page.getPageSize());
        result.setTotalPage(page.getPages());
        result.setTotalSize((int) page.getTotal());
        result.setResult(page.getResult());
        return result;
    }

    public static <T> PageResult<T> emptyPage(int pageNo, int pageSize) {
        return new PageResult<>(pageNo, pageSize);
    }

    @SafeVarargs
    public static <T> PageResult<T> pageByMemory(int pageNo, int pageSize, T... element) {
        PageResult<T> pageResult = new PageResult<>(pageNo, pageSize);
        pageResult.setTotalPage((element.length / pageSize + ((element.length % pageSize == 0) ? 0 : 1)));
        pageResult.setTotalSize(element.length);
        pageResult.setResult(Arrays.asList(element));
        return pageResult;
    }

    public static <T> PageResult<T> pageByMemory(int pageNo, int pageSize, List<T> elements) {
        PageResult<T> pageResult = new PageResult<>(pageNo, pageSize);
        pageResult.setTotalPage((elements.size() / pageSize + ((elements.size() % pageSize == 0) ? 0 : 1)));
        pageResult.setTotalSize(elements.size());
        pageResult.setResult(elements);
        return pageResult;
    }

    public static <T> PageResult<T> buildPage(int pageNo, int pageSize, long totalSize, List<T> elements) {
        PageResult<T> pageResult = new PageResult<>(pageNo, pageSize);
        pageResult.setTotalPage((int) (totalSize / pageSize));
        pageResult.setTotalSize(totalSize);
        pageResult.setResult(elements);
        return pageResult;
    }
}
