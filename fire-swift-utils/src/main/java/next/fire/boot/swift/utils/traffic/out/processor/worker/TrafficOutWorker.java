package next.fire.boot.swift.utils.traffic.out.processor.worker;

import next.fire.boot.swift.utils.traffic.out.processor.model.FactorQuery;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutData;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutPolicy;

import java.util.List;

/**
 * Created by daibing on 2021/11/21.
 */
public interface TrafficOutWorker {

    /**
     * 拦截数据包
     */
    void save(TrafficOutData data);

    /**
     * 放行数据包后删除
     */
    int remove(long dataId);

    /**
     * 放行数据包后批量删除
     */
    int removeList(List<Long> dataIds);

    /**
     * 大致统计流控数据
     */
    int count(FactorQuery query);

    /**
     * 根据策略查询流控数据
     */
    List<TrafficOutData> listByPolicy(TrafficOutPolicy policy);

}
