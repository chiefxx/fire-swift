package next.fire.boot.swift.utils.traffic.out;

import next.fire.boot.swift.utils.system.EnvHelper;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by daibing on 2021/11/21.
 */
public class TrafficOutManager {
    private static final ConcurrentMap<String, Object> CLAZZ_2_BEAN = new ConcurrentHashMap<>(16);
    private static final ConcurrentMap<String, Method> CLAZZ_METHOD_ARG_2_METHOD = new ConcurrentHashMap<>(16);
    private static final ThreadLocal<Boolean> TRAFFIC_LIGHT = new ThreadLocal<Boolean>() {
        @Override
        protected Boolean initialValue() {
            return Boolean.TRUE;
        }
    };
    private static volatile String workerClassName = "next.fire.boot.swift.utils.traffic.out.processor.worker.impl.DefaultTrafficOutWorker";
    private static volatile boolean switchOn = true;

    static {
        String trafficSwitchOn = EnvHelper.getEnvSettingIfPresent("traffic.switch.on", "traffic_switch_on");
        if (!isBlank(trafficSwitchOn)) {
            switchOn = Boolean.parseBoolean(trafficSwitchOn);
        }
    }

    public static boolean isSwitchOn() {
        return switchOn;
    }

    public static String getWorkerClassName() {
        return workerClassName;
    }

    public static void setWorkerClassName(String workerClassName) {
        TrafficOutManager.workerClassName = workerClassName;
    }

    /**
     * 注解Traffic插入代码将调用register来注册bean
     */
    public static void register(Object bean) {
        if (!switchOn) {
            return;
        }
        Object oldBean = CLAZZ_2_BEAN.get(bean.getClass().getSimpleName());
        if (oldBean != null && !bean.getClass().getName().equals(oldBean.getClass().getName())) {
            throw new RuntimeException("Found same class with different package: " + bean.getClass().getName() + ", " + oldBean.getClass().getName());
        }
        if (oldBean == null) {
            Method[] methods = bean.getClass().getDeclaredMethods();
            for (Method method : methods) {
                Integer argNumber = method.getParameterTypes().length;
                String methodKey = getMethodKey(bean.getClass().getSimpleName(), method.getName(), argNumber);
                CLAZZ_METHOD_ARG_2_METHOD.put(methodKey, method);
            }
        }
        CLAZZ_2_BEAN.put(bean.getClass().getSimpleName(), bean);
    }

    public static Object getBean(String clazz) {
        Object bean = CLAZZ_2_BEAN.get(clazz);
        if (bean == null) {
            throw new RuntimeException("Not found Controller by clazz: " + clazz);
        }
        return bean;
    }

    public static Method getMethod(String clazz, String methodName, Integer argNumber) {
        String methodKey = getMethodKey(clazz, methodName, argNumber);
        Method method = CLAZZ_METHOD_ARG_2_METHOD.get(methodKey);
        if (method == null) {
            throw new RuntimeException("Not found Method by methodKey: " + methodKey);
        }
        return method;
    }

    public static Method getMethodIfPresent(String clazz, String methodName, Integer argNumber) {
        String methodKey = getMethodKey(clazz, methodName, argNumber);
        return CLAZZ_METHOD_ARG_2_METHOD.get(methodKey);
    }

    /**
     * 注解Traffic插入代码将调用passByTraffic，来判断是否走流控
     */
    public static boolean passByTraffic() {
        return switchOn && TRAFFIC_LIGHT.get();
    }

    /**
     * 关闭流控: 同一个方法完成两个操作（数据进入流控表和流控表出来的数据真正发送出去），每个操作由具体的线程完成，所以通过线程变量来控制，
     *          HTTP接口收到请求的线程使用默认值（走流控），流控处理线程手工设置（不走流控）
     */
    public static void closeTraffic() {
        TRAFFIC_LIGHT.set(Boolean.FALSE);
    }

    private static String getMethodKey(String clazz, String method, Integer argNumber) {
        return String.format("%s_%s_%s", clazz, method, argNumber);
    }

    private static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

}
