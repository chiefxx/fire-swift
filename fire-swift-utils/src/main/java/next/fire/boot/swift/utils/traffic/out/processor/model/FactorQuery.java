package next.fire.boot.swift.utils.traffic.out.processor.model;

/**
 * Created by daibing on 2018/8/31.
 */
public class FactorQuery {
    private String clazz;
    private String method;
    private String factor0;
    private String factor1;
    private String factor2;

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getFactor0() {
        return factor0;
    }

    public void setFactor0(String factor0) {
        this.factor0 = factor0;
    }

    public String getFactor1() {
        return factor1;
    }

    public void setFactor1(String factor1) {
        this.factor1 = factor1;
    }

    public String getFactor2() {
        return factor2;
    }

    public void setFactor2(String factor2) {
        this.factor2 = factor2;
    }
}
