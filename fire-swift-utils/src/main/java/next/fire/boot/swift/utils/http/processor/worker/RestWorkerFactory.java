package next.fire.boot.swift.utils.http.processor.worker;

import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by daibing on 2021/11/20.
 */
public class RestWorkerFactory {
    private static final ConcurrentMap<String, RestWorker> IMPL_MAPPING = new ConcurrentHashMap<>(2);

    static {
        ServiceLoader<RestWorker> serviceLoader = ServiceLoader.load(RestWorker.class);
        for (RestWorker worker : serviceLoader) {
            try {
                register(worker);
            } catch (Exception e) {
                // ignore error when load spring bean
            }
        }
    }

    /**
     * 如果实现类是Spring Bean，需要手工注册
     */
    public static void register(RestWorker worker) {
        String clazz = worker.getClass().getName();
        IMPL_MAPPING.put(clazz, worker);
    }

    public static RestWorker get(String restWorkerClassName) {
        RestWorker worker = IMPL_MAPPING.get(restWorkerClassName);
        if (worker == null) {
            throw new RuntimeException("Not found RestWorker impl by restWorkerClassName: " + restWorkerClassName);
        }
        return worker;
    }

}
