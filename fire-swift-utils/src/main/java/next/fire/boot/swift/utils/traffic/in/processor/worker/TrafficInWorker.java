package next.fire.boot.swift.utils.traffic.in.processor.worker;

import next.fire.boot.swift.utils.traffic.in.processor.model.TrafficInPolicy;
import next.fire.boot.swift.utils.traffic.in.processor.model.TrafficInStats;

import java.util.Map;

/**
 * Created by daibing on 2021/12/4.
 */
public interface TrafficInWorker {

    /**
     * 根据注解固定策略编号获取入口流量策略
     */
    TrafficInPolicy getPolicy(String policyCode);

    /**
     * 根据注解所在方法和参数获取入口流量策略
     */
    TrafficInPolicy getPolicy(String clazz, String method, int argNumber, Map<String, Object> argMap);

    /**
     * 入口流量统计初始数据，支持多个节点重复插入
     */
    int insertOrUpdate(TrafficInStats stats);

    /**
     * 申请一批授权
     * @param trafficInPolicyCode 策略编号
     * @param floorBoundByPeriod 周期地板值用来确定周期边界
     * @param applyNum 申请数量
     */
    boolean acquireBatch(String trafficInPolicyCode, long floorBoundByPeriod, int applyNum);

}
