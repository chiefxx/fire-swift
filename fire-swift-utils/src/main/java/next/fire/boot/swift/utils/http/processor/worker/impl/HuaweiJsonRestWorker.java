package next.fire.boot.swift.utils.http.processor.worker.impl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import next.fire.boot.swift.utils.codec.ByteHelper;
import next.fire.boot.swift.utils.codec.HmacSHA256;
import next.fire.boot.swift.utils.codec.URLEncodeHelper;
import next.fire.boot.swift.utils.converter.date.DateHelper;
import next.fire.boot.swift.utils.converter.json.JacksonHelper;
import next.fire.boot.swift.utils.http.processor.model.RequestTemplate;
import next.fire.boot.swift.utils.http.processor.model.RestSettings;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xiongjm
 * @date 2021/12/15
 */
public class HuaweiJsonRestWorker extends AbstractRestWorker {
    private static final Pattern BILL_TEMPLATE_PATTERN = Pattern.compile("\\{\\{[A-Za-z0-9]+\\.DATA}}");
    private static final String ACCESS_KEY_SECRET = "AccessKeySecret";
    private static final String ACCESS_KEY_ID = "AccessKeyId";
    private final ThreadLocal<ObjectMapper> THREAD_LOCAL_OBJECT_MAPPER = new ThreadLocal<ObjectMapper>() {
        @Override
        protected ObjectMapper initialValue() {
            return JsonMapper.builder()
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                    .enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
                    .build();
        }
    };

    @Override
    public String buildBody(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams) {
        if (isBlank(template.getBody())) {
            return null;
        }
        // replace .DATA
        JsonNode node = JacksonHelper.toObj(template.getBody());
        replaceDataHolder(node, bizParams, accessParams);
        return toJson(node, settings.getRequestSettings().isRemoveNullValue());
    }

    @Override
    public String buildUrl(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams, String bodySign) {
        String uri = template.getUri();
        // 1.get url params
        Matcher matcher = BILL_TEMPLATE_PATTERN.matcher(uri);
        List<String> urlParamsKey = new ArrayList<>();
        while (matcher.find()) {
            urlParamsKey.add(matcher.group().replace("{{", "").replace(".DATA}}", ""));
        }

        // 2.replace .DATA
        for (String paramKey : urlParamsKey) {
            Object paramsValue = getParamsValue(paramKey.toLowerCase(), bizParams, accessParams);
            if (paramsValue == null) {
                throw new RuntimeException("params value is null paramsKey=" + paramKey);
            }
            String regex = "(?i)\\{\\{" + paramKey + ".DATA}}";
            uri = uri.replaceAll(regex, String.valueOf(paramsValue));
        }
        return uri;
    }

    @Override
    public Map<String, String> buildHeader(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams, String bodySign, String url) {
        Map<String, String> header = template.getHeaders();

        // 1.replace .DATA & remove null value
        Iterator<Map.Entry<String, String>> it = header.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> next = it.next();
            if (!next.getValue().endsWith(".DATA}}")) {
                continue;
            }
            String paramsKey = next.getValue().replace("{{", "").replace(".DATA}}", "").toLowerCase();
            Object paramsValue = getParamsValue(paramsKey, bizParams, accessParams);
            if (settings.getRequestSettings().isRemoveNullValue() && paramsValue == null) {
                it.remove();
            } else {
                next.setValue(String.valueOf(paramsValue));
            }
        }

        // 2.replace .ACTION
        boolean signature = false;
        it = header.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> next = it.next();
            if (!next.getValue().endsWith(".ACTION}}")) {
                continue;
            }
            switch (next.getValue()) {
                case "{{Timestamp.ACTION}}":
                    next.setValue(DateHelper.getISO8601Date(new Date()));
                    break;
                case "{{Signature.ACTION}}":
                    it.remove();
                    signature = true;
                    break;
                default:
                    break;
            }
        }

        // 3.signature
        if (!signature) {
            return header;
        }
        Object accessKeyId = accessParams.get(ACCESS_KEY_ID.toLowerCase());
        Object accessKeySecret = accessParams.get(ACCESS_KEY_SECRET.toLowerCase());
        if (accessKeyId == null || accessKeySecret == null) {
            throw new RuntimeException("Not found access key id|secret from access params: " + ACCESS_KEY_ID.toLowerCase() + "|" + ACCESS_KEY_SECRET.toLowerCase());
        }
        header.put("Authorization", headSign(template.getMethod().name(), bodySign, url, header, String.valueOf(accessKeyId), String.valueOf(accessKeySecret)));
        return header;
    }

    /**
     * 签名步骤
     * 1.拼接method，url，header，body
     * 2.拼接签名方式，时间戳，第一步的字符串做一次sha256获得签名串
     * 3.拼接签名方式，accessKeyId，headerNames，第二步的拼接字符串做sha256获得签名串
     */
    private String headSign(String method, String bodySign, String url, Map<String, String> header, String accessKeyId, String accessKeySecret) {
        // 1.build url block
        URL urlObject = newUrl(url);
        String[] pathString = urlObject.getPath().split("/");
        StringBuilder urlBlock = new StringBuilder();
        for (String path : pathString) {
            urlBlock.append(URLEncodeHelper.percentEncode(path)).append("/");
        }
        urlBlock.append("\n");
        urlBlock.append(isBlank(urlObject.getQuery()) ? "" : urlObject.getQuery());

        // 2.build header block
        Map<String, String> allHeaders = new TreeMap<>();
        for (Map.Entry<String, String> entry : header.entrySet()) {
            if (entry.getKey().startsWith("Content-Type") && entry.getValue().startsWith("multipart/form-data")) {
                continue;
            }
            allHeaders.put(entry.getKey().toLowerCase(), entry.getValue());
        }
        StringBuilder headBlock = new StringBuilder();
        for (Map.Entry<String, String> entry : allHeaders.entrySet()) {
            headBlock.append(entry.getKey()).append(":").append(entry.getValue());
            headBlock.append("\n");
        }
        headBlock.append("\n");
        String signHeaderNames = String.join(";", allHeaders.keySet());
        headBlock.append(signHeaderNames);

        // 3.build rawText
        StringBuilder rawText = new StringBuilder();
        rawText.append(method).append("\n")
                .append(urlBlock).append("\n")
                .append(headBlock).append("\n")
                .append(bodySign);

        // 4.build sign string
        String rawString = ByteHelper.byteToHex(HmacSHA256.sha256(rawText.toString()));
        StringBuilder middleText = new StringBuilder();
        middleText.append("SDK-HMAC-SHA256").append("\n")
                .append(allHeaders.get("x-sdk-date")).append("\n")
                .append(rawString);

        // 5.signature
        byte[] keySecret = String.valueOf(accessKeySecret).getBytes(StandardCharsets.UTF_8);
        byte[] signatureByte = HmacSHA256.hmac(keySecret, middleText.toString());
        String signature = ByteHelper.byteToHex(signatureByte);

        // 6.build authorization
        StringBuilder authorization = new StringBuilder();
        authorization.append("SDK-HMAC-SHA256 Access=").append(accessKeyId).append(",SignedHeaders=").append(signHeaderNames).append(",Signature=").append(signature);
        return authorization.toString();
    }

    private URL newUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(String.format("URL failed: exception: %s", e));
        }
    }

    private void replaceDataHolder(JsonNode node, Map<String, Object> bizParams, Map<String, Object> accessParams) {
        if (node.isValueNode()) {
            return;
        }
        if (node.isObject()) {
            Iterator<Map.Entry<String, JsonNode>> it = node.fields();
            while (it.hasNext()) {
                Map.Entry<String, JsonNode> entry = it.next();
                if (entry.getValue().isValueNode()) {
                    String paramsKey = entry.getValue().textValue().replace("{{", "").replace(".DATA}}", "").toLowerCase();
                    Object paramsValue = getParamsValue(paramsKey, bizParams, accessParams);
                    entry.setValue(toJsonNode(paramsValue));
                } else {
                    replaceDataHolder(entry.getValue(), bizParams, accessParams);
                }
            }
        }
        if (node.isArray()) {
            for (JsonNode jsonNode : node) {
                replaceDataHolder(jsonNode, bizParams, accessParams);
            }
        }
    }

    private JsonNode toJsonNode(Object paramsValue) {
        return THREAD_LOCAL_OBJECT_MAPPER.get().valueToTree(paramsValue);
    }

    private String toJson(Object obj, boolean removeNullValue) {
        if (removeNullValue) {
            THREAD_LOCAL_OBJECT_MAPPER.get().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        } else {
            THREAD_LOCAL_OBJECT_MAPPER.get().setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
        }
        try {
            return THREAD_LOCAL_OBJECT_MAPPER.get().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
