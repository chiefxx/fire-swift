package next.fire.boot.swift.utils.http.processor.worker;

import next.fire.boot.swift.utils.http.processor.model.RequestTemplate;
import next.fire.boot.swift.utils.http.processor.model.RestSettings;

import java.util.Map;

/**
 * Created by daibing on 2021/11/20.
 */
public interface RestWorker {

    String buildBody(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams);

    String buildUrl(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams, String bodySign);

    Map<String, String> buildHeader(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams, String bodySign, String url);

    Map<String, String> execute(RestSettings settings, RequestTemplate template, String url, Map<String, String> header, String body);

    Map<String, Object> parse(RestSettings settings, RequestTemplate template, Map<String, String> result);

}
