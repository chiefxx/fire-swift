package next.fire.boot.swift.utils.traffic.out.processor;

import com.sun.source.tree.Tree;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.List;
import next.fire.boot.swift.utils.generate.id.Snowflake;
import next.fire.boot.swift.utils.traffic.apt.APT;
import next.fire.boot.swift.utils.traffic.out.TrafficOut;
import next.fire.boot.swift.utils.traffic.out.TrafficOutManager;
import next.fire.boot.swift.utils.traffic.out.processor.worker.TrafficOutWorkerFactory;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutData;

import javax.annotation.PostConstruct;
import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.*;

/**
 * Created by daibing on 2021/11/21.
 */
@SupportedAnnotationTypes("next.fire.boot.swift.utils.traffic.out.TrafficOut")
public class TrafficOutCaptureProcessor extends AbstractProcessor {
    private Messager messager;
    private JavacTrees javacTrees;
    private TreeMaker treeMaker;
    private Names names;
    private JavacElements elements;
    private APT apt;

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        messager = processingEnv.getMessager();
        javacTrees = JavacTrees.instance(processingEnv);
        Context context = ((JavacProcessingEnvironment) processingEnv).getContext();
        treeMaker = TreeMaker.instance(context);
        names = Names.instance(context);
        elements = JavacElements.instance(context);
        apt = new APT(treeMaker, names);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (roundEnv.processingOver()) {
            return false;
        }
        for (TypeElement annotation : annotations) {
            Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : elements) {
                if (element.getKind() != ElementKind.METHOD) {
                    messager.printMessage(Diagnostic.Kind.ERROR, "Only method can be annotated by traffic out!");
                    continue;
                }
                TrafficOut traffic = element.getAnnotation(TrafficOut.class);
                Element enclosingElement = element.getEnclosingElement();

                JCTree methodTree = javacTrees.getTree(element);
                methodTree.accept(new TreeTranslator() {
                    @Override
                    public void visitMethodDef(JCTree.JCMethodDecl jcMethodDecl) {
                        // skip method of inner class
                        if (apt.containAnnotation(jcMethodDecl.getModifiers().getAnnotations(), TrafficOut.class)) {
                            jcMethodDecl.body.stats = jcMethodDecl.getBody().getStatements().prependList(
                                    buildTrafficStatements(traffic, enclosingElement.getSimpleName().toString(), jcMethodDecl)
                            );
                        }
                        messager.printMessage(Diagnostic.Kind.NOTE, jcMethodDecl.getName() + " has been processed!");
                        super.visitMethodDef(jcMethodDecl);
                    }
                });

                JCTree clazzTree = javacTrees.getTree(enclosingElement);
                clazzTree.accept(new TreeTranslator() {
                    @Override
                    public void visitClassDef(JCTree.JCClassDecl jcClassDecl) {
                        // skip inner class
                        if (!enclosingElement.getSimpleName().toString().equals(jcClassDecl.getSimpleName().toString())) {
                            super.visitClassDef(jcClassDecl);
                            return;
                        }
                        boolean startupInside = false;
                        for (JCTree mt : jcClassDecl.defs) {
                            if (mt.getKind() != Tree.Kind.METHOD) {
                                continue;
                            }
                            startupInside = apt.containAnnotation(((JCTree.JCMethodDecl) mt).getModifiers().getAnnotations(), PostConstruct.class);
                            if (startupInside) {
                                int index = ((JCTree.JCMethodDecl) mt).getBody().getStatements().toString().indexOf(TrafficOutManager.class.getSimpleName() + "." + "register");
                                if (index < 0) {
                                    mt.accept(new TreeTranslator() {
                                        @Override
                                        public void visitMethodDef(JCTree.JCMethodDecl jcMethodDecl) {
                                            jcMethodDecl.body.stats = jcMethodDecl.getBody().getStatements().prepend(
                                                    apt.methodByArg(TrafficOutManager.class.getName(), "register", "this")
                                            );
                                            super.visitMethodDef(jcMethodDecl);
                                        }
                                    });
                                }
                                break;
                            }
                        }
                        if (!startupInside) {
                            jcClassDecl.defs = jcClassDecl.defs.prepend(buildStartupMethod());
                        }
                        messager.printMessage(Diagnostic.Kind.NOTE, jcClassDecl.getSimpleName() + " has been processed!");
                        super.visitClassDef(jcClassDecl);
                    }
                });
            }
        }
        return true;
    }

    /**
     * if(TrafficOutManager.passByTraffic()) { ... }
     */
    private List<JCTree.JCStatement> buildTrafficStatements(TrafficOut traffic, String clazz, JCTree.JCMethodDecl jcMethodDecl) {
        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
        statements.add(treeMaker.If(
                treeMaker.Apply(
                        List.nil(),
                        apt.memberAccess(TrafficOutManager.class.getName() + "." + "passByTraffic"),
                        List.nil()
                ),
                treeMaker.Block(0, this.buildMixDataStatements(traffic, clazz, jcMethodDecl)),
                null
        ));
        return statements.toList();
    }

    /**
     * 检查方法参数区分处理逻辑，注意需要支持因子key不设置字段
     */
    private List<JCTree.JCStatement> buildMixDataStatements(TrafficOut traffic, String clazz, JCTree.JCMethodDecl jcMethodDecl) {
        // 1、准备参数
        List<String> argNames = apt.getArgNames(jcMethodDecl.getParameters());
        boolean returnVoid = apt.returnVoid(jcMethodDecl.restype);
        List<String> validFactorKeys = listValidFactorKey(traffic.factor0Key(), traffic.factor1Key(), traffic.factor2Key());
        Map<String, String> factorKey2Method = new HashMap<>(validFactorKeys.size());

        // 2、方法参数直接包含所有有效因子key设置的字段
        factorKey2Method.putAll(apt.findField2Method(jcMethodDecl.getParameters(), validFactorKeys));
        if (factorKey2Method.size() == validFactorKeys.size()) {
            return buildNormalDataStatements(traffic, clazz, jcMethodDecl.getName().toString(), argNames, factorKey2Method, returnVoid);
        }

        // 3、方法参数是对象并且包含有效因子key设置的字段
        for (JCTree.JCVariableDecl variableDecl : jcMethodDecl.getParameters()) {
            Symbol.ClassSymbol element = elements.getTypeElement(variableDecl.sym.type.toString());
            if (element == null) {
                continue;
            }
            JCTree.JCClassDecl model = javacTrees.getTree(element);
            factorKey2Method.putAll(apt.findField2Method(variableDecl.name.toString(), model, validFactorKeys));
            if (factorKey2Method.size() == validFactorKeys.size()) {
                return buildNormalDataStatements(traffic, clazz, jcMethodDecl.getName().toString(), argNames, factorKey2Method, returnVoid);
            }
        }

        // 4、方法参数是列表对象，并且对象包含有效因子key设置的字段
        for (JCTree.JCVariableDecl variableDecl : jcMethodDecl.getParameters()) {
            if (!variableDecl.sym.type.tsym.toString().equals(java.util.List.class.getName())) {
                continue;
            }
            for (Type type : variableDecl.sym.type.getTypeArguments()) {
                Symbol.ClassSymbol element = elements.getTypeElement(type.toString());
                if (element == null) {
                    continue;
                }
                JCTree.JCClassDecl model = javacTrees.getTree(element);
                // 列表对象for循环的子对象，获取因子对应方法的时候，需要使用子对象的参数名
                String subArgName = "sub" + variableDecl.name.toString();
                factorKey2Method.putAll(apt.findField2Method(subArgName, model, validFactorKeys));
                if (factorKey2Method.size() == validFactorKeys.size()) {
                    return this.buildLoopDataStatements(traffic, clazz, jcMethodDecl.getName().toString(), argNames,
                            factorKey2Method, returnVoid, model.name.toString(), subArgName, variableDecl.name.toString());
                }
            }
        }

        // 5、其他暂时不支持
        throw new RuntimeException("Not found method arg matched by traffic.factor(1|2|3)Key, method: " + jcMethodDecl.toString());
    }

    /**
     * 支持列表对象参数方法
     */
    private List<JCTree.JCStatement> buildLoopDataStatements(TrafficOut traffic, String clazz, String method, List<String> argNames,
                                                             Map<String, String> factorKey2Method, boolean returnVoid, String modelName,
                                                             String subArgName, String loopArgName) {
        // 将argNames中的列表对象名称替换为子对象名称
        List<String> subArgNames = subArgNames(argNames, loopArgName, subArgName);
        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
        statements.add(treeMaker.ForeachLoop(
                treeMaker.VarDef(treeMaker.Modifiers(0), names.fromString(subArgName), apt.memberAccess(modelName), null),
                apt.memberAccess(loopArgName),
                treeMaker.Block(0, buildDataStatements(traffic, clazz, method, subArgNames, factorKey2Method))
        ));
        statements.add(this.buildReturn(returnVoid));
        return statements.toList();
    }

    /**
     * 支持基础参数方法和对象参数方法
     */
    private List<JCTree.JCStatement> buildNormalDataStatements(TrafficOut traffic, String clazz, String method, List<String> argNames,
                                                               Map<String, String> factorKey2Method, boolean returnVoid) {
        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
        statements.addAll(buildDataStatements(traffic, clazz, method, argNames, factorKey2Method));
        statements.add(buildReturn(returnVoid));
        return statements.toList();
    }

    /**
     * 构造保存数据body
     */
    private List<JCTree.JCStatement> buildDataStatements(TrafficOut traffic, String clazz, String method, List<String> argNames,
                                                         Map<String, String> factorKey2Method) {
        Name dataInstance = names.fromString("data");
        Name argMapInstance = names.fromString("argMap");
        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
        statements.addAll(this.buildArgMap(argMapInstance, argNames));
        statements.add(this.buildNewData(dataInstance));
        statements.add(this.buildId(dataInstance));
        statements.add(apt.methodByValue(dataInstance.toString(), "setClazz", clazz));
        statements.add(apt.methodByValue(dataInstance.toString(), "setMethod", method));
        statements.add(apt.methodByValue(dataInstance.toString(), "setArgNumber", argNames.size()));
        statements.addAll(this.buildFactorData(dataInstance, traffic, factorKey2Method));
        statements.add(apt.methodByArg(dataInstance.toString(), "setArgMap", argMapInstance.toString()));
        statements.add(this.buildSaveData(dataInstance));
        return statements.toList();
    }

    /**
     * TrafficData data = new TrafficData();
     */
    private JCTree.JCStatement buildNewData(Name dataInstance) {
        return treeMaker.VarDef(
                treeMaker.Modifiers(0),
                dataInstance,
                apt.memberAccess(TrafficOutData.class.getName()),
                treeMaker.NewClass(null, List.nil(), apt.memberAccess(TrafficOutData.class.getName()), List.nil(), null)
        );
    }

    /**
     * data.setId(Snowflake.get().nextId().longValue());
     */
    private JCTree.JCStatement buildId(Name dataInstance) {
        return treeMaker.Exec(
                treeMaker.Apply(
                        List.nil(),
                        apt.memberAccess(dataInstance.toString() + "." + "setId"),
                        List.of(treeMaker.Apply(
                                List.nil(),
                                treeMaker.Select(
                                        treeMaker.Apply(List.nil(),
                                        apt.memberAccess(Snowflake.class.getName() + "." + "get"), List.nil()),
                                        names.fromString("nextId")),
                                List.nil()))
                )
        );
    }

    /**
     * 注意：argMap字段顺序不能混淆，函数调用的时候根据该顺序来构造参数
     * Map<String, Object> argMap = new LinkedHashMap<>();
     * argMap.put("batchNo", batchNo);
     * argMap.put("xxx", xxx);
     */
    private List<JCTree.JCStatement> buildArgMap(Name argMapInstance, List<String> argNames) {
        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
        statements.add(treeMaker.VarDef(
                treeMaker.Modifiers(0),
                argMapInstance,
                apt.memberAccess(Map.class.getName(), String.class.getName(), Object.class.getName()),
                treeMaker.NewClass(null, List.nil(), apt.memberAccess(LinkedHashMap.class.getName()), List.nil(),null)
        ));
        for (String argName : argNames) {
            statements.add(apt.methodByMap(argMapInstance.toString(), "put", argName, argName));
        }
        return statements.toList();
    }

    /**
     * data.setFactor0(platform);
     * data.setFactor0(goodsStockDetail.getPlatform());
     * data.setFactor0(goodsStockDetail.getPlatform().name());
     */
    private List<JCTree.JCStatement> buildFactorData(Name dataInstance, TrafficOut traffic, Map<String, String> factorKey2Method) {
        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
        if (!isBlank(traffic.factor0Key())) {
            statements.add(methodByPath(dataInstance.toString(), "setFactor0", factorKey2Method.get(traffic.factor0Key())));
        }
        if (!isBlank(traffic.factor1Key())) {
            statements.add(methodByPath(dataInstance.toString(), "setFactor1", factorKey2Method.get(traffic.factor1Key())));
        }
        if (!isBlank(traffic.factor2Key())) {
            statements.add(methodByPath(dataInstance.toString(), "setFactor2", factorKey2Method.get(traffic.factor2Key())));
        }
        return statements.toList();
    }

    /**
     * 支持参数路径
     * data + setFactor1 + platformAccountCode -> data.setFactor1(platformAccountCode);
     * data + setFactor0 + platform.name -> data.setFactor0(platform.name());
     * data + setFactor0 + goodsStockDetail.getPlatform.name -> data.setFactor0(goodsStockDetail.getPlatform().name());
     */
    private JCTree.JCStatement methodByPath(String instanceName, String methodName, String argPath) {
        if (argPath.contains(".")) {
            return treeMaker.Exec(
                    treeMaker.Apply(
                            List.nil(),
                            apt.memberAccess(instanceName + "." + methodName),
                            List.of(treeMaker.Apply(List.nil(), apt.functionAccess(argPath, 2), List.nil()))
                    )
            );
        } else {
            return apt.methodByArg(instanceName, methodName, argPath);
        }
    }

    /**
     * TrafficOutWorkerFactory.get(TrafficOutManager.getWorkerClassName()).save(data);
     */
    private JCTree.JCStatement buildSaveData(Name dataInstance) {
        return treeMaker.Exec(
                treeMaker.Apply(
                        List.nil(),
                        treeMaker.Select(
                                treeMaker.Apply(
                                        List.nil(),
                                        apt.memberAccess(TrafficOutWorkerFactory.class.getName() + "." + "get"),
                                        List.of(treeMaker.Apply(List.nil(), apt.memberAccess(TrafficOutManager.class.getName() + "." + "getWorkerClassName"), List.nil()))
                                ),
                                names.fromString("save")
                        ),
                        List.of(treeMaker.Ident(dataInstance))
                )
        );
    }

    /**
     * return null;
     */
    private JCTree.JCStatement buildReturn(boolean returnVoid) {
        if (returnVoid) {
            return treeMaker.Return(null);
        } else {
            return treeMaker.Return(treeMaker.Literal(TypeTag.BOT, null));
        }
    }

    /**
     * \ @PostConstruct
     * private void startup() { TrafficOutManager.register(this); }
     */
    private JCTree.JCMethodDecl buildStartupMethod() {
        ListBuffer<JCTree.JCStatement> registerStatements = new ListBuffer<>();
        registerStatements.add(apt.methodByArg(TrafficOutManager.class.getName(), "register", "this"));
        try {
            return treeMaker.MethodDef(
                    treeMaker.Modifiers(
                            Flags.PRIVATE | Flags.ANNOTATION,
                            List.of(treeMaker.Annotation(apt.memberAccess(PostConstruct.class.getName()), List.nil()))),
                    names.fromString("startup"),
                    treeMaker.Type(Type.JCVoidType.class.newInstance()),
                    List.nil(),
                    List.nil(),
                    List.nil(),
                    treeMaker.Block(0, registerStatements.toList()),
                    null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

    private List<String> listValidFactorKey(String... factorKeys) {
        ListBuffer<String> validKeys = new ListBuffer<>();
        for (String key : factorKeys) {
            if (!isBlank(key)) {
                validKeys.add(key);
            }
        }
        return validKeys.toList();
    }

    private List<String> subArgNames(List<String> argNames, String listArgName, String subArgName) {
        ListBuffer<String> subArgNames = new ListBuffer<>();
        for (String argName : argNames) {
            if (argName.equals(listArgName)) {
                subArgNames.add(subArgName);
            } else {
                subArgNames.add(argName);
            }
        }
        return subArgNames.toList();
    }
}
