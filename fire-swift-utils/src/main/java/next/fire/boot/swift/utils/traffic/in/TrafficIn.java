package next.fire.boot.swift.utils.traffic.in;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 入口流量
 * Created by daibing on 2021/12/4.
 */
@Target({ElementType.METHOD, ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.SOURCE)
public @interface TrafficIn {
    boolean checkPermit() default false;
    String policyCode() default "";
}
