package next.fire.boot.swift.utils.http;

import java.util.Map;

/**
 * Created by daibing on 2021/11/13.
 */
public interface RequestHandler<T> {

    /**
     * 请求的业务参数
     */
    Map<String, Object> buildBizParams();

    /**
     * 请求的访问参数
     */
    Map<String, Object> buildAccessParams();

    /**
     * 从返回结果抽取数据
     */
    T extractData(Map<String, Object> output);

}
