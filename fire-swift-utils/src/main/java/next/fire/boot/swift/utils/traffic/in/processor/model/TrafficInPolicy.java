package next.fire.boot.swift.utils.traffic.in.processor.model;

/**
 * Created by daibing on 2021/12/4.
 */
public class TrafficInPolicy {
    /**
     * 流量策略编号
     */
    private String code;
    private String name;

    /**
     * 流量策略规格参数，相对粗粒度到分钟
     */
    private int periodMinutes;
    private int maximumLimit;
    private int preApplyNum;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPeriodMinutes() {
        return periodMinutes;
    }

    public void setPeriodMinutes(int periodMinutes) {
        this.periodMinutes = periodMinutes;
    }

    public int getMaximumLimit() {
        return maximumLimit;
    }

    public void setMaximumLimit(int maximumLimit) {
        this.maximumLimit = maximumLimit;
    }

    public int getPreApplyNum() {
        return preApplyNum;
    }

    public void setPreApplyNum(int preApplyNum) {
        this.preApplyNum = preApplyNum;
    }
}
