package next.fire.boot.swift.utils.http.processor.model;

import next.fire.boot.swift.utils.http.RequestMethod;

import java.util.Map;

/**
 * Created by daibing on 2021/11/20.
 */
public class RequestTemplate {
    /**
     * 请求模板路径
     */
    private String templateUrl;

    /**
     * 请求模板内容
     */
    private RequestMethod method;
    private Map<String, String> headers;
    private String body;
    private String uri;

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public void setMethod(RequestMethod method) {
        this.method = method;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
