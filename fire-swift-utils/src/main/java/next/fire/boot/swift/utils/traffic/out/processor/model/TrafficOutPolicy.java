package next.fire.boot.swift.utils.traffic.out.processor.model;

/**
 * Created by daibing on 2021/11/21.
 */
public class TrafficOutPolicy {
    /**
     * 流量策略编号
     */
    private String code;
    private String name;

    /**
     * 流量策略定位参数
     */
    private String clazz;
    private String method;
    private int argNumber;
    private String factor0;
    private String factor1;
    private String factor2;

    /**
     * 流量策略规格参数
     */
    private int waveCount;
    private int periodSeconds;
    private int callLimit;
    private int transportThreadNum = 1;
    private String errorRetryPattern;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getArgNumber() {
        return argNumber;
    }

    public void setArgNumber(int argNumber) {
        this.argNumber = argNumber;
    }

    public String getFactor0() {
        return factor0;
    }

    public void setFactor0(String factor0) {
        this.factor0 = factor0;
    }

    public String getFactor1() {
        return factor1;
    }

    public void setFactor1(String factor1) {
        this.factor1 = factor1;
    }

    public String getFactor2() {
        return factor2;
    }

    public void setFactor2(String factor2) {
        this.factor2 = factor2;
    }

    public int getWaveCount() {
        return waveCount;
    }

    public void setWaveCount(int waveCount) {
        this.waveCount = waveCount;
    }

    public int getPeriodSeconds() {
        return periodSeconds;
    }

    public void setPeriodSeconds(int periodSeconds) {
        this.periodSeconds = periodSeconds;
    }

    public int getCallLimit() {
        return callLimit;
    }

    public void setCallLimit(int callLimit) {
        this.callLimit = callLimit;
    }

    public int getTransportThreadNum() {
        return transportThreadNum;
    }

    public void setTransportThreadNum(int transportThreadNum) {
        this.transportThreadNum = transportThreadNum;
    }

    public String getErrorRetryPattern() {
        return errorRetryPattern;
    }

    public void setErrorRetryPattern(String errorRetryPattern) {
        this.errorRetryPattern = errorRetryPattern;
    }
}
