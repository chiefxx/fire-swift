package next.fire.boot.swift.utils.traffic.in.processor.model;

/**
 * Created by daibing on 2021/12/4.
 */
public class TrafficInStats {

    private long id;

    /**
     * 策略规格数据
     */
    private String trafficInPolicyCode;
    private int periodMinutes;
    private int maximumLimit;
    private long floorBoundByPeriod;

    /**
     * 策略统计数据
     */
    private int statsApplyTotal;
    private int statsUsedTotal;
    private boolean usedUp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTrafficInPolicyCode() {
        return trafficInPolicyCode;
    }

    public void setTrafficInPolicyCode(String trafficInPolicyCode) {
        this.trafficInPolicyCode = trafficInPolicyCode;
    }

    public int getPeriodMinutes() {
        return periodMinutes;
    }

    public void setPeriodMinutes(int periodMinutes) {
        this.periodMinutes = periodMinutes;
    }

    public int getMaximumLimit() {
        return maximumLimit;
    }

    public void setMaximumLimit(int maximumLimit) {
        this.maximumLimit = maximumLimit;
    }

    public long getFloorBoundByPeriod() {
        return floorBoundByPeriod;
    }

    public void setFloorBoundByPeriod(long floorBoundByPeriod) {
        this.floorBoundByPeriod = floorBoundByPeriod;
    }

    public int getStatsApplyTotal() {
        return statsApplyTotal;
    }

    public void setStatsApplyTotal(int statsApplyTotal) {
        this.statsApplyTotal = statsApplyTotal;
    }

    public int getStatsUsedTotal() {
        return statsUsedTotal;
    }

    public void setStatsUsedTotal(int statsUsedTotal) {
        this.statsUsedTotal = statsUsedTotal;
    }

    public boolean isUsedUp() {
        return usedUp;
    }

    public void setUsedUp(boolean usedUp) {
        this.usedUp = usedUp;
    }
}
