package next.fire.boot.swift.utils.traffic.out.processor;

import com.talkyun.utils.Looper;
import com.talkyun.utils.env.HostManager;
import next.fire.boot.swift.utils.traffic.out.TrafficOutManager;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by daibing on 2021/11/29.
 */
public class TrafficOutReleaseProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(TrafficOutReleaseProcessor.class);
    private static final String USER = HostManager.get().getHostName();
    private static final String ADDRESS = HostManager.get().getLocalIp();
    private final ConcurrentMap<String, TrafficOutPolicy> code2Policy = new ConcurrentHashMap<>();
    private Looper manager;

    public void init() {
        manager = this.buildManager();
        manager.start();
    }

    public void destroy() {
        if (manager != null) {
            manager.shutdown();
        }
    }

    public String registerPolicy(TrafficOutPolicy policy) {
        code2Policy.put(policy.getCode(), policy);
        return policy.getCode();
    }

    public String removePolicy(String policyCode) {
        TrafficOutPolicy policy = code2Policy.remove(policyCode);
        return policy != null ? policy.getCode() : null;
    }

    private Looper buildManager() {
        return new Looper("manager", 5 * 60 * 1000, 60 * 1000) {
            private final ConcurrentMap<String, TrafficOutReleaseTask> code2Task = new ConcurrentHashMap<>();

            @Override
            protected void loop() throws Throwable {
                // 1. 禁用流控传输：如果已经拉起传输线程池就需要先停止！
                if (!TrafficOutManager.isSwitchOn()) {
                    shutdownTask(code2Task, code2Task.keySet());
                    code2Task.clear();
                    LOGGER.info("manager return by disabled traffic: user={}, address={}", USER, ADDRESS);
                    return;
                }

                // 2. 检查任务和当前的策略，不存在策略的任务都关闭
                Set<String> taskCodes = code2Task.keySet();
                taskCodes.removeAll(code2Policy.keySet());
                shutdownTask(code2Task, taskCodes);

                // 3. 检查策略和当前的任务，不存在任务的策略都新起
                Set<String> policyCodes = code2Policy.keySet();
                policyCodes.removeAll(code2Task.keySet());
                startupTask(code2Policy, code2Task, policyCodes);
            }

            @Override
            protected void loopThrowable(Throwable t) {
                LOGGER.error("manager failed: user={}, address={}", USER, ADDRESS, t);
            }
        };
    }

    private void startupTask(ConcurrentMap<String, TrafficOutPolicy> code2Policy, ConcurrentMap<String, TrafficOutReleaseTask> code2Task, Set<String> startupPolicyCodes) {
        for (String code : startupPolicyCodes) {
            TrafficOutReleaseTask task = new TrafficOutReleaseTask(code2Policy.get(code));
            task.startup();
            code2Task.put(code, task);
            LOGGER.info("manager startup traffic release task: code={}, user={}, address={}", code, USER, ADDRESS);
        }
    }

    private void shutdownTask(ConcurrentMap<String, TrafficOutReleaseTask> code2Task, Set<String> shutdownTaskCodes) {
        for (String code : shutdownTaskCodes) {
            TrafficOutReleaseTask task = code2Task.remove(code);
            task.shutdown();
            LOGGER.info("manager shutdown traffic release task: code={}, user={}, address={}", code, USER, ADDRESS);
        }
    }

}
