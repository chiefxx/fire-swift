package next.fire.boot.swift.utils.file;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by daibing on 2019/10/27.
 */
public class FileHelper {

    public static String getCurrentDir() {
        File file = new File(FileHelper.class.getProtectionDomain().getCodeSource().getLocation().getFile());
        try {
            return URLDecoder.decode(file.getParent(), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void removeFile(Collection<String> files) {
        for (String file : files) {
            removeFolder(new File(file));
        }
    }

    public static boolean removeFolder(File path) {
        if (path.isFile()) {
            return path.delete();
        }
        File[] subFiles = path.listFiles();
        if (subFiles != null) {
            for (File subFile : subFiles) {
                removeFolder(subFile);
            }
        }
        return path.delete();
    }

    public static List<File> listSubFile(File path, boolean onlyFile) {
        File[] fileArray = path.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return !(name.startsWith(".") || name.endsWith(".iml") || name.endsWith("target"));
            }
        });
        if (fileArray == null) {
            return Collections.emptyList();
        }
        List<File> files = new ArrayList<>();
        for (File file : fileArray) {
            if (onlyFile) {
                if (file.isFile()) {
                    files.add(file);
                }
            } else {
                files.add(file);
            }
            if (file.isDirectory()) {
                files.addAll(listSubFile(file, onlyFile));
            }
        }
        return files;
    }

    public static void copy(File sourcePath, File targetPath) throws IOException {
        copy(sourcePath, targetPath, true);
    }

    public static void copy(File sourcePath, File targetPath, boolean onlyFile) throws IOException {
        if (!sourcePath.exists()) {
            return;
        }
        copy(sourcePath, targetPath, listSubFile(sourcePath, onlyFile));
    }

    public static void copyByPath(File sourcePath, File targetPath, Collection<String> copyFilePaths) throws IOException {
        List<File> copyFiles = new ArrayList<>(copyFilePaths.size());
        for (String copyFilePath : copyFilePaths) {
            copyFiles.add(new File(copyFilePath.startsWith(sourcePath.getPath()) ? copyFilePath : sourcePath.getPath() + copyFilePath));
        }
        copy(sourcePath, targetPath, copyFiles);
    }

    public static void copy(File sourcePath, File targetPath, Collection<File> copyFiles) throws IOException {
        for (File source : copyFiles) {
            File target = new File(source.getPath().replace(sourcePath.getPath(), targetPath.getPath()));
            if (!source.exists()) {
                continue;
            }
            if (!target.getParentFile().exists()) {
                target.getParentFile().mkdirs();
            }
            Files.copy(source.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public static File createDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static File createFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static void main(String[] args) throws Exception {
        File file = new File("D:\\Program Files (x86)\\JetBrains\\IntelliJ IDEA 15.0.3\\build.txt");
        System.out.println(file.getPath());
        System.out.println(file.getAbsolutePath());
        System.out.println(file.toPath());
        System.out.println(file.getParent());
        System.out.println(file.getParentFile());

//        List<String> dirs = extract(Arrays.asList("/Users/daibing/Downloads/32jre/rt.create"));
//        System.out.println("dirs: " + dirs);

//        List<String> jars = create(Arrays.asList("D:\\temp\\clean\\lib\\rt"));
//        System.out.println("jars: " + jars);

    }
}
