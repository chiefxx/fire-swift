package next.fire.boot.swift.utils.traffic.out.processor;

import com.talkyun.utils.Looper;
import com.talkyun.utils.LooperPool;
import next.fire.boot.swift.utils.traffic.out.TrafficOutManager;
import next.fire.boot.swift.utils.traffic.out.processor.worker.TrafficOutWorkerFactory;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutData;
import next.fire.boot.swift.utils.traffic.out.processor.model.TrafficOutPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by daibing on 2021/11/29.
 */
public class TrafficOutReleaseTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(TrafficOutReleaseTask.class);
    private final TrafficOutPolicy policy;
    private final BlockingQueue<TrafficOutData> transportQueue;
    private final BlockingQueue<TrafficOutData> resultQueue;
    private final int limitNumPerSecond;
    private Looper reader;
    private LooperPool transport;

    public TrafficOutReleaseTask(TrafficOutPolicy policy) {
        this.policy = policy;
        this.limitNumPerSecond = policy.getCallLimit() / (policy.getPeriodSeconds());
        this.transportQueue = new ArrayBlockingQueue<>(this.limitNumPerSecond);
        this.resultQueue = new ArrayBlockingQueue<>(this.limitNumPerSecond);
    }

    public void startup() {
        Object bean = TrafficOutManager.getBean(policy.getClazz());
        Method method = TrafficOutManager.getMethod(policy.getClazz(), policy.getMethod(), policy.getArgNumber());
        reader = this.buildReader(policy);
        transport = this.buildTransport(policy, bean, method);
        reader.start();
        transport.startup();
    }

    public void shutdown() {
        if (transport != null) {
            transport.shutdown();
        }
        if (reader != null) {
            reader.shutdown();
        }
    }

    private Looper buildReader(TrafficOutPolicy policy) {
        return new Looper(policy.getCode() + "-reader", 50, 1000) {
            private long lastPeriodTime = System.currentTimeMillis();
            private int waveTotal = 0;
            @Override
            protected void loop() throws Throwable {
                // 1. 波次抓取数据：一个波次数据可以发送多个流控周期（1s）
                List<TrafficOutData> waveDataList = TrafficOutWorkerFactory.get(TrafficOutManager.getWorkerClassName()).listByPolicy(policy);
                if (waveDataList == null || waveDataList.isEmpty()) {
                    return;
                }

                // 2. 流控策略限流：每个周期等处理完成再继续下一个周期
                int periodTotal = 0;
                List<TrafficOutData> failedDataList = new ArrayList<>(limitNumPerSecond);
                for (TrafficOutData waveData : waveDataList) {
                    // 2.1：检查需要重发的数据：肯定少于频率限制，可以直接加入传输队列
                    for (TrafficOutData failedData : failedDataList) {
                        transportQueue.put(failedData);
                        periodTotal++;
                    }
                    failedDataList.clear();

                    // 2.2: 向传输队列写入数据：达到每个周期的限制
                    if (periodTotal < limitNumPerSecond) {
                        transportQueue.put(waveData);
                        periodTotal++;
                        if (periodTotal < limitNumPerSecond) {
                            continue;
                        }
                        waveTotal += periodTotal;
                        periodTotal = 0;
                    }

                    // todo：是否不调用removeList，直接将从结果队列读取的数据包返回给业务方，带上错误信息

                    // 2.3: 读取结果队列的数据，不满足错误重试模式的数据也直接删除了
                    List<TrafficOutData> transportResultList = blockingReadQueue(resultQueue, limitNumPerSecond);
                    List<Long> removeIds = new ArrayList<>(transportResultList.size());
                    for (TrafficOutData result : transportResultList) {
                        if (result.isError() && isBlank(policy.getErrorRetryPattern()) && result.getErrorMsg().contains(policy.getErrorRetryPattern())) {
                            failedDataList.add(result);
                        } else {
                            removeIds.add(result.getId());
                        }
                    }
                    transportResultList.clear();
                    TrafficOutWorkerFactory.get(TrafficOutManager.getWorkerClassName()).removeList(removeIds);
                    removeIds.clear();

                    // 2.4：检查是否过了一个周期1s避免流量超标：否则就线程等待到1s以上
                    if (lastPeriodTime + 1000 > System.currentTimeMillis()) {
                        doWait(lastPeriodTime + 1000 - System.currentTimeMillis());
                    }
                    lastPeriodTime = System.currentTimeMillis();
                }

                // 3. 波次数据扫尾：一个波次最后一部分数据达不到limitNumPerSecond的场景，这个时候肯定不会再超标了，返回错误的数据也直接删除了
                if (periodTotal > 0) {
                    waveTotal += periodTotal;
                    List<TrafficOutData> transportResultList = blockingReadQueue(resultQueue, periodTotal);
                    List<Long> removeIds = new ArrayList<>(transportResultList.size());
                    for (TrafficOutData result : transportResultList) {
                        removeIds.add(result.getId());
                    }
                    transportResultList.clear();
                    TrafficOutWorkerFactory.get(TrafficOutManager.getWorkerClassName()).removeList(removeIds);
                    removeIds.clear();
                }

                // 4. 释放波次资源
                LOGGER.info("wave ok: waveCount={}, usedTotal={}", waveDataList.size(), waveTotal);
                waveDataList.clear();
            }

            @Override
            protected void loopThrowable(Throwable t) {
                LOGGER.warn("startup reader failed: code={}, error=", policy.getCode(), t);
            }
        };
    }

    private LooperPool buildTransport(TrafficOutPolicy policy, Object bean, Method method) {
        Type[] types = method.getGenericParameterTypes();
        return new LooperPool(policy.getTransportThreadNum(), new LooperPool.LooperBuilder() {
            @Override
            public Looper build() {
                return new Looper(policy.getCode(), 50, 1000) {
                    @Override
                    protected void loop() throws Throwable {
                        // 执行业务逻辑：队列阻塞的方式操作数据
                        TrafficOutData trafficData = transportQueue.take();
                        try {
                            TrafficOutManager.closeTraffic();
                            Object[] args = restoreArgs(method, types, trafficData.getArgMap());
                            method.invoke(bean, args);
                        } catch (Throwable t) {
                            LOGGER.warn("task invoke failed: data={}, error=", trafficData.getId(), t);
                            trafficData.setError(true);
                            trafficData.setErrorMsg(t.getMessage());
                        } finally {
                            resultQueue.put(trafficData);
                        }
                    }

                    @Override
                    protected void loopThrowable(Throwable t) {
                        LOGGER.warn("startup transport failed: code={}, error=", policy.getCode(), t);
                    }
                };
            }
        });
    }

    /**
     * 根据方法和消息体组装参数
     */
    private Object[] restoreArgs(Method method, Type[] types, Map<String, Object> argMap) {
        Object[] args = new Object[types.length];
        int k = 0;
        // todo：注意Method获取到的参数名称是arg0之类的，所以先用顺序来获取字段，后续验证一下是否可以通过字段名词来获取字段
        // todo：针对for循环的消息体，需要注意构造方式
        for (Map.Entry<String, Object> entry : argMap.entrySet()) {
            args[k++] = entry.getValue();
        }
        return args;
    }

    private List<TrafficOutData> blockingReadQueue(BlockingQueue<TrafficOutData> queue, int size) throws InterruptedException {
        List<TrafficOutData> dataList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            dataList.add(queue.take());
        }
        return dataList;
    }

    private static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

    private void doWait(long time) {
        if (time <= 0) {
            return;
        }
        synchronized (this) {
            try {
                this.wait(time);
            } catch (InterruptedException e) {
                e.fillInStackTrace();
            }
        }
    }
}
