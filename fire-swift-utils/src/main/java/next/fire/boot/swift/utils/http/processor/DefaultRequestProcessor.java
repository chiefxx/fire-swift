package next.fire.boot.swift.utils.http.processor;

import com.talkyun.utils.UrlFetch;
import next.fire.boot.swift.utils.codec.ByteHelper;
import next.fire.boot.swift.utils.codec.HmacSHA256;
import next.fire.boot.swift.utils.codec.MD5Helper;
import next.fire.boot.swift.utils.http.RequestHandler;
import next.fire.boot.swift.utils.http.processor.model.RequestTemplate;
import next.fire.boot.swift.utils.http.processor.model.RestSettings;
import next.fire.boot.swift.utils.http.processor.worker.RestWorker;
import next.fire.boot.swift.utils.http.processor.worker.RestWorkerFactory;
import org.yaml.snakeyaml.Yaml;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by daibing on 2021/11/13.
 */
public class DefaultRequestProcessor {
    private static final String EMPTY_SIGN = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

    private final ConcurrentMap<String, RequestTemplate> templateUrl2Template = new ConcurrentHashMap<>();

    public String registerTemplate(RequestTemplate template) {
        templateUrl2Template.put(template.getTemplateUrl(), template);
        return template.getTemplateUrl();
    }

    public String removeTemplate(String templateUrl) {
        RequestTemplate template = templateUrl2Template.remove(templateUrl);
        return template != null ? template.getTemplateUrl() : null;
    }

    public <T> T execute(String templateUrl, RequestHandler<? extends T> handler) {
        // loading request template
        RequestTemplate template = getTemplate(templateUrl);
        return this.execute(template, handler);
    }

    public <T> T execute(RequestTemplate requestTemplate, RequestHandler<? extends T> requestHandler) {
        RestSettings restSettings = new RestSettings();
        restSettings.setRequestSettings(new RestSettings.RequestSettings());
        restSettings.setResponseSettings(new RestSettings.ResponseSettings());
        return this.execute(restSettings, requestTemplate, requestHandler);
    }

    public <T> T execute(RestSettings restSettings, RequestTemplate requestTemplate, RequestHandler<? extends T> requestHandler) {
        // 1. loading request worker
        RestWorker worker = RestWorkerFactory.get(restSettings.getRestWorkerClassName());

        // 2. prepare request body
        String body = worker.buildBody(restSettings, requestTemplate, requestHandler.buildBizParams(), requestHandler.buildAccessParams());
        String bodySign = this.bodySign(restSettings.getRequestSettings().getBodySignAction(), body);

        // 3. prepare request url
        String url = worker.buildUrl(restSettings, requestTemplate, requestHandler.buildBizParams(), requestHandler.buildAccessParams(), bodySign);

        // 4. prepare request header
        Map<String, String> header = worker.buildHeader(restSettings, requestTemplate, requestHandler.buildBizParams(), requestHandler.buildAccessParams(), bodySign, url);

        // 5. send http request
        Map<String, String> result = worker.execute(restSettings, requestTemplate, url, header, body);

        // 6. parse response data
        Map<String, Object> output = worker.parse(restSettings, requestTemplate, result);
        return requestHandler.extractData(output);
    }

    private RequestTemplate getTemplate(String templateUrl) {
        RequestTemplate template = templateUrl2Template.get(templateUrl);
        if (template != null) {
            return template;
        }
        synchronized (templateUrl2Template) {
            template = templateUrl2Template.get(templateUrl);
            if (template != null) {
                return template;
            }
            template = loadTemplate(templateUrl);
            templateUrl2Template.put(templateUrl, template);
            return template;
        }
    }

    /**
     * templateUrl直接输入： /template/aliyun/ecs/ecs-create.yaml
     * Spring: new UrlFetch().fetch(new ClassPathResource(templateUrl).getURL());
     */
    private RequestTemplate loadTemplate(String templateUrl) {
        String fetch = new UrlFetch().fetch(this.getClass().getResource(templateUrl));
        if (isBlank(fetch)) {
            throw new RuntimeException("Not found local request template by " + templateUrl);
        }
        return new Yaml().loadAs(fetch, RequestTemplate.class);
    }

    private String bodySign(RestSettings.SignActionEnum bodySignAction, String body) {
        switch (bodySignAction) {
            case SHA256:
                if(isBlank(body)){
                    return EMPTY_SIGN;
                }
                return ByteHelper.byteToHex(HmacSHA256.sha256(body));
            case MD5:
                return MD5Helper.md5(body);
            default:
                return null;
        }
    }

    private boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

}
