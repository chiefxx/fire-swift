package next.fire.boot.swift.utils.traffic.out.processor.worker;

import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by daibing on 2021/11/21.
 */
public class TrafficOutWorkerFactory {
    private static final ConcurrentMap<String, TrafficOutWorker> IMPL_MAPPING = new ConcurrentHashMap<>(2);

    static {
        ServiceLoader<TrafficOutWorker> serviceLoader = ServiceLoader.load(TrafficOutWorker.class);
        for (TrafficOutWorker worker : serviceLoader) {
            try {
                register(worker);
            } catch (Exception e) {
                // ignore error when load spring bean
            }
        }
    }

    /**
     * 如果实现类是Spring Bean，需要手工注册或者使用TrafficOutWorkerSpringFactory注册
     */
    public static void register(TrafficOutWorker worker) {
        String clazz = worker.getClass().getName();
        IMPL_MAPPING.put(clazz, worker);
    }

    public static TrafficOutWorker get(String workerClassName) {
        TrafficOutWorker worker = IMPL_MAPPING.get(workerClassName);
        if (worker == null) {
            throw new RuntimeException("Not found TrafficOutWorker impl by workerClassName: " + workerClassName);
        }
        return worker;
    }
}
