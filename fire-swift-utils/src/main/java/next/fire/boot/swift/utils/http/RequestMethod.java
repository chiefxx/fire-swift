package next.fire.boot.swift.utils.http;

/**
 * Created by daibing on 2021/12/1.
 */
public enum RequestMethod {
    GET,
    PUT,
    POST,
    DELETE,
    ;

    public static RequestMethod get(String raw) {
        try {
            return RequestMethod.valueOf(raw.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }
}
