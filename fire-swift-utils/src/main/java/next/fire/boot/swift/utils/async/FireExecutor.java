package next.fire.boot.swift.utils.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 线程执行器
 * Created by daibing on 2020/3/8.
 */
public class FireExecutor {
    private static final Logger LOGGER = LoggerFactory.getLogger(FireExecutor.class);
    private static final Integer STAT_THRESHOLD = 100;
    private final AtomicInteger count = new AtomicInteger(0);
    private final ThreadPoolExecutor executor;
    private final Integer corePoolSize;
    private final Integer maximumPoolSize;
    private final Integer queueSize;
    private final String threadNamePrefix;

    public FireExecutor(Integer corePoolSize, Integer maximumPoolSize, Integer queueSize, String threadNamePrefix) {
        this.corePoolSize = corePoolSize == null ? 1 : corePoolSize;
        this.maximumPoolSize = maximumPoolSize == null ? 2 : maximumPoolSize;
        this.queueSize = queueSize == null ? 10 : queueSize;
        this.threadNamePrefix = this.isEmpty(threadNamePrefix) ? "fire-exec" : threadNamePrefix;
        this.executor = this.init();
    }

    private ThreadPoolExecutor init() {
        return new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                60L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(queueSize),
                new ThreadFactory() {
                    final Random random = new Random();
                    @Override
                    public Thread newThread(Runnable r) {
                        Thread thread = new Thread(r);
                        thread.setName(threadNamePrefix + this.random.nextInt());
                        return thread;
                    }
                },
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    public void shutdown() {
        this.printPoolStatus(count.get());
        this.executor.shutdown();
    }

    public <T> void executeTask(Runnable task) {
        this.countNumber();
        this.executor.execute(task);
    }

    public <T> Future<T>  submitTask(Callable<T> task) {
        this.countNumber();
        return this.executor.submit(task);
    }

    public <T> T runTask(Callable<T> task) {
        this.countNumber();
        Future<T> future = this.executor.submit(task);
        try {
            return future.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void printPoolStatus() {
        this.printPoolStatus(count.get());
    }

    private void printPoolStatus(int count) {
        LOGGER.info("ThreadName={}, Count={}, PoolSize={}, LargestPoolSize={}, TaskCount={}, CompletedTaskCount={}, ActiveCount={}, workQueueSize={}",
                threadNamePrefix,
                count,
                executor.getPoolSize(),
                executor.getLargestPoolSize(),
                executor.getTaskCount(),
                executor.getCompletedTaskCount(),
                executor.getActiveCount(),
                executor.getQueue().size()
        );
        // temp test ...
        // System.out.println("countNumber2: " + count);
    }

    private void countNumber() {
        if (STAT_THRESHOLD.compareTo(count.incrementAndGet()) <= 0) {
            this.printPoolStatus(count.getAndSet(0));
        }
        // temp test ...
        // System.out.println("countNumber1: " + count.get());
    }

    private boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static void main(String[] args) throws Exception {
        System.out.println("main start to do...");
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        final AtomicInteger number = new AtomicInteger(0);

        FireExecutor executor = new FireExecutor(2, 4, 128, "my-exec");
        Callable<Void> callable = new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                System.out.println(String.format("runnable time=%s, number=%s", sdf.format(new Date()), number.incrementAndGet()));
                return null;
            }
        };
        System.out.println("start to run...");
        for (int i = 0; i < 302; i++) {
            executor.submitTask(callable);
        }
        System.in.read();
    }
}
