package next.fire.boot.swift.utils.http.processor.model;

import java.util.List;

/**
 * Created by daibing on 2022/1/19.
 */
public class RestSettings {
    private String restWorkerClassName = "next.fire.boot.swift.utils.http.processor.worker.impl.AliyunFormRestWorker";
    private RequestSettings requestSettings;
    private ResponseSettings responseSettings;

    public String getRestWorkerClassName() {
        return restWorkerClassName;
    }

    public void setRestWorkerClassName(String restWorkerClassName) {
        this.restWorkerClassName = restWorkerClassName;
    }

    public RequestSettings getRequestSettings() {
        return requestSettings;
    }

    public void setRequestSettings(RequestSettings requestSettings) {
        this.requestSettings = requestSettings;
    }

    public ResponseSettings getResponseSettings() {
        return responseSettings;
    }

    public void setResponseSettings(ResponseSettings responseSettings) {
        this.responseSettings = responseSettings;
    }

    public static class RequestSettings {
        /** http请求尝试次数 */
        private int tryTimes = 1;
        /** http请求超时时长 */
        private int timeoutMillis = 3000;
        /** http请求body中null值：是否移除 */
        private boolean removeNullValue = true;
        /** 对body做验签，方便在header或url中加入body的验证信息 */
        private SignActionEnum bodySignAction = SignActionEnum.NONE;

        public int getTryTimes() {
            return tryTimes;
        }

        public void setTryTimes(int tryTimes) {
            this.tryTimes = tryTimes;
        }

        public int getTimeoutMillis() {
            return timeoutMillis;
        }

        public void setTimeoutMillis(int timeoutMillis) {
            this.timeoutMillis = timeoutMillis;
        }

        public boolean isRemoveNullValue() {
            return removeNullValue;
        }

        public void setRemoveNullValue(boolean removeNullValue) {
            this.removeNullValue = removeNullValue;
        }

        public SignActionEnum getBodySignAction() {
            return bodySignAction;
        }

        public void setBodySignAction(SignActionEnum bodySignAction) {
            this.bodySignAction = bodySignAction;
        }
    }

    public static class ResponseSettings {
        /** 是否检查响应消息 */
        private boolean checkBody = false;
        /** 获取消息响应头中的字段 */
        private List<String> headerKeys;

        public boolean isCheckBody() {
            return checkBody;
        }

        public void setCheckBody(boolean checkBody) {
            this.checkBody = checkBody;
        }

        public List<String> getHeaderKeys() {
            return headerKeys;
        }

        public void setHeaderKeys(List<String> headerKeys) {
            this.headerKeys = headerKeys;
        }
    }

    public static enum SignActionEnum {
        NONE, SHA256, MD5, ;
    }

}
