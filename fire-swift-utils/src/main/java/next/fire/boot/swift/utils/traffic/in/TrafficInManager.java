package next.fire.boot.swift.utils.traffic.in;

import next.fire.boot.swift.utils.system.EnvHelper;

/**
 * Created by daibing on 2021/12/4.
 */
public class TrafficInManager {
    private static volatile String workerClassName = "next.fire.boot.swift.utils.traffic.in.processor.worker.impl.DefaultTrafficInWorker";
    private static volatile boolean switchOn = true;

    static {
        String trafficSwitchOn = EnvHelper.getEnvSettingIfPresent("traffic.switch.on", "traffic_switch_on");
        if (!isBlank(trafficSwitchOn)) {
            switchOn = Boolean.parseBoolean(trafficSwitchOn);
        }
    }

    public static boolean isSwitchOn() {
        return switchOn;
    }

    public static String getWorkerClassName() {
        return workerClassName;
    }

    public static void setWorkerClassName(String workerClassName) {
        TrafficInManager.workerClassName = workerClassName;
    }

    private static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

}
