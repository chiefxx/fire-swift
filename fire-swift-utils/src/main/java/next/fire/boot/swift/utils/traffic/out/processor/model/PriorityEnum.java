package next.fire.boot.swift.utils.traffic.out.processor.model;

/**
 * 优先级
 * Created by daibing on 2018/8/16.
 */
public enum PriorityEnum {
    NORMAL,
    QUICK,
    URGENT,
    EXTRA_URGENT,
    ;
    public static PriorityEnum get(String raw) {
        try {
            return PriorityEnum.valueOf(raw.toUpperCase());
        } catch (Exception e) {
            return NORMAL;
        }
    }

    public Integer getPriorityNumber() {
        switch (this) {
            case EXTRA_URGENT:
                return 1;
            case URGENT:
                return 4;
            case QUICK:
                return 8;
            case NORMAL:
                return 12;
            default:
                return 12;
        }
    }

}
