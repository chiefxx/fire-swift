package next.fire.boot.swift.utils.http.processor.worker.impl;

import next.fire.boot.swift.utils.codec.URLEncodeHelper;
import next.fire.boot.swift.utils.converter.json.JacksonHelper;
import next.fire.boot.swift.utils.http.RequestHelper;
import next.fire.boot.swift.utils.http.RequestMethod;
import next.fire.boot.swift.utils.http.processor.model.RequestTemplate;
import next.fire.boot.swift.utils.http.processor.model.RestSettings;
import next.fire.boot.swift.utils.http.processor.worker.RestWorker;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xiongjm
 * @date 2022/1/17
 */
public abstract class AbstractRestWorker implements RestWorker {

    @Override
    public abstract String buildBody(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams);

    @Override
    public String buildUrl(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams, String bodySign) {
        return template.getUri();
    }

    @Override
    public Map<String, String> buildHeader(RestSettings settings, RequestTemplate template, Map<String, Object> bizParams, Map<String, Object> accessParams, String bodySign, String url) {
        return template.getHeaders();
    }

    @Override
    public Map<String, String> execute(RestSettings settings, RequestTemplate template, String url, Map<String, String> header, String body) {
        RequestMethod method = template.getMethod();
        int tryTimes = settings.getRequestSettings().getTryTimes();
        if (RequestMethod.GET == method && !isBlank(body)) {
            url += "?" + body;
        }
        RequestHelper httpClient = new RequestHelper(url, settings.getRequestSettings().getTimeoutMillis());
        for (int i = 0; i < tryTimes; i++) {
            try {
                switch (method) {
                    case GET:
                        return httpClient.get(header);
                    case PUT:
                        return httpClient.put(header, body);
                    case POST:
                        return httpClient.post(header, body);
                    case DELETE:
                        return httpClient.delete(header, body);
                    default:
                        return Collections.emptyMap();
                }
            } catch (Throwable t) {
                if (tryTimes - 1 == i) {
                    throw new RuntimeException(String.format("RestWorker http %s failed: url：%s, exception: %s", method, url, t));
                }
            }
        }
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Object> parse(RestSettings settings, RequestTemplate template, Map<String, String> result) {
        if (settings.getResponseSettings() == null || settings.getResponseSettings().getHeaderKeys() == null) {
            return JacksonHelper.toMap(result.get(RequestHelper.RESPONSE_BODY));
        }
        Map<String, Object> map = new HashMap<>(result.size());
        for (String key : settings.getResponseSettings().getHeaderKeys()) {
            if (result.containsKey(key)) {
                map.put(key, result.get(key));
            }
        }
        map.putAll(JacksonHelper.toMap(result.get(RequestHelper.RESPONSE_BODY)));
        return map;
    }

    public static Object getParamsValue(String key, Map<String, Object> bizParams, Map<String, Object> accessParams) {
        if (bizParams.containsKey(key)) {
            return bizParams.get(key);
        }
        if (accessParams.containsKey(key)) {
            return accessParams.get(key);
        }
        return null;
    }

    public static String map2String(Map<String, String> paramsMap, boolean encode) {
        if (paramsMap.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
            sb.append(encode ? URLEncodeHelper.percentEncode(entry.getKey()) : entry.getKey())
                    .append("=")
                    .append(encode ? URLEncodeHelper.percentEncode(entry.getValue()) : entry.getValue())
                    .append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

}
