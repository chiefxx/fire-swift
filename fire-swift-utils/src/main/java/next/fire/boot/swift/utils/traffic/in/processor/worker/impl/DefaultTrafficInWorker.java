package next.fire.boot.swift.utils.traffic.in.processor.worker.impl;

import next.fire.boot.swift.utils.traffic.in.processor.model.TrafficInPolicy;
import next.fire.boot.swift.utils.traffic.in.processor.model.TrafficInStats;
import next.fire.boot.swift.utils.traffic.in.processor.worker.TrafficInWorker;

import java.util.Map;

/**
 * Created by daibing on 2021/12/4.
 */
public class DefaultTrafficInWorker implements TrafficInWorker {

    // todo

    @Override
    public TrafficInPolicy getPolicy(String policyCode) {
        return null;
    }

    @Override
    public TrafficInPolicy getPolicy(String clazz, String method, int argNumber, Map<String, Object> argMap) {
        return null;
    }

    @Override
    public int insertOrUpdate(TrafficInStats stats) {
        return 0;
    }

    @Override
    public boolean acquireBatch(String trafficInPolicyCode, long floorBoundByPeriod, int applyNum) {
        return false;
    }

    /**
     * 数据库范例: dao
     */

    /**
    @Mapper
    public interface TrafficInStatsDao {

        int insertOrUpdate(@Param("stats") TrafficInStats stats);

        int acquireBatch(@Param("trafficInPolicyCode") String trafficInPolicyCode, @Param("floorBoundByPeriod") long floorBoundByPeriod,
                         @Param("applyNum") int applyNum, @Param("lastModify") Date lastModify);
    }
    */

    /**
     <insert id="insertOrUpdate">
         INSERT INTO traffic_in_stats (
             `id`,
             `traffic_in_policy_code`,
             `period_minutes`,
             `maximum_limit`,
             `floor_bound_by_period`,
             `stats_apply_total`,
             `stats_used_total`,
             `create_time`,
             `last_modify`
         ) VALUES (
             #{stats.id},
             #{stats.trafficInPolicyCode},
             #{stats.periodMinutes},
             #{stats.maximumLimit},
             #{stats.floorBoundByPeriod},
             #{stats.statsApplyTotal},
             #{stats.statsUsedTotal},
             #{stats.createTime},
             #{stats.lastModify}
         ) ON DUPLICATE KEY UPDATE
             `period_minutes` = #{stats.periodMinutes},
             `maximum_limit` = #{stats.maximumLimit},
             `last_modify` = #{stats.lastModify};
     </insert>

     <update id="acquireBatch">
         UPDATE traffic_in_stats
         SET stats_apply_total = stats_apply_total + #{applyNum}, `last_modify` = #{lastModify}
         WHERE traffic_in_policy_code = #{trafficInPolicyCode} AND floor_bound_by_period = #{floorBoundByPeriod}
         AND stats_apply_total + #{applyNum} &lt;= maximum_limit
     </update>
     */
}
