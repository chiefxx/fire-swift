package next.fire.boot.swift.utils.traffic.out.processor.worker;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * Created by daibing on 2021/12/3.
 */
public class TrafficOutWorkerSpringFactory extends TrafficOutWorkerFactory implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof TrafficOutWorker) {
            register((TrafficOutWorker) bean);
        }
        return bean;
    }
}
