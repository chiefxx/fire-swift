package next.fire.boot.swift.utils.http.processor.conf;

import next.fire.boot.swift.utils.http.processor.model.CloudDiskTypeEnum;
import next.fire.boot.swift.utils.http.processor.model.PayTypeEnum;

/**
 * @author xiongjm
 * @date 2021/11/3 17:59
 */
public class EcsConfig {
    /**
     * id
     */
    private String providerId;
    /**
     * 用户id
     */
    private String accessKeyId;
    /**
     * 用户secret
     */
    private String accessKeySecret;
    /**
     * 镜像id
     */
    private String imageId;
    /**
     * 安全组ID
     */
    private String securityGroupId;
    /**
     * 交换机id
     */
    private String switchId;
    /**
     * 实例类型
     */
    private String instanceType;
    /**
     * 系统盘的云盘种类
     */
    private CloudDiskTypeEnum diskCategory;
    /**
     * 系统盘大小，单位为GiB。取值范围：20~500
     */
    private int diskSize;
    /**
     * 实例的密码
     */
    private String password;
    /**
     * 实例的付费方式
     */
    private PayTypeEnum payType;
    /**
     * 购买资源的时长，单位为：月
     * 当实例的付费方式为包年包月时才生效且为必选值
     */
    private String period;
    /**
     * 配置文件路径
     */
    private String configPath = "/conf/ecs.json";

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public String getSwitchId() {
        return switchId;
    }

    public void setSwitchId(String switchId) {
        this.switchId = switchId;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public CloudDiskTypeEnum getDiskCategory() {
        return diskCategory;
    }

    public void setDiskCategory(CloudDiskTypeEnum diskCategory) {
        this.diskCategory = diskCategory;
    }

    public int getDiskSize() {
        return diskSize;
    }

    public void setDiskSize(int diskSize) {
        this.diskSize = diskSize;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PayTypeEnum getPayType() {
        return payType;
    }

    public void setPayType(PayTypeEnum payType) {
        this.payType = payType;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }
}
