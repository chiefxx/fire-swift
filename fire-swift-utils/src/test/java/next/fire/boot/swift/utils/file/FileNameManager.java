package next.fire.boot.swift.utils.file;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.io.File.separator;

public class FileNameManager {
    private static final FileNameManager INSTANCE = new FileNameManager();
    private Pattern pattern = Pattern.compile("([0-9]\\.[0-9]\\.[0-9])[\\s\\S]*|([0-9]\\.[0-9])[\\s\\S]*\\.jar");
    // PATTERN: daemon-client-1.0.5[SNAPSHOT].jar => prefix:{daemon-client}-version:{1.0.5}.suffix:{jar}

    public static FileNameManager get() {
        return INSTANCE;
    }

    public String getLatestFileName(String path, String regex){
        final Pattern pattern = Pattern.compile(regex);
        String[] files = new File(path).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(name).matches();
            }
        });
        if (files == null || files.length == 0) {
            return null;
        }

        List<String> list = Arrays.asList(files);
        Collections.sort(list);
        return list.get(list.size() - 1);
    }

    public String getLatestFile(String path, String regex) {
        final Pattern pattern = Pattern.compile(regex);
        String[] files = new File(path).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(name).matches();
            }
        });
        if (files == null || files.length == 0) {
            return null;
        }

        List<String> list = Arrays.asList(files);
        Collections.sort(list);
        return new File(path + File.separator + list.get(list.size() - 1)).getAbsolutePath();
    }

    public String getVersionName(String simpleName, String version) {
        int pos = simpleName.lastIndexOf(".");
        if (pos > 0 && !simpleName.endsWith(".")) {
            return simpleName.substring(0, pos) + "-" + version + simpleName.substring(pos);
        }
        return simpleName + "-" + version;
    }

    public String getNameByURL(String url) {
        int pos = url.lastIndexOf("/");
        return url.substring(pos + 1);
    }

    public String getParentPathByURL(String url) {
        int pos = url.lastIndexOf("/");
        return url.substring(0, pos + 1);
    }

    public String getNameRegexByUrl(String url) {
        int pos = url.lastIndexOf("/");
        return url.substring(pos + 1).replace("-*-SNAPSHOT", ".*");
    }

    public String getVersionNameByURL(String url, String version) {
        return getVersionName(getNameByURL(url), version);
    }

    public String getVersionByFullName(String fullName) {
        Matcher m = pattern.matcher(fullName);
        if (m.find()) {
            String name = m.group();
            return name.substring(0, name.length() - 4);
        }
        return null;
    }

    public String getVersionByFullName(String fullName, String suffix) {
        return this.getVersionByFullName(fullName.replace(suffix, ".jar"));
    }

    public String getVersionByPath(String path) {
        int pos = path.lastIndexOf(separator);
        if (pos > 0 && !path.endsWith(separator)) {
            return getVersionByFullName(path.substring(pos + 1));
        }
        return null;
    }

    public int getVersionIndex(String version) {
        StringBuilder sb = new StringBuilder();
        String[] ver = version.split("\\.");
        for (String s : ver) {
            if (s.length() >= 3) {
                sb.append(s);
                continue;
            }
            for (int i = s.length(); i < 3; i++) {
                sb.append("0");
            }
            sb.append(s);
        }
        return Integer.parseInt(sb.toString());
    }

    public boolean compareVersion(String sourceVersion, String targetVersion) {
        return this.getVersionIndex(sourceVersion) >= this.getVersionIndex(targetVersion);
    }

    public static void main(String[] args) {
        FileNameManager fileNameManager = new FileNameManager();
        String name = "daemon-client-1.0-SNAPSHOT.jar";

        System.out.println(fileNameManager.getVersionByFullName(name));

//        Pattern pattern = Pattern.compile("(([0-9]\\.[0-9]\\.[0-9])|([0-9]\\.[0-9]))[\\s\\S]*\\.jar");
//        Matcher m = pattern.matcher(name);
//        while (m.find()) {
//            System.out.println("分组名称:匹配的值");
//            System.out.println("name:" + m.group());
//            // System.out.println("version:" + m.);
//        }
//
//        String name2 = "daemon-client-1.0.5.jar";
//        Matcher m2 = pattern.matcher(name2);
//        while (m2.find()) {
//            System.out.println("分组名称:匹配的值");
//            System.out.println("name:" + m2.group());
//            // System.out.println("version:" + m.);
//        }

        System.out.println("\n----------------");
        String name2 = "1.0.0-rc";
        Pattern pattern = Pattern.compile("(([0-9]\\.[0-9]\\.[0-9])|([0-9]\\.[0-9]))[\\s\\S]");
        Matcher m = pattern.matcher(name2);
        while (m.find()) {
            System.out.println("分组名称:匹配的值");
            System.out.println("name:" + m.group());
            // System.out.println("version:" + m.);
        }
    }
}