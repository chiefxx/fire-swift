package next.fire.boot.swift.utils.http.skeleton.aliyun.conf;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by daibing on 2021/11/13.
 */
public class AliyunLogConfigManager {
    private static final AliyunLogConfigManager INSTANCE = new AliyunLogConfigManager();
    private final ConcurrentMap<String, AliyunLogConfig> configMap = new ConcurrentHashMap<>(1);

    public static AliyunLogConfigManager get() {
        return INSTANCE;
    }

    public AliyunLogConfig select(String logStoreCode) {
        return configMap.get(logStoreCode);
    }

    public void reload(List<AliyunLogConfig> aliyunLogConfigList) {
        synchronized (configMap) {
            configMap.clear();
            for (AliyunLogConfig aliyunLogConfig : aliyunLogConfigList) {
                configMap.put(aliyunLogConfig.getLogStoreCode(), aliyunLogConfig);
            }
        }
    }

}
