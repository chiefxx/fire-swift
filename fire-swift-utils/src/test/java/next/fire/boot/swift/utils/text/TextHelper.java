package next.fire.boot.swift.utils.text;

import com.talkyun.utils.UrlFetch;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by daibing on 2019/4/19.
 */
public class TextHelper {

    public static String safeCut(String raw, int length) {
        if (raw == null) {
            return null;
        }

        if (raw.length() <= length) {
            return raw;
        }
        return raw.substring(0, length);
    }

    public static String clean(String raw) {
        if (raw == null) {
            return null;
        }
        String regEx = "[`~!@#$%^&*()+=\\-|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(raw);
        return matcher.replaceAll(" ").trim();
    }

    public static String decode(String text, String charset) {
        if (!isBlank(text)) {
            try {
                return URLDecoder.decode(text, charset);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return text;
    }

    public static String templateReplace(Map<String, Object> data, String templateUrl) {
        String template = (new UrlFetch()).fetch(templateUrl);
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            String holder = String.format("\\{\\{%s\\}\\}", entry.getKey());
            template = template.replaceAll(holder, entry.getValue() == null ? "" : entry.getValue().toString());
        }
        return template;
    }

    private static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

    public static void main(String[] args) {
        Map<String, Object> content = new HashMap<>();
        content.put("waybillNo", "waybillNo");
        content.put("recipientDetail", "this.getAddress(receiver.getAddress())");
        content.put("recipientName", "receiver.getName()");
        content.put("recipientPhone", "receiver.getPhone()");
        content.put("sourcetSortCenterName", "paramsObj.get()");
        content.put("originalCrossCode", "33");
        content.put("originalTabletrolleyCode", "33");
        content.put("targetSortCenterName", "555");
        content.put("destinationCrossCode", "888");
        content.put("destinationTabletrolleyCode", "5555");
        content.put("siteName", "3333");
        content.put("road", "6666");
        content.put("senderDetail", "6666");
        content.put("senderName", "4444");
        content.put("senderPhone", "3333");
        content.put("templateURL", "33333");
        content.put("waybillCode", "waybillNo");
        content.put("remark", "remark");
        String result = TextHelper.templateReplace(content, TextHelper.class.getResource("/template/jd.data.json").toString());
        System.out.println("result: " + result);
    }

}
