package next.fire.boot.swift.utils.http.skeleton.aliyun.client;

import next.fire.boot.swift.utils.codec.MD5Helper;
import next.fire.boot.swift.utils.codec.URLEncodeHelper;
import next.fire.boot.swift.utils.converter.json.JacksonHelper;
import next.fire.boot.swift.utils.http.skeleton.AbstractRequestClient;
import next.fire.boot.swift.utils.http.RequestHelper;
import next.fire.boot.swift.utils.http.skeleton.aliyun.conf.AliyunLogConfig;
import next.fire.boot.swift.utils.http.skeleton.aliyun.conf.AliyunLogConfigManager;
import next.fire.boot.swift.utils.http.skeleton.aliyun.helper.AliyunHelper;
import next.fire.boot.swift.utils.http.RequestMethod;

import java.util.*;

/**
 * Created by daibing on 2021/11/13.
 */
public class AliyunLogClient extends AbstractRequestClient {
    public static final String PROJECT_NAME = "PROJECT_NAME";
    public static final String LOG_REQUEST_ID = "x-log-requestid";
    public static final String LOG_PROGRESS = "x-log-progress";
    public static final String LOG_COUNT = "x-log-count";
    public static final String RESPONSE_DATA_LIST = "RESPONSE_DATA_LIST";
    private static final String CONTENT_TYPE = "application/json";

    @Override
    protected byte[] buildRawBody(RequestMethod method, String uri, String providerId, Map<String, Object> bizParams, Map<String, Object> accessParams) {
        return new byte[0];
    }

    @Override
    protected byte[] buildEncodeBody(byte[] rawBody, Map<String, Object> accessParams) {
        return new byte[0];
    }

    @Override
    protected Map<String, String> buildHeader(RequestMethod method, String uri, String providerId, Map<String, Object> bizParams, Map<String, Object> accessParams,
                                              byte[] rawBody, byte[] encodeBody) {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(providerId);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + providerId);
        }
        String logResourceHost = this.getLogResourceHost(config.getAccountEndpoint(), accessParams);
        String md5 = encodeBody.length > 0 ? MD5Helper.md5(encodeBody).toUpperCase() : "";
        String date = AliyunHelper.getGmtString(new Date());
        Map<String, String> header = new TreeMap<>();
        header.put("Content-MD5", md5);
        header.put("Content-Type", CONTENT_TYPE);
        header.put("x-log-bodyrawsize", String.valueOf(rawBody.length));
        header.put("x-log-apiversion", "0.6.0");
        header.put("x-log-signaturemethod", "hmac-sha1");
        header.put("Host", logResourceHost);
        header.put("Date", date);
        return header;
    }

    @Override
    protected String buildSign(RequestMethod method, String uri, String providerId, Map<String, Object> bizParams, Map<String, Object> accessParams,
                               Map<String, String> header) {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(providerId);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + providerId);
        }
        String canonicalizedHeaders = AliyunHelper.getCanonicalizedHeaders(header, "x-log-", "x-acs-");
        return AliyunHelper.headSign(method.name(), header.get("Content-MD5"), header.get("Content-Type"), header.get("Date"),
                canonicalizedHeaders, uri, config.getAccessKeySecret());
    }

    @Override
    protected void addSign2Header(String providerId, Map<String, String> header, String sign) {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(providerId);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + providerId);
        }
        header.put("Authorization", AliyunHelper.getAuthorization("LOG ", config.getAccessKeyId(), sign));
    }

    @Override
    protected String buildUrl(String uri, String providerId, Map<String, Object> bizParams, Map<String, Object> accessParams, String sign) {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(providerId);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + providerId);
        }
        String protocol = AliyunHelper.getProtocol(config.getAccountEndpoint());
        String logResourceHost = this.getLogResourceHost(config.getAccountEndpoint(), accessParams);

        Map<String, String> parameter = getParameter(uri);
        String queryString = this.paramToQueryString(parameter);
        uri = queryString == null ? uri : uri.substring(0, uri.indexOf("?")) + "?" + queryString;
        return protocol + logResourceHost + uri;
    }

    @Override
    protected Map<String, Object> parse(Map<String, String> postResult, Map<String, Object> accessParams) {
        Map<String, Object> output = new HashMap<>(postResult.size());
        output.put(LOG_REQUEST_ID, postResult.get(LOG_REQUEST_ID));
        if (postResult.containsKey(LOG_PROGRESS)) {
            output.put(LOG_PROGRESS, postResult.get(LOG_PROGRESS));
        }
        if (postResult.containsKey(LOG_COUNT)) {
            output.put(LOG_COUNT, postResult.get(LOG_COUNT));
        }
        String body = postResult.get(RequestHelper.RESPONSE_BODY);
        if (accessParams.containsKey(RESPONSE_DATA_LIST) && Boolean.parseBoolean(accessParams.get(RESPONSE_DATA_LIST).toString())) {
            List<Map<String, Object>> list = JacksonHelper.toMapList(body);
            output.put(RequestHelper.RESPONSE_BODY, list);
        } else {
            Map<String, Object> map = JacksonHelper.toMap(body);
            if (map != null && !map.isEmpty()) {
                output.putAll(map);
            }
        }
        return output;
    }

    @Override
    protected boolean checkBody(Map<String, Object> output, boolean skipCheckBodyStatus) {
        return true;
    }

    @Override
    protected boolean checkSign(String providerId, Map<String, String> rawOutput, Map<String, Object> output) {
        return true;
    }

    private Map<String, String> getParameter(String uri) {
        int beginIndex = uri.indexOf("?");
        HashMap<String, String> params = new HashMap<>();
        if (beginIndex > 0) {
            String contents = uri.substring(uri.indexOf('?') + 1);
            String[] paramPairArr = contents.split("&");
            for (String paramPair : paramPairArr) {
                String paramKey = paramPair.substring(0, paramPair.indexOf("="));
                String paramValue = paramPair.substring(paramPair.indexOf("=") + 1);
                params.put(paramKey, paramValue);
            }
        }
        return params;
    }

    private String paramToQueryString(Map<String, String> params) {
        if (params == null || params.isEmpty()) {
            return null;
        }
        StringBuilder paramString = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> p : params.entrySet()) {
            String key = p.getKey();
            String value = p.getValue();
            if (!first) {
                paramString.append("&");
            }
            // Urlencode each request parameter
            paramString.append(URLEncodeHelper.percentEncode(key));
            if (value != null) {
                paramString.append("=").append(URLEncodeHelper.percentEncode(value));
            }
            first = false;
        }
        return paramString.toString();
    }

    private String getLogResourceHost(String endpoint, Map<String, Object> actionParams) {
        String projectName = actionParams != null && actionParams.containsKey(PROJECT_NAME) ? actionParams.get(PROJECT_NAME).toString() : null;
        return AliyunHelper.getResourceHost(endpoint, projectName);
    }
}
