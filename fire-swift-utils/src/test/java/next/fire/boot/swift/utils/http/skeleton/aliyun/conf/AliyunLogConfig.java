package next.fire.boot.swift.utils.http.skeleton.aliyun.conf;

/**
 * Created by daibing on 2021/11/13.
 */
public class AliyunLogConfig {
    private String logStoreCode;
    private String accessKeyId;
    private String accessKeySecret;
    private String accountEndpoint;
    private String logProject;
    private String logStore;

    public String getLogStoreCode() {
        return logStoreCode;
    }

    public void setLogStoreCode(String logStoreCode) {
        this.logStoreCode = logStoreCode;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getAccountEndpoint() {
        return accountEndpoint;
    }

    public void setAccountEndpoint(String accountEndpoint) {
        this.accountEndpoint = accountEndpoint;
    }

    public String getLogProject() {
        return logProject;
    }

    public void setLogProject(String logProject) {
        this.logProject = logProject;
    }

    public String getLogStore() {
        return logStore;
    }

    public void setLogStore(String logStore) {
        this.logStore = logStore;
    }
}
