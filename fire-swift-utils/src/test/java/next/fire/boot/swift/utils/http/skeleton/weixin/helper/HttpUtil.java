package next.fire.boot.swift.utils.http.skeleton.weixin.helper;

import javax.net.ssl.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基于openx的HttpUtils扩展，支持微信的双向证书( 需要同一个package的PEMImporter支持 )
 * Created by daibing on 2019/1/4.
 */
public class HttpUtil {

    private static final String NEW_LINE = "\n";

    private static class TrustAnyTrustManager implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    private static final String DEF_CHARSET = "utf-8";
    private String url;
    private int timeout;
    private boolean keepAlive;

    public HttpUtil(String url, int timeout, boolean keepAlive) {
        this.url = url;
        this.timeout = timeout;
        this.keepAlive = keepAlive;
    }

    public String get() throws IOException {
        return this.handle("GET", null, null, null, null, null);
    }

    public String get(Map<String, String> header) throws IOException {
        return this.handle("GET", header, null, null, null, null);
    }

    public String post(String body) throws IOException {
        return this.handle("POST", null, body, null, null, null);
    }

    public String post(Map<String, String> header, String body) throws IOException {
        return this.handle("POST", header, body, null, null, null);
    }

    public String put(Map<String, String> header, String body) throws IOException {
        return this.handle("PUT", header, body, null, null, null);
    }

    public String delete(Map<String, String> header, String body) throws IOException {
        return this.handle("DELETE", header, body, null, null, null);
    }

    public String post(Map<String, String> header, String body, String privateKey, String certificate, String password) throws IOException {
        return this.handle("POST", header, body, privateKey, certificate, password);
    }

    private String handle(String method, Map<String, String> header, String body, String privateKey, String certificate, String password) throws IOException {
        URL url2 = new URL(url);
        HttpURLConnection conn = isHttps(url2) ? getHttpsConn(url2, privateKey, certificate, password) : (HttpURLConnection) url2.openConnection();

        // socket settings
        conn.setRequestMethod(method);
        if (!"GET".equalsIgnoreCase(method)) {
            conn.setDoOutput(true);
        }
        conn.setDoInput(true);
        conn.setReadTimeout(timeout);
        conn.setConnectTimeout(timeout);
        conn.setUseCaches(false);
        if (keepAlive) {
            conn.setRequestProperty("Connection", "Keep-Alive");
        } else {
            conn.setRequestProperty("Connection", "close");
        }

        // write header
        if (header != null) {
            for (Map.Entry<String, String> entry : header.entrySet()) {
                conn.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }

        // write body
        if (body != null && body.trim().length() != 0) {
            byte[] data = body.getBytes(DEF_CHARSET);
             if (this.isEmpty(conn.getRequestProperty("Content-Type"))) {
                 conn.setRequestProperty("Content-Type", "application/json; charset=" + DEF_CHARSET);
             }
            conn.setRequestProperty("Content-Length", String.valueOf(data.length));
            conn.getOutputStream().write(data);
        }

        int status = conn.getResponseCode();
        String result = null;
        if (status >= 200 && status < 400) {
            result = this.doRead(conn.getInputStream());
            if (this.isEmpty(result)) {
                result = this.doReadHeader(conn);
            }
            conn.disconnect();
            return result;
        } else {
            result = this.doRead(conn.getErrorStream());
            conn.disconnect();
            throw new IOException(result);
        }
    }

    private String doReadHeader(HttpURLConnection conn) {
        Map<String, String> header = this.getResponseHeader(conn);
        String contentType = conn.getContentType();
        if (this.isEmpty(contentType)) {
            contentType = conn.getRequestProperty("Content-Type");
        }
        if (!this.isEmpty(contentType) && contentType.toLowerCase().contains("xml")) {
             return this.map2XML(header);
        } else {
            return this.map2JSON(header, true);
        }
    }

    private Map<String, String> getResponseHeader(HttpURLConnection conn) {
        Map<String, List<String>> fields = conn.getHeaderFields();
        Map<String, String> headers = new HashMap<>(fields.size());
        for (Map.Entry<String,List<String>> entry : fields.entrySet()) {
            if (entry.getKey() == null || entry.getKey().isEmpty()) {
                continue;
            }
            if (entry.getValue() == null || entry.getValue().size() == 0) {
                continue;
            }
            StringBuilder sb = new StringBuilder();
            for (String val : entry.getValue()) {
                sb.append(",").append(val);
            }
            headers.put(entry.getKey(), sb.toString().substring(1));
        }
        return headers;
    }

    private HttpsURLConnection getHttpsConn(URL url, String privateKey, String certificate, String password) {
        SSLSocketFactory sslSocketFactory = null;
        if (this.isEmpty(privateKey) || this.isEmpty(certificate) || this.isEmpty(password)) {
            sslSocketFactory = this.getSSLSocketFactory();
        } else {
            sslSocketFactory = this.getSSLSocketFactory(privateKey, certificate, password);
        }
        try {
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setSSLSocketFactory(sslSocketFactory);
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            return conn;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private SSLSocketFactory getSSLSocketFactory() {
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{new TrustAnyTrustManager()}, new SecureRandom());
            return sc.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private SSLSocketFactory getSSLSocketFactory(String privateKey, String certificate, String password) {
        try {
            Class<?> clz = Class.forName(this.getClass().getPackage().getName() + ".PEMImporter");
            Method createKeyStore = clz.getMethod("createKeyStore", String.class, String.class, String.class);
            KeyStore keyStore = (KeyStore) createKeyStore.invoke(null, privateKey, certificate, password);
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keyStore, password.toCharArray());

            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(kmf.getKeyManagers(), new TrustManager[]{new TrustAnyTrustManager()}, new SecureRandom());
            return sc.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String doRead(InputStream is) throws IOException {
        byte[] buf2 = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream(4 * 1024);
        for (int num; (num = is.read(buf2)) != -1; ) {
            baos.write(buf2, 0, num);
        }
        return baos.toString(DEF_CHARSET);
    }

    private boolean isHttps(URL url) {
        return url.getProtocol().equalsIgnoreCase("https");
    }

    private boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    private String map2XML(Map<String, String> map) {
        if (map == null || map.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(256);
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>").append(NEW_LINE);
        sb.append("<xml>").append(NEW_LINE);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue() != null) {
                sb.append("<").append(entry.getKey()).append(">").append(entry.getValue()).append("</").append(entry.getKey()).append(">").append(NEW_LINE);
            }
        }
        sb.append("</xml>");
        return sb.toString();
    }

    private String map2JSON(Map<String, String> map, boolean quota) {
        if (map == null) {
            return null;
        }
        if (map.size() == 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue() != null) {
                if (quota) {
                    sb.append("\"").append(entry.getKey()).append("\":\"").append(entry.getValue()).append("\"").append(",");
                } else {
                    sb.append("\"").append(entry.getKey()).append("\":").append(entry.getValue()).append(",");
                }
            }
        }
        if (sb.length() > 1) {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("}");
        return sb.toString();
    }

//    public static void main(String[] args) {
//        System.out.println("HttpUtil start to do...");
//        HttpUtil httpUtil = new HttpUtil("baidu.com", 60, true);
//        Map<String, String> map = new HashMap<>();
//        map.put("Connection", "close");
//        map.put("b", "opi");
//        String result = httpUtil.map2JSON(map, true);
//        System.out.println("result: " + result);
//    }
}