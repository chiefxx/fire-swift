package next.fire.boot.swift.utils;

import com.talkyun.utils.env.HostManager;
import next.fire.boot.swift.utils.codec.ByteHelper;
import next.fire.boot.swift.utils.codec.HmacSHA256;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.nio.charset.StandardCharsets;

/**
 * Created by daibing on 2021/12/6.
 */
@RunWith(JUnit4.class)
public class SimpleTest {

    @Test
    public void ipTest() {
        System.out.println("ip test");
        String localIp = HostManager.get().getLocalIp();
        String suffix = localIp.substring(localIp.lastIndexOf(".") + 1);
        byte lastIp = (byte) Integer.parseInt(suffix);
        System.out.println("lastIp" + lastIp);
    }

    @Test
    public void emptyBodySha256() {
        String body = "";
        String hex = ByteHelper.byteToHex(HmacSHA256.sha256(body));
        System.out.println("hex: " + hex);
    }

    @Test
    public void sha256Test() {
        String stringToSign = "SDK-HMAC-SHA256\n" +
                "20211223T013417Z\n" +
                "0345e00b5310c3e770e6bac30b62638a9568aaeb50e62fac24080f38c35742a7";
        String keySecret ="3GppfRNeDjf73ogcOHSavyRRpsHUyJpRmq2rsnqg";
        byte[] keyBytes = keySecret.getBytes(StandardCharsets.UTF_8);
        byte[] hmac = HmacSHA256.hmac(keyBytes, stringToSign);
        String hex = ByteHelper.byteToHex(hmac);
        System.out.println("hex: " + hex);

        String marshal = new HexBinaryAdapter().marshal(hmac);
        System.out.println("marshal: " + marshal);
    }
}
