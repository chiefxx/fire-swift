package next.fire.boot.swift.utils.converter.json;

import com.talkyun.utils.json.JSON;
import next.fire.boot.swift.utils.converter.json.model.SupplierHouse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.Map;

/**
 * Created by daibing on 2021/11/17.
 */
@RunWith(JUnit4.class)
public class JsonTest {

    @Test
    public void jacksonLong2Str() {
        String raw = "{\n" +
                "            \"createTime\": 1636684428000,\n" +
                "            \"creator\": \"admin\",\n" +
                "            \"doorWidth\": 5.00,\n" +
                "            \"extensionLength\": 5.00,\n" +
                "            \"facadeNumber\": 2,\n" +
                "            \"floorArea\": 100.60,\n" +
                "            \"houseAddress\": \"湖北省武汉市\",\n" +
                "            \"houseCertificateId\": \"xx1212121214\",\n" +
                "            \"id\": 490497218349211648,\n" +
                "            \"landlordType\": \"firstLandlord\",\n" +
                "            \"modifier\": \"admin\",\n" +
                "            \"modifyTime\": 1636684428000,\n" +
                "            \"rentDesc\": \"租第3层\",\n" +
                "            \"signboardHeight\": 3.00,\n" +
                "            \"supplierId\": 490250967624032256,\n" +
                "            \"totalFloor\": 5,\n" +
                "            \"usableArea\": 80.20\n" +
                "        }";
        SupplierHouse obj = JacksonHelper.toObj(raw, SupplierHouse.class);
        obj.setCreator(null);
        System.out.println("obj1: " + JSON.toJsonStringWithNull(obj));
        System.out.println("obj2: " + JSON.toJsonString(obj));
        System.out.println("obj3: " + JacksonHelper.toJson(obj));
    }

    @Test
    public void mapTest() {
        String json = "{\n" +
                "  \"createTime\": 1636684428000,\n" +
                "  \"creator\": null,\n" +
                "  \"doorWidth\": 5.00,\n" +
                "  \"extensionLength\": 5.00,\n" +
                "  \"facadeNumber\": 2,\n" +
                "  \"floorArea\": 100.60,\n" +
                "  \"houseAddress\": \"湖北省武汉市\",\n" +
                "  \"houseCertificateId\": \"xx1212121214\",\n" +
                "  \"id\": \"490497218349211648\",\n" +
                "  \"landlordType\": \"firstLandlord\",\n" +
                "  \"modifier\": \"admin\",\n" +
                "  \"modifyTime\": 1636684428000,\n" +
                "  \"rentDesc\": \"租第3层\",\n" +
                "  \"signboardHeight\": 3.00,\n" +
                "  \"supplierId\": \"490250967624032256\",\n" +
                "  \"totalFloor\": 5,\n" +
                "  \"usableArea\": 80.20\n" +
                "}";
        Map<String, Object> data = JacksonHelper.toMap(json);
        System.out.println("data: " + data);
        Map<Object, Object> data2 = JacksonHelper.toMap(json);
        System.out.println("data2: " + data2);
        Map<String, String> data3 = JacksonHelper.toMap(json);
        System.out.println("data3: " + data3);
    }

    @Test
    public void mapListTest() {
        String json = "[\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"0.0.0.0-\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320624001\",\n" +
                "    \"thread\": \"2756\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.ErrorProcess.LoggerHelper:0\",\n" +
                "    \"throwable\": \"未将对象引用设置到对象的实例。\\r\\n调用堆栈：   在 YFPos.ErrorProcess.LoggerHelper.ErrorLog(Exception ex, LogTrigger logTrigger)\\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.191.86.51\",\n" +
                "    \"__tag__:__hostname__\": \"YF-20210923WAZV\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1E753EAF5575A993-8\",\n" +
                "    \"__tag__:__client_ip__\": \"113.246.80.189\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320627\",\n" +
                "    \"__time__\": \"1637320624\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320625076\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.AnimatedRenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-5A\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320627\",\n" +
                "    \"__time__\": \"1637320625\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320624029\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-5A\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320627\",\n" +
                "    \"__time__\": \"1637320624\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320623012\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-5A\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320627\",\n" +
                "    \"__time__\": \"1637320623\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.189.16.99-D887\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320621855\",\n" +
                "    \"thread\": \"4844\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)附加信息:GetServerTime异常未能解析此远程名称: 'ac.yifengx.com'\\r\\n调用堆栈：   在 System.Net.HttpWebRequest.GetRequestStream(TransportContext& context)\\r\\n   在 System.Net.HttpWebRequest.GetRequestStream()\\r\\n   在 YFPos.Utils.HttpClient.PostByJson(String url, String jsonStr, Int32 timeout, String token)\\r\\n   在 YFPos.Remote.GetServerTime.GetServerTimeRequest.GetTime(DateTime& time)\\r\\n   在 YFPos.Remote.HTTP.HttpTopic.RefreshHttpWork()\\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.189.16.99\",\n" +
                "    \"__tag__:__hostname__\": \"PC-20210326MBEL\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__pack_id__\": \"8E214721696FAFDE-0\",\n" +
                "    \"__tag__:__client_ip__\": \"124.229.125.23\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320623\",\n" +
                "    \"__time__\": \"1637320625\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"0.0.0.0-\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320623840\",\n" +
                "    \"thread\": \"9976\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.ErrorProcess.LoggerHelper:0\",\n" +
                "    \"throwable\": \"推送信息：YES，错误等级：S\\r\\n门店编码：G208，收银台ID：134348，IP地址：10.191.96.51，应用ID：NO_APPID，应用版本：6.58\\r\\n接口编码：PAY_2110101021，接口名称：OnlinePay\\r\\n接口状态：，错误编码：S2110101026\\r\\n请求时间：2021-11-19 19:17:03 799，响应时间：2021-11-19 19:17:03 825\\r\\n请求参数：\\r\\n时间：2021-11-19 19:17:03 799—信息：StoreCode:G208，PaymentModeCode:PN9，Parameter1:http://127.0.0.1:10080/upay-client/api/interface/call，Parameter2:430611，Parameter3:430611，Parameter4:http://10.136.68.158:21004/hsa-hgs-adapt/api/card/initDll，Parameter5:430100|10.136.68.158:21004\\r\\n\\r\\n响应参数：\\r\\n时间：2021-11-19 19:17:03 825—信息：支付失败：[2303]-[岳阳市医保] 在线支付失败 \\r\\n\\r\\n追加信息：\\r\\n调用堆栈：Fatal error encountered during command execution.，   在 MySql.Data.MySqlClient.MySqlCommand.ExecuteReader(CommandBehavior behavior)\\r\\n   在 MySql.Data.MySqlClient.MySqlCommand.ExecuteDbDataReader(CommandBehavior behavior)\\r\\n   在 System.Data.Common.DbCommand.System.Data.IDbCommand.ExecuteReader(CommandBehavior behavior)\\r\\n   在 Dapper.SqlMapper.ExecuteReaderWithFlagsFallback(IDbCommand cmd, Boolean wasClosed, CommandBehavior behavior)\\r\\n   在 Dapper.SqlMapper.<QueryImpl>d__69`1.MoveNext()\\r\\n   在 System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)\\r\\n   在 System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)\\r\\n   在 Dapper.SqlMapper.Query[T](IDbConnection cnn, String sql, Object param, IDbTransaction transaction, Boolean buffered, Nullable`1 commandTimeout, Nullable`1 commandType)\\r\\n   在 Dapper.SimpleCRUD.GetList[T](IDbConnection connection, String conditions, Object parameters, IDbTransaction transaction, Nullable`1 commandTimeout)\\r\\n   在 YFPos.Business.Sales.SaleComponents.SalePayBiz.OnlinePay(SalesOrder order, BaseCustomerpaym customerpaym, Object ownerHandle, Decimal needAmount, Decimal& amount, String& msg, Orders dd)\\r\\n\\r\\n\\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"127.0.0.1\",\n" +
                "    \"__tag__:__hostname__\": \"YF-20210806AVWM\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"C23177839012018F-1\",\n" +
                "    \"__tag__:__client_ip__\": \"175.7.70.230\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320623\",\n" +
                "    \"__time__\": \"1637320623\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320621984\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-59\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320624\",\n" +
                "    \"__time__\": \"1637320622\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320620969\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-59\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320624\",\n" +
                "    \"__time__\": \"1637320621\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320619901\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.AnimatedRenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\\n[YFPos] [10.142.33.162-F015] [product] [2021-11-19 19:16:59,905] [1637320619905] [4180] [ERROR] [YFPos.Utils.LogHelper:0] - (ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-59\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320624\",\n" +
                "    \"__time__\": \"1637320620\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"appId\": \"YFPos\",\n" +
                "    \"host\": \"10.142.33.162-F015\",\n" +
                "    \"stage\": \"product\",\n" +
                "    \"logTime\": \"1637320618864\",\n" +
                "    \"thread\": \"4180\",\n" +
                "    \"level\": \"ERROR\",\n" +
                "    \"location\": \"YFPos.Utils.LogHelper:0\",\n" +
                "    \"throwable\": \"(ver:6.58)未捕获的UI异常: 已添加了具有相同键的项。    在 System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)\\r\\n   在 System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)\\r\\n   在 System.Collections.Generic.Dictionary`2..ctor(IDictionary`2 dictionary, IEqualityComparer`1 comparer)\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetItemPeers()\\r\\n   在 Microsoft.Windows.Automation.Peers.DataGridAutomationPeer.GetChildrenCore()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.EnsureChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildrenInternal(Int32 invalidateLimit)\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateChildren()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.Automation.Peers.AutomationPeer.UpdateSubtree()\\r\\n   在 System.Windows.ContextLayoutManager.fireAutomationEvents()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayout()\\r\\n   在 System.Windows.ContextLayoutManager.UpdateLayoutCallback(Object arg)\\r\\n   在 System.Windows.Media.MediaContext.InvokeOnRenderCallback.DoWork()\\r\\n   在 System.Windows.Media.MediaContext.FireInvokeOnRenderCallbacks()\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandlerCore(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Media.MediaContext.RenderMessageHandler(Object resizedCompositionTarget)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.InternalRealCall(Delegate callback, Object args, Int32 numArgs)\\r\\n   在 System.Windows.Threading.ExceptionWrapper.TryCatchWhen(Object source, Delegate callback, Object args, Int32 numArgs, Delegate catchHandler) \\r\",\n" +
                "    \"__topic__\": \"product\",\n" +
                "    \"__source__\": \"10.142.33.162\",\n" +
                "    \"__tag__:__hostname__\": \"PC-PC\",\n" +
                "    \"__tag__:__path__\": \"D:\\\\YiFengPOS\\\\YfPos\\\\log\\\\EXCEPTION\\\\20211119.yf.log\",\n" +
                "    \"__tag__:__user_defined_id__\": \"yf-hunan-group\",\n" +
                "    \"__tag__:__pack_id__\": \"1971DC1EB85850AC-58\",\n" +
                "    \"__tag__:__client_ip__\": \"118.239.10.78\",\n" +
                "    \"__tag__:__receive_time__\": \"1637320622\",\n" +
                "    \"__time__\": \"1637320618\"\n" +
                "  }\n" +
                "]";
         List<Map<String, Object>> list = JacksonHelper.toMapList(json);
//        List<Map> list = JacksonHelper.toObjList(json, Map.class);
        System.out.println("list: " + list);
    }
}
