package next.fire.boot.swift.utils.converter.yaml;

import com.google.common.base.Splitter;
import next.fire.boot.swift.utils.http.RequestMethod;
import next.fire.boot.swift.utils.http.processor.model.RequestTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.yaml.snakeyaml.Yaml;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daibing on 2021/12/3.
 */
@RunWith(JUnit4.class)
public class YamlTest {

    @Test
    public void yamlTest() {
        String body = "AccessKeyId={{AccessKeyId.DATA}}&Action=CreateInstance&Description={{Description.DATA}}&Format=json" +
                "&HostName={{HostName.DATA}}&ImageId={{ImageId.DATA}}&InstanceChargeType={{InstanceChargeType.DATA}}" +
                "&InstanceName={{InstanceName.DATA}}&InstanceType={{InstanceType.DATA}}&Password={{Password.DATA}}" +
                "&RegionId={{RegionId.DATA}}&SecurityGroupId={{SecurityGroupId.DATA}}&SignatureMethod=HMAC-SHA1" +
                "&SignatureNonce={{SignatureNonce.DATA}}&SignatureVersion=1.0&SystemDisk.Category={{SystemDisk.Category.DATA}}" +
                "&SystemDisk.Size={{SystemDisk.Size.DATA}}&Timestamp={{Timestamp.DATA}}&VSwitchId={{VSwitchId.DATA}}" +
                "&Version=2014-05-26&ZoneId={{ZoneId.DATA}}&Signature={{Signature.ACTION}}";
        Map<String, String> header = new HashMap<>(1);
        header.put("Content-Type", "application/x-www-form-urlencoded; charset=" + StandardCharsets.UTF_8.name());
        header.put("Content-Length", String.valueOf(body.length()));
        RequestTemplate template = new RequestTemplate();
        template.setTemplateUrl("/template/aliyun/ecs/ecs-create.yaml");
        template.setMethod(RequestMethod.POST);
        template.setHeaders(header);
        template.setBody(body);
        template.setUri("https://ecs.aliyuncs.com/");
//        template.setWorkerClassName(AliyunFormRequestWorker.class.getName());
//        template.setTimeoutMillis(30 * 1000);
        String dump = new Yaml().dump(template);
        System.out.println("dump: " + dump);

        RequestTemplate template2 = new Yaml().loadAs(dump, RequestTemplate.class);
        System.out.println("template2: " + template2);
    }

    @Test
    public void splitTest() {
        String body = "AccessKeyId={{AccessKeyId.DATA}}&Action=CreateInstance&Description={{Description.DATA}}&Format=json &HostName={{HostName.DATA}}&ImageId={{ImageId.DATA}}&InstanceChargeType={{InstanceChargeType.DATA}} &InstanceName={{InstanceName.DATA}}&InstanceType={{InstanceType.DATA}}&Password={{Password.DATA}} &RegionId={{RegionId.DATA}}&SecurityGroupId={{SecurityGroupId.DATA}}&SignatureMethod=HMAC-SHA1 &SignatureNonce={{SignatureNonce.DATA}}&SignatureVersion=1.0&SystemDisk.Category={{SystemDisk.Category.DATA}} &SystemDisk.Size={{SystemDisk.Size.DATA}}&Timestamp={{Timestamp.DATA}}&VSwitchId={{VSwitchId.DATA}} &Version=2014-05-26&ZoneId={{ZoneId.DATA}}&Signature={{Signature.ACTION}}";
        Map<String, String> split = Splitter.on("&").withKeyValueSeparator("=").split(body);
        System.out.println("split: " + split);
    }

    @Test
    public void splitTest2() {
        String body = "AccessKeyId={{AccessKeyId.DATA}}&Action=CreateInstance&Description={{Description.DATA}}&Format=&HostName={{HostName.DATA}}&ImageId={{ImageId.DATA}}&InstanceChargeType={{InstanceChargeType.DATA}} &InstanceName={{InstanceName.DATA}}&InstanceType={{InstanceType.DATA}}&Password={{Password.DATA}} &RegionId={{RegionId.DATA}}&SecurityGroupId={{SecurityGroupId.DATA}}&SignatureMethod=HMAC-SHA1 &SignatureNonce={{SignatureNonce.DATA}}&SignatureVersion=1.0&SystemDisk.Category={{SystemDisk.Category.DATA}} &SystemDisk.Size={{SystemDisk.Size.DATA}}&Timestamp={{Timestamp.DATA}}&VSwitchId={{VSwitchId.DATA}} &Version=2014-05-26&ZoneId={{ZoneId.DATA}}&Signature={{Signature.ACTION}}";
        Map<String, String> split = Splitter.on("&").omitEmptyStrings().trimResults().withKeyValueSeparator("=").split(body);
        System.out.println("split: " + split);
    }
}
