package next.fire.boot.swift.utils.http.processor;

import next.fire.boot.swift.utils.converter.date.DateHelper;
import next.fire.boot.swift.utils.http.RequestHandler;
import next.fire.boot.swift.utils.http.processor.conf.EcsConfig;
import next.fire.boot.swift.utils.http.processor.model.CloudDiskTypeEnum;
import next.fire.boot.swift.utils.http.processor.model.PayTypeEnum;
import next.fire.boot.swift.utils.generate.id.Snowflake;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daibing on 2021/12/2.
 */
@RunWith(JUnit4.class)
public class DefaultRequestProcessorTest {
    private final DefaultRequestProcessor processor = new DefaultRequestProcessor();
    private String providerId = "aliyun_amwuqd_1";
    EcsConfig config = new EcsConfig();

    @Before
    public void setUp() {
        config.setProviderId(providerId);
        config.setAccessKeyId("LTAI5tNQaLzWyvesrr9MnCkG");
        config.setAccessKeySecret("HrucKpGE8VLff17e6HGD5C2zShxIDi");
        config.setImageId("ubuntu_18_04_64_20G_alibase_20190624.vhd");
        config.setInstanceType("ecs.s6-c1m1.small");
        config.setSecurityGroupId("sg-wz91mv6i3x6k6jujzd8i");
        config.setDiskCategory(CloudDiskTypeEnum.EFFICIENT);
        config.setDiskSize(100);
        config.setSwitchId("vsw-wz9qkvh9cg5fi1e5ktu6h");
        config.setPassword("aliyun@123");
        config.setPayType(PayTypeEnum.POST_PAID);
        config.setPeriod("1");

        // setting proxy
        System.setProperty("proxySet","true");
        System.setProperty("proxyHost", "127.0.0.1");
        System.setProperty("proxyPort", "8888");
    }

    @Test
    public void ecsCreateTest() {
        String name = "fire-cloud-sdk-daibing-008";
        String desc = "戴兵测试机器08";
        String result = processor.execute("/template/ecs-create.yaml", new RequestHandler<String>() {
            @Override
            public Map<String, Object> buildBizParams() {
                Map<String, Object> params = new HashMap<>();
                //读取文件级参数
                params.put("ZoneId", "cn-shenzhen-e");

                //读取配置级参数
                params.put("ImageId", config.getImageId());
                params.put("SecurityGroupId", config.getSecurityGroupId());
                params.put("VSwitchId", config.getSwitchId());
                params.put("InstanceType", config.getInstanceType());
                params.put("SystemDisk.Category", config.getDiskCategory().getValue());
                params.put("SystemDisk.Size", String.valueOf(config.getDiskSize()));
                params.put("Password", config.getPassword());
                params.put("InstanceChargeType", config.getPayType().getValue());
                if (config.getPayType().equals(PayTypeEnum.PRE_PAID)) {
                    params.put("Period", config.getPeriod());
                }

                //读取入参级参数
                params.put("InstanceName", name);
                params.put("HostName", name.replaceAll("_", "-"));
                params.put("Description", desc);

                //请求识别参数
                params.put("AccessKeyId", config.getAccessKeyId());
                params.put("SignatureNonce", String.valueOf(Snowflake.get().nextId()));
                params.put("Timestamp", DateHelper.getISO8601Time(new Date()));
                return params;
            }

            @Override
            public Map<String, Object> buildAccessParams() {
                Map<String, Object> accessParams = new HashMap<>();
                accessParams.put("RegionId", "cn-shenzhen");
                accessParams.put("AccessKeyId", "LTAI5tNQaLzWyvesrr9MnCkG");
                accessParams.put("AccessKeySecret", "HrucKpGE8VLff17e6HGD5C2zShxIDi");
                return accessParams;
            }

            @Override
            public String extractData(Map output) {
                return String.valueOf(output.get("InstanceId"));
            }
        });
        System.out.println("result: " + result);
    }
}
