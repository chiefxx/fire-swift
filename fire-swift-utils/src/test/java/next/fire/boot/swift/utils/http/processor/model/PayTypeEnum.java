package next.fire.boot.swift.utils.http.processor.model;

/**
 * @author xiongjm
 * @date 2021/11/8 17:14
 */
public enum PayTypeEnum {
    /**
     * 按量付费
     */
    POST_PAID,
    /**
     * 包年包月
     */
    PRE_PAID;

    public static PayTypeEnum get(String raw) {
        try {
            return PayTypeEnum.valueOf(raw.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }

    public String getDesc() {
        switch (this) {
            case POST_PAID:
                return "按量付费";
            case PRE_PAID:
                return "包年包月";
            default:
                return "无效类型: " + this;
        }
    }

    public static PayTypeEnum getEnum(String value) {
        switch (value) {
            case "PrePaid":
            case "Prepaid":
            case "PrePay":
                return PayTypeEnum.PRE_PAID;
            case "PostPaid":
            case "Postpaid":
            case "PayOnDemand":
                return PayTypeEnum.POST_PAID;
            default:
                return null;
        }
    }

    public String getValue() {
        switch (this) {
            case PRE_PAID:
                return "PrePaid";
            case POST_PAID:
                return "PostPaid";
            default:
                return "无效类型: " + this;
        }
    }

}
