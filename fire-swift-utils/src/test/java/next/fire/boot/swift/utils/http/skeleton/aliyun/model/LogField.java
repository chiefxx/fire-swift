package next.fire.boot.swift.utils.http.skeleton.aliyun.model;

import java.util.List;

/**
 * Created by daibing on 2019/10/17.
 */
public class LogField {

    private String fieldName;

    private String indexMode;

    private boolean docValue;

    private boolean caseSensitive;

    private boolean chn;

    private String alias;

    private boolean text;

    private List<String> token;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getIndexMode() {
        return indexMode;
    }

    public void setIndexMode(String indexMode) {
        this.indexMode = indexMode;
    }

    public boolean isDocValue() {
        return docValue;
    }

    public void setDocValue(boolean docValue) {
        this.docValue = docValue;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public boolean isChn() {
        return chn;
    }

    public void setChn(boolean chn) {
        this.chn = chn;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isText() {
        return text;
    }

    public void setText(boolean text) {
        this.text = text;
    }

    public List<String> getToken() {
        return token;
    }

    public void setToken(List<String> token) {
        this.token = token;
    }
}
