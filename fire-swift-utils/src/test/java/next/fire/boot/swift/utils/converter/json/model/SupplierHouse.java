package next.fire.boot.swift.utils.converter.json.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 供应商房屋信息
 * @author ZhengYangHuang
 * @date 2021/11/8
 **/
public class SupplierHouse {
    /**
     * supplierId: 供应商ID
     * landlordType：房东类型：一房东、二房东、承租方
     * houseCertificateId: 房产证
     * houseAddress: 房屋地址
     * rentDesc: 租赁描述
     * totalFloor: 共 xxx 层
     * facadeNumber：门面数(间)
     * floorArea: 建筑面积(平方米)
     * usableArea: 使用面积(平方米)
     * doorWidth: 门头宽（米）
     * signboardHeight: 招牌高（米）
     * extensionLength：再顺延（米）
     */
    private long id;
    private long supplierId;
    private String landlordType;
    private String houseCertificateId;
    private String houseAddress;
    private String rentDesc;
    private int totalFloor;
    private int facadeNumber;
    private BigDecimal floorArea;
    private BigDecimal usableArea;
    private BigDecimal doorWidth;
    private BigDecimal signboardHeight;
    private BigDecimal extensionLength;
    private String creator;
    private String modifier;
    private Date createTime;
    private Date modifyTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(long supplierId) {
        this.supplierId = supplierId;
    }

    public String getLandlordType() {
        return landlordType;
    }

    public void setLandlordType(String landlordType) {
        this.landlordType = landlordType;
    }

    public String getHouseCertificateId() {
        return houseCertificateId;
    }

    public void setHouseCertificateId(String houseCertificateId) {
        this.houseCertificateId = houseCertificateId;
    }

    public String getHouseAddress() {
        return houseAddress;
    }

    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }

    public String getRentDesc() {
        return rentDesc;
    }

    public void setRentDesc(String rentDesc) {
        this.rentDesc = rentDesc;
    }

    public int getTotalFloor() {
        return totalFloor;
    }

    public void setTotalFloor(int totalFloor) {
        this.totalFloor = totalFloor;
    }

    public int getFacadeNumber() {
        return facadeNumber;
    }

    public void setFacadeNumber(int facadeNumber) {
        this.facadeNumber = facadeNumber;
    }

    public BigDecimal getFloorArea() {
        return floorArea;
    }

    public void setFloorArea(BigDecimal floorArea) {
        this.floorArea = floorArea;
    }

    public BigDecimal getUsableArea() {
        return usableArea;
    }

    public void setUsableArea(BigDecimal usableArea) {
        this.usableArea = usableArea;
    }

    public BigDecimal getDoorWidth() {
        return doorWidth;
    }

    public void setDoorWidth(BigDecimal doorWidth) {
        this.doorWidth = doorWidth;
    }

    public BigDecimal getSignboardHeight() {
        return signboardHeight;
    }

    public void setSignboardHeight(BigDecimal signboardHeight) {
        this.signboardHeight = signboardHeight;
    }

    public BigDecimal getExtensionLength() {
        return extensionLength;
    }

    public void setExtensionLength(BigDecimal extensionLength) {
        this.extensionLength = extensionLength;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}