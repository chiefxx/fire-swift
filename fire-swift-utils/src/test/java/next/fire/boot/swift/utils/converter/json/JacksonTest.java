package next.fire.boot.swift.utils.converter.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by daibing on 2021/12/16.
 */
@RunWith(JUnit4.class)
public class JacksonTest {
    ObjectMapper mapper = JsonMapper.builder()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .serializationInclusion(JsonInclude.Include.NON_EMPTY)
            .enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
            .build();
    String content = "{\n" +
            "  \"dry_run\": true,\n" +
            "  \"server\": {\n" +
            "    \"imageRef\": \"{{imageRef.DATA}}\",\n" +
            "    \"flavorRef\": \"{{flavorRef.DATA}}\",\n" +
            "    \"name\": \"{{name.DATA}}\",\n" +
            "    \"adminPass\": \"{{adminPass.DATA}}\",\n" +
            "    \"vpcid\": \"{{vpcid.DATA}}\",\n" +
            "    \"nics\": [{\n" +
            "               \"subnet_id\": \"{{subnet_id.DATA}}\"\n" +
            "             }],\n" +
            "    \"root_volume\": {\n" +
            "      \"volumetype\": \"{{volumetype.DATA}}\",\n" +
            "      \"size\": \"{{size.DATA}}\"\n" +
            "    },\n" +
            "    \"security_groups\": [{\n" +
            "      \"id\": \"{{id.DATA}}\"\n" +
            "    }],\n" +
            "    \"extendparam\": {\n" +
            "      \"chargingMode\": \"{{chargingMode.DATA}}\",\n" +
            "      \"periodType\": \"{{periodType.DATA}}\",\n" +
            "      \"periodNum\": \"{{periodNum.DATA}}\"\n" +
            "    },\n" +
            "    \"description\": \"{{description.DATA}}\"\n" +
            "  }\n" +
            "}";
    Map<String, Object> bizParams = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        SimpleModule simpleModule = new SimpleModule();
//        simpleModule.addSerializer(Object.class, SimpleJsonSerializer.instance);
//        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
//        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        mapper.registerModule(simpleModule);
        bizParams.put("imageRef", "imageRef-99993");
        bizParams.put("periodType", "periodType-99994");
        bizParams.put("chargingMode", null);
        bizParams.put("periodNum", null);
    }

    /**
     * 1.替换参数
     * 2.删除为空参数
     * 3.数字动态增加，例如security_groups，有多个id
     */

    @Test
    public void contentTest() throws JsonProcessingException {
        System.out.println("Content test start.");
        JsonNode node = mapper.readTree(content);
        replaceHolder(node, bizParams);
        String result = mapper.writeValueAsString(node);
        System.out.println("result: " + result);
    }

    private void replaceHolder(JsonNode node, Map<String, Object> bizParams) {
        if (node.isValueNode()) {
            System.out.println(node.toString());
            return;
        }
        if (node.isObject()) {
            Iterator<Map.Entry<String, JsonNode>> it = node.fields();
            while (it.hasNext()) {
                Map.Entry<String, JsonNode> entry = it.next();
                if (!entry.getValue().isValueNode()) {
                    replaceHolder(entry.getValue(), bizParams);
                }
                if (entry.getValue().isValueNode() && bizParams.containsKey(entry.getKey())) {
                    entry.setValue(mapper.valueToTree(bizParams.get(entry.getKey())));
                }
            }
        }
        if (node.isArray()) {
            Iterator<JsonNode> it = node.iterator();
            while (it.hasNext()) {
                replaceHolder(it.next(), bizParams);
            }
        }
    }


    public static class SimpleJsonSerializer extends JsonSerializer<Object> {
        public static final SimpleJsonSerializer instance = new SimpleJsonSerializer();
        @Override
        public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            System.out.println("value: " + value);
            gen.writeString(value.toString());
        }
    }

}
