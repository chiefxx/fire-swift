package next.fire.boot.swift.utils.http.skeleton.aliyun;

import next.fire.boot.swift.utils.converter.json.JacksonHelper;
import next.fire.boot.swift.utils.http.RequestHelper;
import next.fire.boot.swift.utils.http.RequestHandler;
import next.fire.boot.swift.utils.http.skeleton.aliyun.client.AliyunLogClient;
import next.fire.boot.swift.utils.http.skeleton.aliyun.conf.AliyunLogConfig;
import next.fire.boot.swift.utils.http.skeleton.aliyun.conf.AliyunLogConfigManager;
import next.fire.boot.swift.utils.http.skeleton.aliyun.helper.AliyunHelper;
import next.fire.boot.swift.utils.http.skeleton.aliyun.model.LogData;
import next.fire.boot.swift.utils.http.skeleton.aliyun.model.LogField;
import next.fire.boot.swift.utils.http.skeleton.aliyun.model.LogMeta;
import next.fire.boot.swift.utils.http.RequestMethod;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

/**
 * Created by daibing on 2021/11/13.
 */
@RunWith(JUnit4.class)
public class AbstractRequestClientTest {
    private String logStoreCode = "log0001";
    private final AliyunLogClient logClient = new AliyunLogClient();

    @Before
    public void setUp() throws Exception {
        AliyunLogConfig config = new AliyunLogConfig();
        config.setLogStoreCode(logStoreCode);
        config.setAccessKeyId("LTAI4FizqSca1WdoqkoaGjeU");     // 密钥信息需要屏蔽, 使用的时候替换
        config.setAccessKeySecret("Q2TdkYaPcuHvqdi4Xib67fToKcSoMZ"); // 密钥信息需要屏蔽, 使用的时候替换
        config.setAccountEndpoint("http://cn-shenzhen.log.aliyuncs.com");
        config.setLogProject("yf-cashier-log");
        config.setLogStore("yf-cashier-log");
        List<AliyunLogConfig> aliyunLogConfigList = Arrays.asList(config);
        AliyunLogConfigManager.get().reload(aliyunLogConfigList);
    }

    @Test
    public void getMetaTest() throws Exception {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(logStoreCode);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + logStoreCode);
        }
        String resourceUri = "/logstores/" + config.getLogStore();
        LogMeta meta = logClient.execute(RequestMethod.GET, resourceUri, logStoreCode, false, false, new RequestHandler<LogMeta>() {
            @Override
            public Map<String, Object> buildBizParams() {
                return null;
            }

            @Override
            public Map<String, Object> buildAccessParams() {
                return buildNewActionParams(config.getLogProject(), false);
            }

            @Override
            public LogMeta extractData(Map<String, Object> output) {
                LogMeta meta = new LogMeta();
                meta.setLogStoreName(String.valueOf(output.get("logstoreName")));
                meta.setTtl(Integer.valueOf(output.get("ttl").toString()));
                meta.setShardCount(Integer.valueOf(output.get("shardCount").toString()));
                meta.setAutoSplit(Boolean.parseBoolean(output.get("autoSplit").toString()));
                meta.setCreateTime(parseTimestamp(Long.valueOf(output.get("createTime").toString())));
                meta.setLastModify(parseTimestamp(Long.valueOf(output.get("lastModifyTime").toString())));
                return meta;
            }
        });
        System.out.println("Meta: " + meta.toString());
    }

    @Test
    public void listFieldTest() throws Exception {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(logStoreCode);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + logStoreCode);
        }
        String resourceUri = "/logstores/" + config.getLogStore() + "/index";
        List<LogField> fields = logClient.execute(RequestMethod.GET, resourceUri, logStoreCode, false, false, new RequestHandler<List<LogField>>() {
            @Override
            public Map<String, Object> buildBizParams() {
                return null;
            }

            @Override
            public Map<String, Object> buildAccessParams() {
                return buildNewActionParams(config.getLogProject(), false);
            }

            @Override
            @SuppressWarnings("unchecked")
            public List<LogField> extractData(Map<String, Object> output) {
                List<LogField> fields = new ArrayList<>();
                Map<String, Object> keys = (Map<String, Object>) output.get("keys");
                for (Map.Entry<String, Object> entry : keys.entrySet()) {
                    LogField field = new LogField();
                    field.setFieldName(entry.getKey());
                    field.setIndexMode(output.get("index_mode").toString());
                    Map<String, Object> detail = (Map<String, Object>) entry.getValue();
                    field.setDocValue(Boolean.parseBoolean(detail.get("doc_value").toString()));
                    field.setCaseSensitive(Boolean.parseBoolean(String.valueOf(detail.get("caseSensitive"))));
                    field.setChn(Boolean.parseBoolean(String.valueOf(detail.get("chn"))));
                    field.setAlias(detail.get("alias").toString());
                    field.setText("text".equalsIgnoreCase(detail.get("type").toString()));
                    field.setToken((List<String>) ((Map<String, Object>) entry.getValue()).get("token"));
                    fields.add(field);
                }
                return fields;
            }
        });
        System.out.println("fields: " + fields.toString());
    }


    @Test
    public void listDataTest() throws Exception {
        AliyunLogConfig config = AliyunLogConfigManager.get().select(logStoreCode);
        if (config == null) {
            throw new RuntimeException("aliyun log config not found by " + logStoreCode);
        }
        int pageNo = 1;
        int pageSize = 10;
        Map<String, String> queryString = new TreeMap<>();
        queryString.put("type", "log");
        // 注意from和to两个字段为必填，并且为日志数据到达服务端的时间点，精度为秒，所以将结束时间调整为查询日志结束时间+3分钟
        queryString.put("from", String.valueOf(System.currentTimeMillis() / 1000 - 30 * 60));
        queryString.put("to", String.valueOf(System.currentTimeMillis() / 1000));
        queryString.put("line", String.valueOf(pageSize));
        // 注意从第一页开始
        queryString.put("offset", String.valueOf((pageNo - 1) * pageSize));
        queryString.put("reverse", String.valueOf(true));
        String resourceUri = AliyunHelper.getCanonicalizedResource("/logstores/" + config.getLogStore(), queryString);
        List<LogData> fields = logClient.execute(RequestMethod.GET, resourceUri, logStoreCode, false, false, new RequestHandler<List<LogData>>() {
            @Override
            public Map<String, Object> buildBizParams() {
                return null;
            }

            @Override
            public Map<String, Object> buildAccessParams() {
                return buildNewActionParams(config.getLogProject(), true);
            }

            @Override
            @SuppressWarnings("unchecked")
            public List<LogData> extractData(Map<String, Object> output) {
                List<Map<String, Object>> list = (List<Map<String, Object>>) output.get(RequestHelper.RESPONSE_BODY);
                return JacksonHelper.toObjList(list, LogData.class);
            }
        });
        System.out.println("fields: " + fields.toString());
    }

    private Map<String, Object> buildNewActionParams(String projectName, boolean responseList) {
        Map<String, Object> actionParams = new HashMap<>();
        actionParams.put(AliyunLogClient.PROJECT_NAME, projectName);
        actionParams.put(AliyunLogClient.RESPONSE_DATA_LIST, responseList);
        return actionParams;
    }

    private Date parseTimestamp(Long timestamp) {
        if (timestamp.toString().length() == 13) {
            return new Date(timestamp);
        }
        return new Date(timestamp.intValue() * 1000L);
    }
}