package next.fire.boot.swift.utils.http.processor.model;

/**
 * @author xiongjm
 * @date 2021/11/3 17:59
 */
public enum CloudDiskTypeEnum {
    /**
     * 普通云盘
     */
    NORMAL,
    /**
     * SSD云盘
     */
    SSD,
    /**
     * 高效云盘
     */
    EFFICIENT
    ;

    public static CloudDiskTypeEnum get(String raw) {
        try {
            return CloudDiskTypeEnum.valueOf(raw.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }

    public String getDesc() {
        switch (this) {
            case NORMAL:
                return "普通云盘";
            case SSD:
                return "SSD云盘";
            case EFFICIENT:
                return "高效云盘";
            default:
                return "无效类型: " + this;
        }
    }

    public String getValue() {
        switch (this) {
            case NORMAL:
                return "cloud";
            case SSD:
                return "cloud_ssd";
            case EFFICIENT:
                return "cloud_efficiency";
            default:
                return "无效类型: " + this;
        }
    }

}
