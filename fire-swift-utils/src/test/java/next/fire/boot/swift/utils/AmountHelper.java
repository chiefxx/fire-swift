package next.fire.boot.swift.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * @author hx
 * @date 2019/5/5
 */
public class AmountHelper {

    public static BigDecimal c2y(Integer amount) {
        return new BigDecimal(amount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_DOWN);
    }

    public static BigDecimal y2c(String amount) {
        return new BigDecimal(amount).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_DOWN);
    }

    public static BigDecimal divide(BigDecimal dividend, BigDecimal divisor) {
        return dividend.divide(divisor, 10, BigDecimal.ROUND_DOWN);
    }

    public static BigDecimal multiply(BigDecimal multiplier, BigDecimal multiplicand) {
        return multiplier.multiply(multiplicand, new MathContext(10, RoundingMode.DOWN));
    }

    public static BigDecimal multiply(BigDecimal multiplier, Integer percent) {
        return multiplier.multiply(new BigDecimal(percent), new MathContext(10, RoundingMode.DOWN))
                .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_DOWN);
    }

    public static Integer totalAmount(BigDecimal quantity, Integer exercisePrice) {
        return quantity.multiply(new BigDecimal(exercisePrice)).setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
    }

}
