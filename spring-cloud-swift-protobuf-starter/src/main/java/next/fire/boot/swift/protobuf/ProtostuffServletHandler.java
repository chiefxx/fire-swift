package next.fire.boot.swift.protobuf;

import com.talkyun.openx.RestfulHeaderManager;
import com.talkyun.openx.server.core.ServiceContext;
import com.talkyun.openx.server.core.ServiceRequest;
import com.talkyun.openx.server.core.ServiceResponse;
import com.talkyun.utils.json.JSON;
import next.fire.boot.swift.openx.server.SwiftServletHandler;
import next.fire.boot.swift.protobuf.autoconfigure.ProtobufProperties;
import next.fire.boot.swift.protobuf.helper.ByteUtil;
import next.fire.boot.swift.protobuf.helper.LZ4Encoder;
import next.fire.boot.swift.protobuf.helper.Lz4Util;
import next.fire.boot.swift.protobuf.helper.ProtostuffUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.talkyun.openx.server.core.ServiceContext.REQ_HOST;
import static com.talkyun.openx.server.core.ServiceContext.REQ_IP;
import static com.talkyun.openx.server.core.ServiceResponse.Status.NORMAL;
import static javax.servlet.http.HttpServletResponse.*;
import static next.fire.boot.swift.protobuf.ProtoConstant.*;

/**
 * 支持protobuf+lz4
 * Created by daibing on 2021/8/19.
 */
public class ProtostuffServletHandler extends SwiftServletHandler {
    private static final Logger logger = LoggerFactory.getLogger(ProtostuffServletHandler.class);
    private final List<ProtobufProperties.ArgTypeConfig> argTypeConfigs;
    private final boolean printRequestBody;

    public ProtostuffServletHandler(ServletContext sc, List<String> throughHeaderPrefixList,
                                    List<ProtobufProperties.ArgTypeConfig> argTypeConfigs, boolean printRequestBody) {
        super(sc, throughHeaderPrefixList);
        this.argTypeConfigs = argTypeConfigs;
        this.printRequestBody = printRequestBody;
    }

    public void handle(HttpServletRequest req, HttpServletResponse resp, boolean lz4) throws IOException {
        // 0. cors first
        String method = req.getMethod().toLowerCase();
        this.doCors(req, resp);
        if ("options".equals(method)) {
            return;
        }

        // 1. servlet handle
        String uri = this.getLastURI(req.getRequestURI(), context);
        String data = this.doRead(req, uri, lz4);
        String session = req.getSession().getId();
        //   - ready service request
        ServiceRequest sr = codec.decode(uri, data, session);

        //   1.1 - not found service impl, response 404
        if (sr == null) {
            logger.warn("Not found rest request! {} ", uri);
            this.doWrite(resp, SC_NOT_FOUND, "{\"message\":\"" + uri + " not found!\"}");
            return;
        }
        //   - ready service context
        String host = this.getRemoteHost(req);
        String reqIp = this.getRemoteAddress(req);
        ServiceContext ctx = ServiceContext.getContext(session);
        ctx.put(REQ_IP, reqIp);
        ctx.put(REQ_HOST, host);
        //   - set request to ctx
        ctx.setServiceRequest(sr);

        // ADD header through
        this.doHeaderThrough(req, this.throughHeaderPrefixList);

        // 2. service invoker
        ServiceResponse sp = this.invoke(ctx, sr);
        ctx.clean();

        // clean local header
        RestfulHeaderManager.get().getOnceHead().clear();

        // 3. servlet response
        this.doResponse(resp, sp, lz4);
    }

    /**
     * Protobuf 使用 Protostuff 序列化和反序列化的注意事项：
     * 1、proto解码前是不能获取参数对象，所以必须外部设置参数对象Class和是否List
     * 2、openx调用的入参是列表对象的时候，当前月定是封装为一个JSON对象（带参数名），
     * 但是protobuf调用的入参是列表对象的时候，必须直接传入对象列表的序列号字节数组，然后在服务端拼接后满足openx需求
     * 所以必须外部设置参数名来拼接json
     */
    protected String doRead(HttpServletRequest req, String uri, boolean lz4) throws IOException {
        byte[] bytes = ByteUtil.doRead(req.getInputStream());
        int rawSize = Integer.parseInt(req.getHeader(RAW_CONTENT_LENGTH));
        if (printRequestBody) {
            logger.info("Request data: uri={}, rawSize={}, contentLength={}, bytes={}, lz4={}",
                    uri, rawSize, req.getContentLength(), ByteUtil.byteToHex(bytes), lz4);
        }
        byte[] data = lz4 ? LZ4Encoder.decompressFromLhLz4Chunk(bytes, rawSize) : bytes;

        ProtobufProperties.ArgTypeConfig argType = this.getArgType(uri);
        if (argType.isList()) {
            List<?> objList = ProtostuffUtil.deserializeList(data, argType.getArgType());
            return String.format("{\"%s\":%s}", argType.getArgName(), JSON.toJsonString(objList));
        } else {
            Object obj = ProtostuffUtil.deserialize(data, argType.getArgType());
            return String.format("{\"%s\":%s}", argType.getArgName(), JSON.toJsonString(obj));
        }
    }

    protected void doResponse(HttpServletResponse resp, ServiceResponse sp, boolean lz4) throws IOException {
        String json = sp.getResult();
        if (sp.getStatus() == NORMAL) {
            this.doWrite(resp, lz4, SC_OK, json);
        } else {
            this.doWrite(resp, lz4, SC_INTERNAL_SERVER_ERROR, json);
        }
    }

    protected void doWrite(HttpServletResponse resp, boolean lz4, int code, String json) throws IOException {
        byte[] serialize = ProtostuffUtil.serialize(json);
        byte[] data = lz4 ? Lz4Util.compressByStream(serialize) : serialize;
        String contentType = CONTENT_TYPE_PREFIX + (lz4 ? PROTOBUF_LZ4 : PROTOBUF);
        resp.setStatus(code);
        resp.setCharacterEncoding(CHARSET);
        resp.setContentType(contentType);
        resp.setHeader("Cache-Control", "no-cache");
        resp.getOutputStream().write(data);
    }

    private ProtobufProperties.ArgTypeConfig getArgType(String uri) {
        if (this.argTypeConfigs == null || this.argTypeConfigs.isEmpty()) {
            throw new UnsupportedOperationException("Not found arg type of " + uri);
        }
        for (ProtobufProperties.ArgTypeConfig argTypeConfig : this.argTypeConfigs) {
            if (argTypeConfig.getUri().equalsIgnoreCase(uri)) {
                return argTypeConfig;
            }
        }
        throw new UnsupportedOperationException("Not found arg type of " + uri);
    }

}
