package next.fire.boot.swift.protobuf;

import com.talkyun.openx.server.core.InterceptorException;
import com.talkyun.openx.server.error.PageBuilder;
import com.talkyun.openx.server.session.SessionManager;
import next.fire.boot.swift.protobuf.autoconfigure.ProtobufProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by daibing on 2021/8/19.
 */
public class ProtostuffFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProtostuffFilter.class);
    private final SessionManager sessionManager = SessionManager.get();
    private final PageBuilder pageBuilder = PageBuilder.get();
    private final ProtobufProperties properties;
    private ProtostuffServletHandler handler;

    public ProtostuffFilter(ProtobufProperties properties) {
        this.properties = properties;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        handler = new ProtostuffServletHandler(filterConfig.getServletContext(), Arrays.asList(properties.getThroughHeaderPrefixes()),
                properties.getArgTypeConfigs(), properties.isPrintRequestBody());
        LOGGER.info("-> protobuf is ready.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (isBlank(request.getContentType())) {
            chain.doFilter(request, response);
            return;
        }
        String contentType = request.getContentType().toLowerCase();
        if (contentType.contains(ProtoConstant.PROTOBUF)) {
            sessionManager.updateActiveTime(((HttpServletRequest) request).getSession().getId());
            try {
                this.doProtoFilter(request, response, chain, contentType.contains(ProtoConstant.PROTOBUF_LZ4));
            } catch (InterceptorException e) {
                LOGGER.error(e.getMessage(), e);
                // response json (include exception)
                pageBuilder.responseJsonPage((HttpServletRequest) request, (HttpServletResponse) response, e.getCause());
            } catch (Throwable t) {
                LOGGER.error(t.getMessage(), t);
                // response 500 page
                pageBuilder.responseErrorPage((HttpServletRequest) request, (HttpServletResponse) response, t);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // ...
    }

    public void doProtoFilter(ServletRequest request, ServletResponse response, FilterChain chain, boolean lz4) throws IOException {
        handler.handle((HttpServletRequest) request, (HttpServletResponse) response, lz4);
    }

    private boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }
}
