package next.fire.boot.swift.protobuf.helper;

import net.jpountz.lz4.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by daibing on 2021/8/19.
 */
public class Lz4Util {
    private static final int ARRAY_SIZE = 1024;

    public static byte[] compressByStream(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return new byte[0];
        }
        LZ4Compressor compressor = LZ4Factory.fastestInstance().fastCompressor();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (LZ4BlockOutputStream compressOutputStream = new LZ4BlockOutputStream(outputStream, ARRAY_SIZE, compressor)) {
            compressOutputStream.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException("stream compress bytes error: ", e);
        }
        return outputStream.toByteArray();
    }

    public static byte[] decompressByStream(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return new byte[0];
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(ARRAY_SIZE);
        LZ4FastDecompressor decompressor = LZ4Factory.fastestInstance().fastDecompressor();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        try (LZ4BlockInputStream decompressedInputStream = new LZ4BlockInputStream(inputStream, decompressor)) {
            int count;
            byte[] buffer = new byte[ARRAY_SIZE];
            while ((count = decompressedInputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, count);
            }
        } catch (IOException e) {
            throw new RuntimeException("stream decompress bytes error: ", e);
        }
        return outputStream.toByteArray();
    }

}
