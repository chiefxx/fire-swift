package next.fire.boot.swift.protobuf.autoconfigure;

import next.fire.boot.swift.protobuf.ProtostuffFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import javax.servlet.Filter;

/**
 * Created by daibing on 2021/8/19.
 */
@Configuration
@EnableConfigurationProperties(ProtobufProperties.class)
@ConditionalOnClass({ProtobufProperties.class})
public class ProtobufAutoConfiguration {
    private final ProtobufProperties properties;

    public ProtobufAutoConfiguration(ProtobufProperties properties) {
        this.properties = properties;
    }

    @Bean
    public FilterRegistrationBean<Filter> authFilterRegistration() {
        ProtostuffFilter protostuffFilter = new ProtostuffFilter(properties);
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>();
        registration.setFilter(protostuffFilter);
        registration.addUrlPatterns(properties.getUrlPatterns());
        registration.setName("protostuffFilter");
        registration.setOrder(Ordered.LOWEST_PRECEDENCE - 10000);
        return registration;
    }
}
