package next.fire.boot.swift.protobuf;

/**
 * Created by daibing on 2021/8/24.
 */
public class ProtoConstant {

    public static final String CONTENT_TYPE_PREFIX = "application/";

    public static final String PROTOBUF = "x-protobuf";

    public static final String PROTOBUF_LZ4 = "x-protobuf-lz4";

    public static final String RAW_CONTENT_LENGTH = "x-raw-content-length";

}
