package next.fire.boot.swift.protobuf.helper;

import io.protostuff.LinkedBuffer;
import io.protostuff.ProtostuffIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by daibing on 2021/8/19.
 */
public class ProtostuffUtil {
    /**
     * 缓存Schema
     */
    private static final Map<Class<?>, Schema<?>> schemaCache = new ConcurrentHashMap<>();

    /**
     * 序列化方法，把指定对象序列化成字节数组
     */
    @SuppressWarnings("unchecked")
    public static <T> byte[] serialize(T obj) {
        if (obj == null) {
            return new byte[0];
        }
        Class<T> clazz = (Class<T>) obj.getClass();
        Schema<T> schema = getSchema(clazz);
        LinkedBuffer buffer = LinkedBuffer.allocate(LinkedBuffer.DEFAULT_BUFFER_SIZE);
        try {
            return ProtostuffIOUtil.toByteArray(obj, schema, buffer);
        } finally {
            buffer.clear();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> byte[] serializeList(List<T> objList) throws IOException {
        if (objList == null || objList.isEmpty()) {
            return new byte[0];
        }
        Schema<T> schema = getSchema((Class<T>) objList.get(0).getClass());
        LinkedBuffer buffer = LinkedBuffer.allocate(LinkedBuffer.DEFAULT_BUFFER_SIZE);
        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();;
            ProtostuffIOUtil.writeListTo(bos, objList, schema, buffer);
            return bos.toByteArray();
        } finally {
            buffer.clear();
            try {
                if (bos != null) {
                    bos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 反序列化方法，将字节数组反序列化成指定Class类型
     */
    public static <T> T deserialize(byte[] data, Class<T> clazz) {
        if (data == null || data.length == 0) {
            return null;
        }
        Schema<T> schema = getSchema(clazz);
        T obj = schema.newMessage();
        ProtostuffIOUtil.mergeFrom(data, obj, schema);
        return obj;
    }

    public static <T> List<T> deserializeList(byte[] data, Class<T> clazz) throws IOException {
        if (data == null || data.length == 0) {
            return Collections.emptyList();
        }
        Schema<T> schema = getSchema(clazz);
        return ProtostuffIOUtil.parseListFrom(new ByteArrayInputStream(data), schema);
    }

    /**
     * 从本地获取Schema，如果不存在就调用RuntimeSchema.getSchema()获取（支持线程安全）
     */
    @SuppressWarnings("unchecked")
    private static <T> Schema<T> getSchema(Class<T> clazz) {
        Schema<T> schema = (Schema<T>) schemaCache.get(clazz);
        if (schema != null) {
            return schema;
        }
        schema = RuntimeSchema.getSchema(clazz);
        if (schema == null) {
            throw new RuntimeException("runtime schema initial failed: " + clazz.getName());
        }
        schemaCache.put(clazz, schema);
        return schema;
    }
}
