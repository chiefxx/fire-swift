package next.fire.boot.swift.protobuf.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * Created by daibing on 2021/8/19.
 */
@ConfigurationProperties(prefix = "swift.protobuf")
public class ProtobufProperties {
    private String urlPatterns = "/*";
    private List<ArgTypeConfig>  argTypeConfigs;
    private String[] throughHeaderPrefixes = new String[]{"x-"};
    private boolean printRequestBody = false;

    public String getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(String urlPatterns) {
        this.urlPatterns = urlPatterns;
    }

    public List<ArgTypeConfig> getArgTypeConfigs() {
        return argTypeConfigs;
    }

    public void setArgTypeConfigs(List<ArgTypeConfig> argTypeConfigs) {
        this.argTypeConfigs = argTypeConfigs;
    }

    public String[] getThroughHeaderPrefixes() {
        return throughHeaderPrefixes;
    }

    public void setThroughHeaderPrefixes(String[] throughHeaderPrefixes) {
        this.throughHeaderPrefixes = throughHeaderPrefixes;
    }

    public boolean isPrintRequestBody() {
        return printRequestBody;
    }

    public void setPrintRequestBody(boolean printRequestBody) {
        this.printRequestBody = printRequestBody;
    }

    public static class ArgTypeConfig {
        private String uri;
        private boolean list;
        private String argName;
        private Class<?> argType;

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public boolean isList() {
            return list;
        }

        public void setList(boolean list) {
            this.list = list;
        }

        public String getArgName() {
            return argName;
        }

        public void setArgName(String argName) {
            this.argName = argName;
        }

        public Class<?> getArgType() {
            return argType;
        }

        public void setArgType(Class<?> argType) {
            this.argType = argType;
        }
    }
}
