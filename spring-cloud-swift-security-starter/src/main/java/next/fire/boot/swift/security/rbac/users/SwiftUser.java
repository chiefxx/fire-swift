package next.fire.boot.swift.security.rbac.users;

import next.fire.boot.swift.security.auth.model.User;

/**
 * Created by daibing on 2021/8/10.
 */
public class SwiftUser extends User {
    private String token;
    private boolean globalAdmin = false;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isGlobalAdmin() {
        return globalAdmin;
    }

    public void setGlobalAdmin(boolean globalAdmin) {
        this.globalAdmin = globalAdmin;
    }

    @Override
    public String toString() {
        return "SwiftUser{" +
                "token='" + token + '\'' +
                ", globalAdmin=" + globalAdmin +
                '}';
    }
}
