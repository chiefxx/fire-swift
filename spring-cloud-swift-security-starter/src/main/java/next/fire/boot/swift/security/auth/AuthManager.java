package next.fire.boot.swift.security.auth;

import next.fire.boot.swift.security.auth.model.Permission;
import next.fire.boot.swift.security.auth.model.User;

/**
 * Created by daibing on 2021/8/4.
 * @author refer to AuthManager of version nacos-2.0.3
 */
public interface AuthManager {

    /**
     * Authentication of request, identify the user who request the resource.
     *
     * @param request where we can find the user information
     * @return user related to this request, null if no user info is found.
     */
    User login(Object request);

    /**
     * Authentication of request, identify the user who request the resource.
     *
     * @param request where we can find the user information
     * @return user related to this request, null if no user info is found.
     */
    User loginRemote(Object request);

    /**
     * Authorization of request, constituted with resource and user.
     *
     * @param permission permission to auth
     * @param user       user who wants to access the resource.
     */
    void auth(Permission permission, User user);
}
