package next.fire.boot.swift.security.autoconfigure;

import io.jsonwebtoken.io.Decoders;
import next.fire.boot.swift.security.auth.model.AuthSystemTypeEnum;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by daibing on 2021/8/4.
 */
@ConfigurationProperties(prefix = "swift.security")
public class SecurityProperties {
    private boolean cachingEnabled = false;
    private boolean authEnabled = false;
    private String secretKey = "SecretKey012345678901234567890123456789012345678901234567890123456789";
    private byte[] secretKeyBytes;
    private long tokenValidityInSeconds = 18000;
    private AuthSystemTypeEnum authSystemType = AuthSystemTypeEnum.SWIFT;
    private String serverIdentityKey = "serverIdentity";
    private String serverIdentityValue = "security";
    private boolean enableUserAgentAuthWhite = false;
    private String ldapUserNamePattern = "cn={0},ou=user,dc=company,dc=com";
    private String ldapUrl = "ldap://localhost:389";
    private int ldapTimeout = 3000;

    public boolean isCachingEnabled() {
        return cachingEnabled;
    }

    public void setCachingEnabled(boolean cachingEnabled) {
        this.cachingEnabled = cachingEnabled;
    }

    public boolean isAuthEnabled() {
        return authEnabled;
    }

    public void setAuthEnabled(boolean authEnabled) {
        this.authEnabled = authEnabled;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public byte[] getSecretKeyBytes() {
        if (secretKeyBytes == null) {
            secretKeyBytes = Decoders.BASE64.decode(secretKey);
        }
        return secretKeyBytes;
    }

    public void setSecretKeyBytes(byte[] secretKeyBytes) {
        this.secretKeyBytes = secretKeyBytes;
    }

    public long getTokenValidityInSeconds() {
        return tokenValidityInSeconds;
    }

    public void setTokenValidityInSeconds(long tokenValidityInSeconds) {
        this.tokenValidityInSeconds = tokenValidityInSeconds;
    }

    public AuthSystemTypeEnum getAuthSystemType() {
        return authSystemType;
    }

    public void setAuthSystemType(AuthSystemTypeEnum authSystemType) {
        this.authSystemType = authSystemType;
    }

    public String getServerIdentityKey() {
        return serverIdentityKey;
    }

    public void setServerIdentityKey(String serverIdentityKey) {
        this.serverIdentityKey = serverIdentityKey;
    }

    public String getServerIdentityValue() {
        return serverIdentityValue;
    }

    public void setServerIdentityValue(String serverIdentityValue) {
        this.serverIdentityValue = serverIdentityValue;
    }

    public boolean isEnableUserAgentAuthWhite() {
        return enableUserAgentAuthWhite;
    }

    public void setEnableUserAgentAuthWhite(boolean enableUserAgentAuthWhite) {
        this.enableUserAgentAuthWhite = enableUserAgentAuthWhite;
    }

    public String getLdapUserNamePattern() {
        return ldapUserNamePattern;
    }

    public void setLdapUserNamePattern(String ldapUserNamePattern) {
        this.ldapUserNamePattern = ldapUserNamePattern;
    }

    public String getLdapUrl() {
        return ldapUrl;
    }

    public void setLdapUrl(String ldapUrl) {
        this.ldapUrl = ldapUrl;
    }

    public int getLdapTimeout() {
        return ldapTimeout;
    }

    public void setLdapTimeout(int ldapTimeout) {
        this.ldapTimeout = ldapTimeout;
    }
}
