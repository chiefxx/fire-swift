package next.fire.boot.swift.security.rbac.users;

import next.fire.boot.swift.security.auth.model.Page;
import next.fire.boot.swift.security.auth.model.User;
import next.fire.boot.swift.security.autoconfigure.SecurityProperties;
import next.fire.boot.swift.security.persist.UserPersistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by daibing on 2021/8/6.
 * @author refer to NacosUserDetailsServiceImpl of version nacos-2.0.3
 */
public class SwiftUserDetailsServiceImpl implements UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftUserDetailsServiceImpl.class);
    private Map<String, User> userMap = new ConcurrentHashMap<>();
    private final SecurityProperties properties;
    private final UserPersistService userPersistService;

    public SwiftUserDetailsServiceImpl(SecurityProperties properties, UserPersistService userPersistService) {
        this.properties = properties;
        this.userPersistService = userPersistService;
    }

    @Scheduled(initialDelay = 5000, fixedDelay = 15000)
    private void reload() {
        try {
            Page<User> users = getUsersFromDatabase(1, Integer.MAX_VALUE);
            if (users == null) {
                return;
            }

            Map<String, User> map = new ConcurrentHashMap<>(16);
            for (User user : users.getPageItems()) {
                map.put(user.getUsername(), user);
            }
            userMap = map;
        } catch (Exception e) {
            LOGGER.warn("[LOAD-USERS] load failed", e);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMap.get(username);
        if (!properties.isCachingEnabled()) {
            user = userPersistService.findUserByUsername(username);
        }

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new SwiftUserDetails(user);
    }

    public void updateUserPassword(String username, String password) {
        userPersistService.updateUserPassword(username, password);
    }

    public Page<User> getUsersFromDatabase(int pageNo, int pageSize) {
        return userPersistService.getUsers(pageNo, pageSize);
    }

    public User getUser(String username) {
        User user = userMap.get(username);
        if (!properties.isCachingEnabled() || user == null) {
            user = getUserFromDatabase(username);
        }
        return user;
    }

    public User getUserFromDatabase(String username) {
        return userPersistService.findUserByUsername(username);
    }

    public List<String> findUserLikeUsername(String username) {
        return userPersistService.findUserLikeUsername(username);
    }

    public void createUser(String username, String password) {
        userPersistService.createUser(username, password);
    }

    public void deleteUser(String username) {
        userPersistService.deleteUser(username);
    }
}
