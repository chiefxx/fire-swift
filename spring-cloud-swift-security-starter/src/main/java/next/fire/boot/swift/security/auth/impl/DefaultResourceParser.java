package next.fire.boot.swift.security.auth.impl;

import next.fire.boot.swift.security.auth.ResourceParser;
import next.fire.boot.swift.security.auth.model.SecurityConstant;

/**
 * Created by daibing on 2021/8/4.
 */
public class DefaultResourceParser implements ResourceParser {

    @Override
    public String parseName(Object request) {
        return SecurityConstant.EMPTY;
    }
}
