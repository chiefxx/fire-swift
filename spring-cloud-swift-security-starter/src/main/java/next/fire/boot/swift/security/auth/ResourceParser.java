package next.fire.boot.swift.security.auth;

/**
 * Created by daibing on 2021/8/4.
 * @author refer to ResourceParser of version nacos-2.0.3
 */
public interface ResourceParser {

    /**
     * Parse a unique name of the resource from the request.
     *
     * @param request where we can find the resource info. Given it may vary from Http request to gRPC request, we use a
     *                Object type for future accommodation.
     * @return resource name
     */
    String parseName(Object request);
}
