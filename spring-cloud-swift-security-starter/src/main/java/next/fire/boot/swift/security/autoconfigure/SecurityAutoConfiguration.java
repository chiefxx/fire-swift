package next.fire.boot.swift.security.autoconfigure;

import next.fire.boot.swift.security.AuthFilter;
import next.fire.boot.swift.security.OpenxAuthInterceptor;
import next.fire.boot.swift.security.auth.model.AuthSystemTypeEnum;
import next.fire.boot.swift.security.persist.PermissionPersistService;
import next.fire.boot.swift.security.persist.RolePersistService;
import next.fire.boot.swift.security.persist.UserPersistService;
import next.fire.boot.swift.security.provider.jwt.JwtAuthenticationEntryPoint;
import next.fire.boot.swift.security.provider.jwt.JwtAuthenticationTokenFilter;
import next.fire.boot.swift.security.provider.jwt.JwtTokenManager;
import next.fire.boot.swift.security.provider.ldap.LdapAuthenticationProvider;
import next.fire.boot.swift.security.provider.swift.SwiftAuthManager;
import next.fire.boot.swift.security.rbac.roles.SwiftRoleServiceImpl;
import next.fire.boot.swift.security.rbac.users.SwiftUserDetailsServiceImpl;
import next.fire.boot.swift.security.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsUtils;

import javax.servlet.Filter;

/**
 * Created by daibing on 2021/8/5.
 * @author refer to NacosAuthConfig of version nacos-2.0.3
 */
@Configuration
@EnableConfigurationProperties(SecurityProperties.class)
@ConditionalOnClass({SecurityProperties.class})
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityAutoConfiguration extends WebSecurityConfigurerAdapter {
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String SECURITY_IGNORE_URLS_SPILT_CHAR = ",";
    // fixme
    public static final String LOGIN_ENTRY_POINT = "/v1/auth/login";
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/v1/auth/**";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String CONSOLE_RESOURCE_NAME_PREFIX = "console/";
    public static final String UPDATE_PASSWORD_ENTRY_POINT = CONSOLE_RESOURCE_NAME_PREFIX + "user/password";
    private static final String DEFAULT_ALL_PATH_PATTERN = "/**";
    private static final String PROPERTY_IGNORE_URLS = "swift.security.ignore.urls";
    private final SecurityProperties properties;

    @Autowired
    private Environment env;

    /**
     * 业务工程自己提供用户、角色、权限的实现类
     */
    @Autowired
    private UserPersistService userPersistService;

    @Autowired
    private RolePersistService rolePersistService;

    @Autowired
    private PermissionPersistService permissionPersistService;

    public SecurityAutoConfiguration(SecurityProperties properties) {
        this.properties = properties;
    }

    @Bean
    public OpenxAuthInterceptor authInterceptor() throws Exception {
        SwiftAuthManager authManager = new SwiftAuthManager(properties, jwtTokenManager(), swiftRoleServiceImpl(), authenticationManagerBean());
        return new OpenxAuthInterceptor(properties, authManager);
    }

    @Bean
    public JwtTokenManager jwtTokenManager() {
        return new JwtTokenManager(properties);
    }

    @Bean
    public SwiftRoleServiceImpl swiftRoleServiceImpl() {
        return new SwiftRoleServiceImpl(properties, userPersistService, rolePersistService, permissionPersistService);
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public FilterRegistrationBean<Filter> authFilterRegistration() {
        AuthFilter authFilter = new AuthFilter(properties);
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>();
        registration.setFilter(authFilter);
        registration.addUrlPatterns("/*");
        registration.setName("authFilter");
        registration.setOrder(6);
        return registration;
    }

    @Bean
    public AuthenticationProvider ldapAuthenticationProvider() {
        return new LdapAuthenticationProvider(properties, (SwiftUserDetailsServiceImpl) swiftUserDetailsServiceImpl(), swiftRoleServiceImpl());
    }

    @Bean
    public UserDetailsService swiftUserDetailsServiceImpl() {
        return new SwiftUserDetailsServiceImpl(properties, userPersistService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        switch (properties.getAuthSystemType()) {
            case SWIFT:
                // auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
                auth.userDetailsService(swiftUserDetailsServiceImpl()).passwordEncoder(passwordEncoder());
                break;
            case LDAP:
                auth.authenticationProvider(ldapAuthenticationProvider());
                break;
            case JWT:
            default:
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        if (AuthSystemTypeEnum.JWT == properties.getAuthSystemType()) {
            http.csrf().disable().cors() // We don't need CSRF for JWT based authentication
                    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and().authorizeRequests().requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                    .antMatchers(LOGIN_ENTRY_POINT).permitAll()
                    .and().authorizeRequests().antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).authenticated()
                    .and().exceptionHandling().authenticationEntryPoint(new JwtAuthenticationEntryPoint());

            // disable cache
            http.headers().cacheControl();

            // register jwt auth token filter
            JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter(jwtTokenManager());
            http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        }
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        String ignoreUrls = null;
        switch (properties.getAuthSystemType()) {
            case SWIFT:
                ignoreUrls = DEFAULT_ALL_PATH_PATTERN;
                break;
            case LDAP:
                ignoreUrls = DEFAULT_ALL_PATH_PATTERN;
                break;
            case JWT:
                ignoreUrls = env.getProperty(PROPERTY_IGNORE_URLS, DEFAULT_ALL_PATH_PATTERN);
                break;
            default:
        }
        if (StringUtils.isNotBlank(ignoreUrls)) {
            for (String each : ignoreUrls.trim().split(SECURITY_IGNORE_URLS_SPILT_CHAR)) {
                web.ignoring().antMatchers(each.trim());
            }
        }
    }

}
