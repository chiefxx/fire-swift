package next.fire.boot.swift.security.auth.model;

/**
 * Created by daibing on 2021/7/7.
 */
public enum ActionTypeEnum {
    READ,
    WRITE,
    ;
    public static ActionTypeEnum get(String raw) {
        try {
            return ActionTypeEnum.valueOf(raw.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }

    public String getDesc() {
        switch (this) {
            case READ:
                return "读";
            case WRITE:
                return "写";
            default:
                return "无效动作类型: " + this;
        }
    }
}
