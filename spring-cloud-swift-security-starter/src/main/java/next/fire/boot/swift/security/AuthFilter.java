package next.fire.boot.swift.security;

import next.fire.boot.swift.security.auth.model.SecurityConstant;
import next.fire.boot.swift.security.autoconfigure.SecurityProperties;
import next.fire.boot.swift.security.utils.StringUtils;
import next.fire.boot.swift.security.utils.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by daibing on 2021/8/5.
 * @author refer to AuthFilter of version nacos-2.0.3
 */
public class AuthFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFilter.class);
    private final SecurityProperties properties;

    public AuthFilter(SecurityProperties properties) {
        this.properties = properties;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!properties.isAuthEnabled()) {
            chain.doFilter(request, response);
            return;
        }

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        if (properties.isEnableUserAgentAuthWhite()) {
            String userAgent = WebUtils.getUserAgent(req);
            if (StringUtils.startsWith(userAgent, SecurityConstant.SWIFT_SERVER_HEADER)) {
                chain.doFilter(request, response);
                return;
            }
        } else if (StringUtils.isNotBlank(properties.getServerIdentityKey()) && StringUtils
                .isNotBlank(properties.getServerIdentityValue())) {
            String serverIdentity = req.getHeader(properties.getServerIdentityKey());
            if (properties.getServerIdentityValue().equals(serverIdentity)) {
                chain.doFilter(request, response);
                return;
            }
            LOGGER.warn("Invalid server identity value for {} from {}", properties.getServerIdentityKey(), req.getRemoteHost());
        } else {
            resp.sendError(HttpServletResponse.SC_FORBIDDEN,
                    "Invalid server identity key or value, Please make sure set `nacos.core.auth.server.identity.key`"
                            + " and `nacos.core.auth.server.identity.value`, or open `nacos.core.auth.enable.userAgentAuthWhite`");
            return;
        }
        chain.doFilter(request, response);

        /**
         * 注意：AuthFilter#doFilter后面一部分业务逻辑迁移到 OpenxAuthInterceptor 完成：
         * 1、迁移的这一部分逻辑需要根据Req获取Method（进一步获取Secured注解）
         * 2、Openx项目已经有这一块逻辑（根据body获取参数名获取method），避免重复解析body
         * 3、综合考虑将这一块逻辑迁移到openx拦截器
         */
    }

    @Override
    public void destroy() {
        // nothing.
    }

}
