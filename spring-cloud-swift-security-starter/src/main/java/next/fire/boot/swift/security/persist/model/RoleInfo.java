package next.fire.boot.swift.security.persist.model;

/**
 * Created by daibing on 2021/8/6.
 */
public class RoleInfo {
    private String role;
    private String username;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "RoleInfo{" +
                "role='" + role + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
