package next.fire.boot.swift.security.auth.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daibing on 2021/8/10.
 */
public class Page<E> {
    /**
     * totalCount.
     */
    private int totalCount;

    /**
     * pageNumber.
     */
    private int pageNumber;

    /**
     * pagesAvailable.
     */
    private int pagesAvailable;

    /**
     * pageItems.
     */
    private List<E> pageItems = new ArrayList<E>();

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPagesAvailable() {
        return pagesAvailable;
    }

    public void setPagesAvailable(int pagesAvailable) {
        this.pagesAvailable = pagesAvailable;
    }

    public List<E> getPageItems() {
        return pageItems;
    }

    public void setPageItems(List<E> pageItems) {
        this.pageItems = pageItems;
    }
}
