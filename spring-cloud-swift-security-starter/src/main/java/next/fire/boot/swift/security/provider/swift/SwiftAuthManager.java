package next.fire.boot.swift.security.provider.swift;

import com.talkyun.openx.RestfulHeaderManager;
import com.talkyun.openx.server.core.ServiceRequest;
import io.jsonwebtoken.ExpiredJwtException;
import next.fire.boot.swift.security.auth.AuthManager;
import next.fire.boot.swift.security.auth.exception.AccessException;
import next.fire.boot.swift.security.auth.model.Permission;
import next.fire.boot.swift.security.auth.model.SecurityConstant;
import next.fire.boot.swift.security.auth.model.User;
import next.fire.boot.swift.security.autoconfigure.SecurityAutoConfiguration;
import next.fire.boot.swift.security.autoconfigure.SecurityProperties;
import next.fire.boot.swift.security.persist.model.RoleInfo;
import next.fire.boot.swift.security.provider.jwt.JwtTokenManager;
import next.fire.boot.swift.security.rbac.roles.SwiftRoleServiceImpl;
import next.fire.boot.swift.security.rbac.users.SwiftUser;
import next.fire.boot.swift.security.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by daibing on 2021/8/5.
 * @author refer to NacosAuthManager of version nacos-2.0.3
 */
public class SwiftAuthManager implements AuthManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftAuthManager.class);
    private static final String TOKEN_PREFIX = "Bearer ";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_PASSWORD = "password";

    private final SecurityProperties properties;
    private final JwtTokenManager tokenManager;
    private final SwiftRoleServiceImpl roleService;
    private final AuthenticationManager authenticationManager;

    public SwiftAuthManager(SecurityProperties properties, JwtTokenManager tokenManager, SwiftRoleServiceImpl roleService, AuthenticationManager authenticationManager) {
        this.properties = properties;
        this.tokenManager = tokenManager;
        this.roleService = roleService;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public User login(Object request) {
        String token = resolveToken(request);
        if (StringUtils.isBlank(token)) {
            throw new AccessException("swift.login.without.token", "user not found!");
        }

        try {
            tokenManager.validateToken(token);
        } catch (ExpiredJwtException e) {
            throw new AccessException("swift.login.expired.token", "token expired!");
        } catch (Exception e) {
            throw new AccessException("swift.login.error.token", "token invalid!");
        }

        Authentication authentication = tokenManager.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String username = authentication.getName();
        SwiftUser user = new SwiftUser();
        user.setUsername(username);
        user.setToken(token);
        List<RoleInfo> roleInfoList = roleService.getRoles(username);
        if (roleInfoList != null) {
            for (RoleInfo roleInfo : roleInfoList) {
                if (roleInfo.getRole().equals(SwiftRoleServiceImpl.GLOBAL_ADMIN_ROLE)) {
                    user.setGlobalAdmin(true);
                    break;
                }
            }
        }
        // todo: HttpServletRequest  req.setAttribute(SecurityConstant.SWIFT_USER_KEY, user);
        return user;
    }

    @Override
    public User loginRemote(Object request) {
        // gRPC
        return null;
    }

    @Override
    public void auth(Permission permission, User user) {
        LOGGER.info("auth permission: {}, user: {}", permission, user);
        if (!roleService.hasPermission(user.getUsername(), permission)) {
            throw new AccessException("swift.auth.without.permission", "authorization failed!");
        }
    }

    /**
     * Get token from header support openx header
     */
    private String resolveToken(Object request) {
        if (request instanceof HttpServletRequest) {
            return this.resolveToken((HttpServletRequest) request);
        }
        if (request instanceof ServiceRequest) {
            return RestfulHeaderManager.get().getOnceHead().get(SecurityAutoConfiguration.AUTHORIZATION_HEADER);
        }
        return null;
    }

    /**
     * Get token from header.
     */
    private String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(SecurityAutoConfiguration.AUTHORIZATION_HEADER);
        if (StringUtils.isNotBlank(bearerToken) && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        bearerToken = request.getParameter(SecurityConstant.ACCESS_TOKEN);
        if (StringUtils.isBlank(bearerToken)) {
            String userName = request.getParameter(PARAM_USERNAME);
            String password = request.getParameter(PARAM_PASSWORD);
            bearerToken = resolveTokenFromUser(userName, password);
        }

        return bearerToken;
    }

    private String resolveTokenFromUser(String userName, String rawPassword) {
        String finalName;
        Authentication authenticate;
        try {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName,
                    rawPassword);
            authenticate = authenticationManager.authenticate(authenticationToken);
        } catch (AuthenticationException e) {
            throw new AccessException("swift.login.without.token", "unknown user!");
        }

        if (null == authenticate || StringUtils.isBlank(authenticate.getName())) {
            finalName = userName;
        } else {
            finalName = authenticate.getName();
        }

        return tokenManager.createToken(finalName);
    }
}
