package next.fire.boot.swift.security.persist;

import next.fire.boot.swift.security.auth.model.Page;
import next.fire.boot.swift.security.persist.model.PermissionInfo;

/**
 * Created by daibing on 2021/8/6.
 * @author refer to PermissionPersistService of version nacos-2.0.3
 */
public interface PermissionPersistService {
    /**
     * get the permissions of role by page.
     *
     * @param role role
     * @param pageNo pageNo
     * @param pageSize pageSize
     * @return permissions page info
     */
    Page<PermissionInfo> getPermissions(String role, int pageNo, int pageSize);

    /**
     * assign permission to role.
     *
     * @param role role
     * @param resource resource
     * @param action action
     */
    void addPermission(String role, String resource, String action);

    /**
     * delete the role's permission.
     *
     * @param role role
     * @param resource resource
     * @param action action
     */
    void deletePermission(String role, String resource, String action);
}
