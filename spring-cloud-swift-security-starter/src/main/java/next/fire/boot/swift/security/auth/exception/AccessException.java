package next.fire.boot.swift.security.auth.exception;

/**
 * Created by daibing on 2021/8/4.
 */
public class AccessException extends RuntimeException {
    private static final long serialVersionUID = -2973098267737500900L;
    public static final String SECURITY_ERROR = "FIRE.SWIFT.SECURITY.ERROR";

    private String code = "FIRE.SWIFT.SECURITY.SYSTEM.ERROR";
    private String message = "fire swift security internal error!";

    public AccessException() {
        super();
    }

    public AccessException(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "{\"code\":\"" + code + "\",\"message\":\"" + message + "\",\"class\":\"" + this.getClass().getName() + "\"}";
    }
}
