package next.fire.boot.swift.security.auth;

import next.fire.boot.swift.security.auth.impl.DefaultResourceParser;
import next.fire.boot.swift.security.auth.model.ActionTypeEnum;
import next.fire.boot.swift.security.auth.model.SecurityConstant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by daibing on 2021/8/4.
 * @author refer to Secured of version nacos-2.0.3
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {

    /**
     * The action type of the request.
     */
    ActionTypeEnum action() default ActionTypeEnum.READ;

    /**
     * The name of resource related to the request.
     */
    String resource() default SecurityConstant.EMPTY;

    /**
     * Resource name parser. Should have lower priority than resource().
     */
    Class<? extends ResourceParser> parser() default DefaultResourceParser.class;
}
