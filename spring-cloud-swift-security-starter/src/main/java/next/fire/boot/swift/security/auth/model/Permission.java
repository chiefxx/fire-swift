package next.fire.boot.swift.security.auth.model;

/**
 * Created by daibing on 2021/8/4.
 */
public class Permission {
    private String resource;
    private ActionTypeEnum actionType;

    public Permission() {
    }

    public Permission(String resource, ActionTypeEnum actionType) {
        this.resource = resource;
        this.actionType = actionType;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public ActionTypeEnum getActionType() {
        return actionType;
    }

    public void setActionType(ActionTypeEnum actionType) {
        this.actionType = actionType;
    }
}
