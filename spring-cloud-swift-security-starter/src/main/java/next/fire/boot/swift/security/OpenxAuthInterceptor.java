package next.fire.boot.swift.security;

import com.talkyun.openx.server.core.AbstractInterceptor;
import com.talkyun.openx.server.core.ServiceRequest;
import com.talkyun.openx.server.impl.SimpleServiceFactory;
import com.talkyun.utils.Reflector;
import next.fire.boot.swift.security.auth.AuthManager;
import next.fire.boot.swift.security.auth.ResourceParser;
import next.fire.boot.swift.security.auth.Secured;
import next.fire.boot.swift.security.auth.exception.AccessException;
import next.fire.boot.swift.security.auth.model.ActionTypeEnum;
import next.fire.boot.swift.security.auth.model.Permission;
import next.fire.boot.swift.security.autoconfigure.SecurityProperties;
import next.fire.boot.swift.security.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by daibing on 2021/8/10.
 */
public class OpenxAuthInterceptor extends AbstractInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenxAuthInterceptor.class);
    private final Map<Class<? extends ResourceParser>, ResourceParser> parserInstance = new ConcurrentHashMap<>();
    private final SecurityProperties properties;
    private final AuthManager authManager;

    public OpenxAuthInterceptor(SecurityProperties properties, AuthManager authManager) {
        super.setMatchPattern(Arrays.asList("*.*"));
        this.properties = properties;
        this.authManager = authManager;
    }

    @Override
    protected void doMessageIn(ServiceRequest sr) throws Throwable {
        if (!properties.isAuthEnabled()) {
            return;
        }
        Class<?> clazz = SimpleServiceFactory.getInstance().getService(sr.getMapping()).getClass();
        if (clazz == null) {
            LOGGER.warn("不能取到服务实现类 [{}:{}] ", sr.getMapping(), sr.getMethod());
            return;
        }
        Method method = Reflector.getMethod(clazz, sr.getMethod(), sr.getArgs() != null ? sr.getArgs().size() : 0);
        if (method == null) {
            return;
        }
        if (this.isAnnotationPresentPro(method, Secured.class) && properties.isAuthEnabled()) {
            LOGGER.debug("auth start, request: {} {}", sr.getMapping(), sr.getMethod());
            Secured secured = this.getAnnotationPro(method, Secured.class);
            ActionTypeEnum action = secured.action();
            String resource = secured.resource();

            if (StringUtils.isBlank(resource)) {
                ResourceParser parser = getResourceParser(secured.parser());
                resource = parser.parseName(sr);
            }
            if (StringUtils.isBlank(resource)) {
                // deny if we don't find any resource:
                throw new AccessException("swift.openx.resource.is.null", "resource name invalid!");
            }
            authManager.auth(new Permission(resource, action), authManager.login(sr));
        }
    }

    /**
     * springcglib: enhancerbyspringcglib
     */
    private boolean isAnnotationPresentPro(Method method, Class<? extends Annotation> annotationClass) {
        if (method.isAnnotationPresent(annotationClass)) {
            return true;
        }
        return AnnotationUtils.findAnnotation(method, annotationClass) != null;
    }

    public <T extends Annotation> T getAnnotationPro(Method method, Class<T> annotationClass) {
        if (method.isAnnotationPresent(annotationClass)) {
            return method.getAnnotation(annotationClass);
        }
        return AnnotationUtils.findAnnotation(method, annotationClass);
    }

    private ResourceParser getResourceParser(Class<? extends ResourceParser> parserClass) throws IllegalAccessException, InstantiationException {
        ResourceParser parser = parserInstance.get(parserClass);
        if (parser == null) {
            parser = parserClass.newInstance();
            parserInstance.put(parserClass, parser);
        }
        return parser;
    }
}
