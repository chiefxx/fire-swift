package next.fire.boot.swift.security.rbac.roles;

import io.jsonwebtoken.lang.Collections;
import next.fire.boot.swift.security.auth.model.Page;
import next.fire.boot.swift.security.auth.model.Permission;
import next.fire.boot.swift.security.autoconfigure.SecurityProperties;
import next.fire.boot.swift.security.autoconfigure.SecurityAutoConfiguration;
import next.fire.boot.swift.security.persist.PermissionPersistService;
import next.fire.boot.swift.security.persist.RolePersistService;
import next.fire.boot.swift.security.persist.UserPersistService;
import next.fire.boot.swift.security.persist.model.PermissionInfo;
import next.fire.boot.swift.security.persist.model.RoleInfo;
import next.fire.boot.swift.security.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * Created by daibing on 2021/8/10.
 */
public class SwiftRoleServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftRoleServiceImpl.class);
    public static final String GLOBAL_ADMIN_ROLE = "ROLE_ADMIN";
    private static final int DEFAULT_PAGE_NO = 1;

    private final SecurityProperties properties;
    private final UserPersistService userPersistService;
    private final RolePersistService rolePersistService;
    private final PermissionPersistService permissionPersistService;

    private volatile Set<String> roleSet = ConcurrentHashMap.newKeySet();
    private volatile Map<String, List<RoleInfo>> roleInfoMap = new ConcurrentHashMap<>();
    private volatile Map<String, List<PermissionInfo>> permissionInfoMap = new ConcurrentHashMap<>();

    public SwiftRoleServiceImpl(SecurityProperties properties, UserPersistService userPersistService,
                                RolePersistService rolePersistService, PermissionPersistService permissionPersistService) {
        this.properties = properties;
        this.userPersistService = userPersistService;
        this.rolePersistService = rolePersistService;
        this.permissionPersistService = permissionPersistService;
    }

    @Scheduled(initialDelay = 5000, fixedDelay = 15000)
    private void reload() {
        try {
            Page<RoleInfo> roleInfoPage = rolePersistService
                    .getRolesByUserName(StringUtils.EMPTY, DEFAULT_PAGE_NO, Integer.MAX_VALUE);
            if (roleInfoPage == null) {
                return;
            }
            Set<String> tmpRoleSet = new HashSet<>(16);
            Map<String, List<RoleInfo>> tmpRoleInfoMap = new ConcurrentHashMap<>(16);
            for (RoleInfo roleInfo : roleInfoPage.getPageItems()) {
                if (!tmpRoleInfoMap.containsKey(roleInfo.getUsername())) {
                    tmpRoleInfoMap.put(roleInfo.getUsername(), new ArrayList<>());
                }
                tmpRoleInfoMap.get(roleInfo.getUsername()).add(roleInfo);
                tmpRoleSet.add(roleInfo.getRole());
            }

            Map<String, List<PermissionInfo>> tmpPermissionInfoMap = new ConcurrentHashMap<>(16);
            for (String role : tmpRoleSet) {
                Page<PermissionInfo> permissionInfoPage = permissionPersistService
                        .getPermissions(role, DEFAULT_PAGE_NO, Integer.MAX_VALUE);
                tmpPermissionInfoMap.put(role, permissionInfoPage.getPageItems());
            }

            roleSet = tmpRoleSet;
            roleInfoMap = tmpRoleInfoMap;
            permissionInfoMap = tmpPermissionInfoMap;
        } catch (Exception e) {
            LOGGER.warn("[LOAD-ROLES] load failed", e);
        }
    }

    /**
     * Determine if the user has permission of the resource.
     *
     * <p>Note if the user has many roles, this method returns true if any one role of the user has the desired
     * permission.
     *
     * @param username   user info
     * @param permission permission to auth
     * @return true if granted, false otherwise
     */
    public boolean hasPermission(String username, Permission permission) {
        //update password
        if (SecurityAutoConfiguration.UPDATE_PASSWORD_ENTRY_POINT.equals(permission.getResource())) {
            return true;
        }

        List<RoleInfo> roleInfoList = getRoles(username);
        if (io.jsonwebtoken.lang.Collections.isEmpty(roleInfoList)) {
            return false;
        }

        // Global admin pass:
        for (RoleInfo roleInfo : roleInfoList) {
            if (GLOBAL_ADMIN_ROLE.equals(roleInfo.getRole())) {
                return true;
            }
        }

        // Old global admin can pass resource 'console/':
        if (permission.getResource().startsWith(SecurityAutoConfiguration.CONSOLE_RESOURCE_NAME_PREFIX)) {
            return false;
        }

        // For other roles, use a pattern match to decide if pass or not.
        for (RoleInfo roleInfo : roleInfoList) {
            List<PermissionInfo> permissionInfoList = getPermissions(roleInfo.getRole());
            if (Collections.isEmpty(permissionInfoList)) {
                continue;
            }
            for (PermissionInfo permissionInfo : permissionInfoList) {
                String permissionResource = permissionInfo.getResource().replaceAll("\\*", ".*");
                String permissionAction = permissionInfo.getAction();
                if (permissionAction.contains(permission.getActionType().name()) && Pattern
                        .matches(permissionResource, permission.getResource())) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<RoleInfo> getRoles(String username) {
        List<RoleInfo> roleInfoList = roleInfoMap.get(username);
        if (!properties.isCachingEnabled() || roleInfoList == null) {
            Page<RoleInfo> roleInfoPage = getRolesFromDatabase(username, DEFAULT_PAGE_NO, Integer.MAX_VALUE);
            if (roleInfoPage != null) {
                roleInfoList = roleInfoPage.getPageItems();
            }
        }
        return roleInfoList;
    }

    public Page<RoleInfo> getRolesFromDatabase(String userName, int pageNo, int pageSize) {
        Page<RoleInfo> roles = rolePersistService.getRolesByUserName(userName, pageNo, pageSize);
        if (roles == null) {
            return new Page<>();
        }
        return roles;
    }

    public List<PermissionInfo> getPermissions(String role) {
        List<PermissionInfo> permissionInfoList = permissionInfoMap.get(role);
        if (!properties.isCachingEnabled() || permissionInfoList == null) {
            Page<PermissionInfo> permissionInfoPage = getPermissionsFromDatabase(role, DEFAULT_PAGE_NO, Integer.MAX_VALUE);
            if (permissionInfoPage != null) {
                permissionInfoList = permissionInfoPage.getPageItems();
            }
        }
        return permissionInfoList;
    }

    public Page<PermissionInfo> getPermissionsByRoleFromDatabase(String role, int pageNo, int pageSize) {
        return permissionPersistService.getPermissions(role, pageNo, pageSize);
    }

    /**
     * Add role.
     *
     * @param role     role name
     * @param username user name
     */
    public void addRole(String role, String username) {
        if (userPersistService.findUserByUsername(username) == null) {
            throw new IllegalArgumentException("user '" + username + "' not found!");
        }
        if (GLOBAL_ADMIN_ROLE.equals(role)) {
            throw new IllegalArgumentException("role '" + GLOBAL_ADMIN_ROLE + "' is not permitted to create!");
        }
        rolePersistService.addRole(role, username);
        roleSet.add(role);
    }

    public void deleteRole(String role, String userName) {
        rolePersistService.deleteRole(role, userName);
    }

    public void deleteRole(String role) {
        rolePersistService.deleteRole(role);
        roleSet.remove(role);
    }

    public Page<PermissionInfo> getPermissionsFromDatabase(String role, int pageNo, int pageSize) {
        Page<PermissionInfo> pageInfo = permissionPersistService.getPermissions(role, pageNo, pageSize);
        if (pageInfo == null) {
            return new Page<>();
        }
        return pageInfo;
    }

    /**
     * Add permission.
     *
     * @param role     role name
     * @param resource resource
     * @param action   action
     */
    public void addPermission(String role, String resource, String action) {
        if (!roleSet.contains(role)) {
            throw new IllegalArgumentException("role " + role + " not found!");
        }
        permissionPersistService.addPermission(role, resource, action);
    }

    public void deletePermission(String role, String resource, String action) {
        permissionPersistService.deletePermission(role, resource, action);
    }

    public List<String> findRolesLikeRoleName(String role) {
        return rolePersistService.findRolesLikeRoleName(role);
    }
}
