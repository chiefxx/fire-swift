package next.fire.boot.swift.security.auth.model;

/**
 * Created by daibing on 2021/7/7.
 */
public enum AuthSystemTypeEnum {
    SWIFT,
    LDAP,
    JWT,
    ;
    public static AuthSystemTypeEnum get(String raw) {
        try {
            return AuthSystemTypeEnum.valueOf(raw.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }

    public String getDesc() {
        switch (this) {
            case SWIFT:
                return "自建";
            case LDAP:
                return "AD";
            case JWT:
                return "Json web token";
            default:
                return "无效授权系统类型: " + this;
        }
    }
}
