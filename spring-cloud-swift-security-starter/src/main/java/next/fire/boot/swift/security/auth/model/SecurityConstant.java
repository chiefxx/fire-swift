package next.fire.boot.swift.security.auth.model;

/**
 * Created by daibing on 2021/8/5.
 */
public class SecurityConstant {

    public static final String EMPTY = "";

    public static final String SWIFT_SERVER_HEADER = "Swift-Server";

    public static final String ACCESS_TOKEN = "accessToken";

    public static final String SWIFT_USER_KEY = "swiftUser";

}
