package next.fire.boot.swift.security.persist;

import next.fire.boot.swift.security.auth.model.Page;
import next.fire.boot.swift.security.auth.model.User;

import java.util.List;

/**
 * Created by daibing on 2021/8/6.
 * @author refer to UserPersistService of version nacos-2.0.3
 */
public interface UserPersistService {
    /**
     * create user.
     *
     * @param username username
     * @param password password
     */
    void createUser(String username, String password);

    /**
     * delete user by username.
     *
     * @param username username
     */
    void deleteUser(String username);

    /**
     * update password of the user.
     *
     * @param username username
     * @param password password
     */
    void updateUserPassword(String username, String password);

    /**
     * query user by username.
     *
     * @param username username
     * @return user
     */
    User findUserByUsername(String username);

    /**
     * get users by page.
     *
     * @param pageNo pageNo
     * @param pageSize pageSize
     * @return user page info
     */
    Page<User> getUsers(int pageNo, int pageSize);

    /**
     * fuzzy query user by username.
     *
     * @param username username
     * @return usernames
     */
    List<String> findUserLikeUsername(String username);
}
