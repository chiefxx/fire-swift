package next.fire.boot.swift.security.auth.model;

/**
 * Created by daibing on 2021/8/4.
 */
public class Resource {
    public static final String SPLITTER = ":";
    public static final String ANY = "*";
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String parseName() {
        return key.substring(0, key.lastIndexOf(SPLITTER));
    }

    @Override
    public String toString() {
        return "Resource{" +
                "key='" + key + '\'' +
                '}';
    }
}
