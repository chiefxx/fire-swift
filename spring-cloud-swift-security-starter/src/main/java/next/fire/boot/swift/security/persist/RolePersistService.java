package next.fire.boot.swift.security.persist;

import next.fire.boot.swift.security.auth.model.Page;
import next.fire.boot.swift.security.persist.model.RoleInfo;

import java.util.List;

/**
 * Created by daibing on 2021/8/6.
 * @author refer to RolePersistService of version nacos-2.0.3
 */
public interface RolePersistService {
    /**
     * get roles by page.
     *
     * @param pageNo pageNo
     * @param pageSize pageSize
     * @return roles page info
     */
    Page<RoleInfo> getRoles(int pageNo, int pageSize);

    /**
     * query the user's roles by username.
     *
     * @param username username
     * @param pageNo pageNo
     * @param pageSize pageSize
     * @return roles page info
     */
    Page<RoleInfo> getRolesByUserName(String username, int pageNo, int pageSize);

    /**
     * assign role to user.
     *
     * @param role role
     * @param userName username
     */
    void addRole(String role, String userName);

    /**
     * delete role.
     *
     * @param role role
     */
    void deleteRole(String role);

    /**
     * delete user's role.
     *
     * @param role role
     * @param username username
     */
    void deleteRole(String role, String username);

    /**
     * fuzzy query roles by role name.
     *
     * @param role role
     * @return roles
     */
    List<String> findRolesLikeRoleName(String role);
}
