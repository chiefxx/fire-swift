package next.fire.boot.swift.openx.client;

import next.fire.boot.swift.openx.client.autoconfigure.OpenxClientProperties;
import next.fire.boot.swift.openx.client.factory.SwiftRestfulSpringFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daibing on 2021/7/20.
 */
public class SwiftBeanDefinitionRegistry implements BeanDefinitionRegistryPostProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftBeanDefinitionRegistry.class);
    private static final String SEATA_OPENX_CLIENT_REQUEST_INTERCEPTOR = "next.fire.boot.swift.seata.openx.client.SeataRequestInterceptor";
    private final OpenxClientProperties properties;

    public SwiftBeanDefinitionRegistry(OpenxClientProperties properties) {
        this.properties = properties;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        if (properties == null || properties.getClientBeanConfigs() == null) {
            LOGGER.warn("Not found openx client config!");
            return;
        }
        Class<?> seataOpenxClientRequestInterceptorType = this.buildSeataOpenxClientRequestInterceptorType();
        for (OpenxClientProperties.ClientBeanConfig config : properties.getClientBeanConfigs()) {
            // 公共拦截器在前，业务拦截器在后，最后加载Seata请求拦截器
            List<Class<?>> requestInterceptorTypes = this.buildRequestInterceptorType(
                    properties.getRequestInterceptorType(),
                    config.getRequestInterceptorType(),
                    seataOpenxClientRequestInterceptorType);
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(config.getBeanType());
            GenericBeanDefinition definition = (GenericBeanDefinition) builder.getRawBeanDefinition();
            definition.setBeanClass(SwiftRestfulSpringFactory.class);
            definition.getPropertyValues().add("mapping", config.getMapping());
            definition.getPropertyValues().add("interface", config.getBeanType());
            definition.getPropertyValues().add("url", config.getUrl());
            definition.getPropertyValues().add("timeout", properties.getTimeout());
            definition.getPropertyValues().add("requestInterceptorTypes", requestInterceptorTypes);
            definition.setAutowireMode(GenericBeanDefinition.AUTOWIRE_BY_TYPE);
            registry.registerBeanDefinition(config.getBeanType().getSimpleName(), definition);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        // nothing.
    }

    private Class<?> buildSeataOpenxClientRequestInterceptorType() {
        try {
            Class<?> clazz = Class.forName(SEATA_OPENX_CLIENT_REQUEST_INTERCEPTOR);
            LOGGER.info("build seata openx client request interceptor {} success!", SEATA_OPENX_CLIENT_REQUEST_INTERCEPTOR);
            return clazz;
        } catch (Throwable t) {
            return null;
        }
    }

    private List<Class<?>> buildRequestInterceptorType(Class<?>... clazzArray) {
        List<Class<?>> requestInterceptorTypes = new ArrayList<>();
        for (Class<?> clazz : clazzArray) {
            if (clazz == null) {
                continue;
            }
            requestInterceptorTypes.add(clazz);
        }
        return requestInterceptorTypes;
    }

    protected boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

}
