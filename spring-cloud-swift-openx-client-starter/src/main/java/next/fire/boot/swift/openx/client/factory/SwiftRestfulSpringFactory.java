package next.fire.boot.swift.openx.client.factory;

import org.springframework.beans.factory.FactoryBean;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 1、直接拷贝openx-client的RestfulSpringFactory
 * 2、支持直接设置Class
 */
public class SwiftRestfulSpringFactory extends SwiftRestfulServiceFactory implements FactoryBean {
    private String mapping;
    private Class<?> clazz;

    public SwiftRestfulSpringFactory() {

    }

    public SwiftRestfulSpringFactory(String url, int threadNum) {
        super(url, threadNum);
    }

    public SwiftRestfulSpringFactory(String url, ThreadPoolExecutor executor) {
        super(url, executor);
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    public void setInterface(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Class<?> getInterface() {
        return clazz;
    }

    @Override
    public Object getObject() throws Exception {
        if (mapping == null) {
            return super.getService(clazz);
        } else {
            return super.getService(mapping, clazz);
        }
    }

    @Override
    public Class<?> getObjectType() {
        return clazz;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

}
