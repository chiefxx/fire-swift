package next.fire.boot.swift.openx.client.autoconfigure;

import next.fire.boot.swift.openx.client.SwiftBeanDefinitionRegistry;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.NoSuchElementException;

/**
 * Created by daibing on 2021/7/6.
 */
@Configuration
public class OpenxClientAutoConfiguration implements EnvironmentAware {
    private OpenxClientProperties properties;

    /**
     * SwiftBeanDefinitionRegistry实现BeanDefinitionRegistryPostProcessor接口，需要手工获取配置
     */
//    public OpenxClientAutoConfiguration(OpenxClientProperties properties) {
//        this.properties = properties;
//    }

    @Bean(name = "openxClientRegistry")
    public SwiftBeanDefinitionRegistry openxClientRegistry() {
        return new SwiftBeanDefinitionRegistry(properties);
    }

    @Override
    public void setEnvironment(Environment environment) {
        try {
            properties = Binder.get(environment).bind(OpenxClientProperties.CLIENT_CONF_PREFIX, OpenxClientProperties.class).get();
        } catch (NoSuchElementException e) {
            // ignore if without configure openx.client conf
        }
    }
}
