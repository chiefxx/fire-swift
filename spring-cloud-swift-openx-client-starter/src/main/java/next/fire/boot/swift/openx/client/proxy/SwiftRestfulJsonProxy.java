package next.fire.boot.swift.openx.client.proxy;

import com.talkyun.openx.RestfulHeaderManager;
import com.talkyun.openx.client.RestfulJsonProxy;
import next.fire.boot.swift.openx.client.interceptor.RequestInterceptor;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by daibing on 2021/7/21.
 */
public class SwiftRestfulJsonProxy extends RestfulJsonProxy {
    private List<RequestInterceptor> requestInterceptors;

    public SwiftRestfulJsonProxy() {
    }

    public SwiftRestfulJsonProxy(String url, String mapping, int timeout, List<RequestInterceptor> requestInterceptors) {
        super(url, mapping, timeout);
        this.requestInterceptors = requestInterceptors;
    }


    @Override
    protected Object doInvoke(Object proxy, Method method, Object[] args) throws Exception {
        for (RequestInterceptor requestInterceptor : requestInterceptors) {
            requestInterceptor.apply(RestfulHeaderManager.get());
        }
        return super.doInvoke(proxy, method, args);
    }

}
