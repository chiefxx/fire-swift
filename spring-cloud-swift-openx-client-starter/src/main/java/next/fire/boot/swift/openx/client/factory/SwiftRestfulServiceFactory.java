package next.fire.boot.swift.openx.client.factory;

import com.talkyun.openx.client.RestfulJsonProxy;
import next.fire.boot.swift.openx.client.interceptor.RequestInterceptor;
import next.fire.boot.swift.openx.client.proxy.SwiftRestfulJsonProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 1、直接拷贝openx-client的RestfulServiceFactory
 * 2、替换RestfulJsonProxy为SwiftRestfulJsonProxy
 * 3、新增客户端请求拦截器2个：一个公共拦截器，一个业务拦截器
 */
public class SwiftRestfulServiceFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftRestfulServiceFactory.class);
    private String url;
    private int timeout = 3 * 1000; // 3 second

    private String httpProxyAddress;       // host:port
    private String httpProxyAuthorization; // username:password
    private List<Class<? extends RequestInterceptor>> requestInterceptorTypes;

    public SwiftRestfulServiceFactory() {
    }

    @Deprecated
    public SwiftRestfulServiceFactory(boolean sync) {
        // Deprecated
    }

    public SwiftRestfulServiceFactory(String url) {
        this(url, 2);
    }

    @Deprecated
    public SwiftRestfulServiceFactory(String url, int threadNum) {
        this.url = url;
    }

    @Deprecated
    public SwiftRestfulServiceFactory(String url, ThreadPoolExecutor executor) {
        this.url = url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTimeout(int timeout) {
        if (timeout < 500) {
            throw new RuntimeException("Timeout setting is too small! " + timeout);
        }
        this.timeout = timeout;
    }

    public void setHttpProxyAddress(String httpProxyAddress) {
        this.httpProxyAddress = httpProxyAddress;
    }

    public void setHttpProxyAuthorization(String httpProxyAuthorization) {
        this.httpProxyAuthorization = httpProxyAuthorization;
    }

    public void setRequestInterceptorTypes(List<Class<? extends RequestInterceptor>> requestInterceptorTypes) {
        this.requestInterceptorTypes = requestInterceptorTypes;
    }

    @Deprecated
    public void setSyncInvoke(boolean sync) {
    }

    @Deprecated
    public void setExecutor(ThreadPoolExecutor executor) {
    }

    public String getUrl() {
        return url;
    }

    public <T> T getService(String mapping, Class<T> clazz) {
        return this.getService(mapping, clazz, timeout);
    }

    @SuppressWarnings("unchecked")
    public <T> T getService(String mapping, Class<T> clazz, int timeout) {
        try {
            return (T) getRemoteProxy(mapping, clazz, timeout);
        } catch (Exception e) {
            throw new RuntimeException("Can't lookup " + mapping + ":" + clazz.getName(), e);
        }
    }

    public <T> T getService(Class<T> clazz) {
        return getService(null, clazz);
    }

    public <T> T getService(Class<T> clazz, int timeout) {
        return getService(null, clazz, timeout);
    }

    protected boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

    private Object getRemoteProxy(String mapping, Class<?> clazz, int timeout) {
        if (mapping == null) {
            mapping = this.getServiceMapping(clazz);
        }

        List<RequestInterceptor> requestInterceptorInstances = this.buildRequestInterceptorInstance(requestInterceptorTypes);
        RestfulJsonProxy proxy = new SwiftRestfulJsonProxy(url, mapping.toLowerCase(), timeout, requestInterceptorInstances);
        proxy.setApiClass(clazz);
        if (!isBlank(httpProxyAddress)) {
            proxy.setHttpProxyAddress(httpProxyAddress);
        }
        if (!isBlank(httpProxyAuthorization)) {
            proxy.setHttpProxyAuthorization(httpProxyAuthorization);
        }

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return Proxy.newProxyInstance(loader, new Class[]{clazz}, proxy);
    }

    private String getServiceMapping(Class<?> clazz) {
        // ex: com.talkyun.game.UserService => game/UserService
        String[] parts = clazz.getName().split("\\.");
        if (parts.length > 1) {
            return parts[parts.length - 2] + "/" + parts[parts.length - 1];
        } else {
            return clazz.getName();
        }
    }

    private List<RequestInterceptor> buildRequestInterceptorInstance(List<Class<? extends RequestInterceptor>> requestInterceptorTypes) {
        if (requestInterceptorTypes.isEmpty()) {
            return Collections.emptyList();
        }
        List<RequestInterceptor> requestInterceptorInstances = new ArrayList<>(requestInterceptorTypes.size());
        for (Class<? extends RequestInterceptor> clazz : requestInterceptorTypes) {
            try {
                requestInterceptorInstances.add(clazz.newInstance());
            } catch (Throwable t) {
                LOGGER.warn("initial openx client request interceptor instance failed: clazz={}, error=", clazz, t);
            }
        }
        return requestInterceptorInstances;
    }

}