package next.fire.boot.swift.openx.client.autoconfigure;

import next.fire.boot.swift.openx.client.interceptor.RequestInterceptor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by daibing on 2021/7/6.
 */
@Component
@ConfigurationProperties(prefix = "openx.client")
public class OpenxClientProperties {
    public static final String CLIENT_CONF_PREFIX = "openx.client";
    private int timeout = 3000;
    private Class<? extends RequestInterceptor> requestInterceptorType;
    private List<ClientBeanConfig> clientBeanConfigs;
    /**
     * 注意：客户端对象类型引用清单配置仅供代码关联（受限于idea限制，配置列表对象内部的Class参数不能点击跳转），不做实际作用！
     */
    private List<Class<?>> clientBeanTypeRefList;

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public Class<? extends RequestInterceptor> getRequestInterceptorType() {
        return requestInterceptorType;
    }

    public void setRequestInterceptorType(Class<? extends RequestInterceptor> requestInterceptorType) {
        this.requestInterceptorType = requestInterceptorType;
    }

    public List<ClientBeanConfig> getClientBeanConfigs() {
        return clientBeanConfigs;
    }

    public void setClientBeanConfigs(List<ClientBeanConfig> clientBeanConfigs) {
        this.clientBeanConfigs = clientBeanConfigs;
    }

    public List<Class<?>> getClientBeanTypeRefList() {
        return clientBeanTypeRefList;
    }

    public void setClientBeanTypeRefList(List<Class<?>> clientBeanTypeRefList) {
        this.clientBeanTypeRefList = clientBeanTypeRefList;
    }

    public static class ClientBeanConfig {
        private Class<?> beanType;
        private String mapping;
        private String url;
        private Class<? extends RequestInterceptor> requestInterceptorType;

        public Class<?> getBeanType() {
            return beanType;
        }

        public void setBeanType(Class<?> beanType) {
            this.beanType = beanType;
        }

        public String getMapping() {
            return mapping;
        }

        public void setMapping(String mapping) {
            this.mapping = mapping;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Class<? extends RequestInterceptor> getRequestInterceptorType() {
            return requestInterceptorType;
        }

        public void setRequestInterceptorType(Class<? extends RequestInterceptor> requestInterceptorType) {
            this.requestInterceptorType = requestInterceptorType;
        }
    }
}
