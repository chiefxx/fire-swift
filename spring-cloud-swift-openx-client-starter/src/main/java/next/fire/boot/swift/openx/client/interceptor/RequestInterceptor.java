package next.fire.boot.swift.openx.client.interceptor;

import com.talkyun.openx.RestfulHeaderManager;

/**
 * Created by daibing on 2021/7/21.
 */
public interface RequestInterceptor {

    void apply(RestfulHeaderManager instance);

}
