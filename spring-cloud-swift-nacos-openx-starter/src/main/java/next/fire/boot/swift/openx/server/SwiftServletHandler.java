package next.fire.boot.swift.openx.server;

import com.talkyun.openx.RestfulHeaderManager;
import com.talkyun.openx.error.ExceptionHandler;
import com.talkyun.openx.server.AbstractHandler;
import com.talkyun.openx.server.core.*;
import com.talkyun.openx.server.impl.FastJsonInvoker;
import com.talkyun.openx.server.impl.SimpleServiceFactory;
import com.talkyun.utils.json.JSON;
import com.talkyun.utils.json.JSONObject;
import next.fire.boot.swift.openx.server.codec.SwiftJsonCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import static com.talkyun.openx.server.core.ServiceContext.REQ_HOST;
import static com.talkyun.openx.server.core.ServiceContext.REQ_IP;
import static com.talkyun.openx.server.core.ServiceResponse.Status.ERROR;
import static com.talkyun.openx.server.core.ServiceResponse.Status.NORMAL;
import static javax.servlet.http.HttpServletResponse.*;

/**
 * SwiftServletHandler的来源：
 * 1、主体从RegistryableServletHandler拷贝
 * 2、参考ServletHandler使用ServiceFactory和FastJsonInvoker，以及基于JsonCodec增强的SwiftJsonCodec
 * 3、增加了透传header的参数可配置
 * 4、将部分保护级别调整为protected，方便protobuf沿用
 */
public class SwiftServletHandler extends AbstractHandler {
    private static final Logger logger = LoggerFactory.getLogger(SwiftServletHandler.class);
    protected static final ExceptionHandler handler = new ExceptionHandler();
    protected static final String CHARSET = "utf-8";
    protected final String context;
    protected final SwiftJsonCodec codec;
    protected final ServiceInvoker invoker;
    protected final List<String> throughHeaderPrefixList;

    public SwiftServletHandler(ServletContext sc, List<String> throughHeaderPrefixList) {
        ServiceFactory factory = SimpleServiceFactory.getInstance();
        context = sc.getContextPath();
        codec = new SwiftJsonCodec(context, factory);
        invoker = new FastJsonInvoker(factory);
        this.throughHeaderPrefixList = throughHeaderPrefixList;
        logger.info("Create swift ServletHandler: {} - {} ", this.getClass().getSimpleName(), isBlank(context) ? "/" : context);
    }

    public void handle(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // 0. cors first
        String method = req.getMethod().toLowerCase();
        this.doCors(req, resp);
        if ("options".equals(method)) {
            return;
        }

        // 1. servlet handle
        String uri = this.getLastURI(req.getRequestURI(), context);
        String data = this.doRead(req);
        String session = req.getSession().getId();
        //   - ready service request
        ServiceRequest sr = codec.decode(uri, data, session);

        //   1.1 - not found service impl, response 404
        if (sr == null) {
            logger.warn("Not found rest request! {} ", uri);
            this.doWrite(resp, SC_NOT_FOUND, "{\"message\":\"" + uri + " not found!\"}");
            return;
        }
        //   - ready service context
        String host = this.getRemoteHost(req);
        String reqIp = this.getRemoteAddress(req);
        ServiceContext ctx = ServiceContext.getContext(session);
        ctx.put(REQ_IP, reqIp);
        ctx.put(REQ_HOST, host);
        //   - set request to ctx
        ctx.setServiceRequest(sr);

        // ADD header through
        this.doHeaderThrough(req, this.throughHeaderPrefixList);

        // 2. service invoker
        ServiceResponse sp = this.invoke(ctx, sr);
        ctx.clean();

        // clean local header
        RestfulHeaderManager.get().getOnceHead().clear();

        // 3. servlet response
        this.doResponse(resp, sp);
    }

    protected ServiceResponse invoke(ServiceContext ctx, ServiceRequest sr) {
        try {
            return invoker.invoke(ctx, sr);
        } catch (Throwable t) {
            // handle throwable, wrap to response
            if (t instanceof ServiceException) {
                t = t.getCause(); // get raw throwable
            } else if (t instanceof InterceptorException && t.getCause() != null) {
                t = t.getCause();
            }
            return new ServiceResponse(sr.getSession(), ERROR, handler.encode(t));
        }
    }

    protected void doCors(HttpServletRequest req, HttpServletResponse resp) {
        String origin = req.getHeader("Origin");
        if (null == origin || "null".equalsIgnoreCase(origin)) {
            resp.addHeader("Access-Control-Allow-Origin", "*");
        } else {
            resp.setHeader("Access-Control-Allow-Origin", origin);
        }
        resp.addHeader("Access-Control-Manager", "GateWay-1");
        resp.addHeader("Access-Control-Allow-Methods", "GET,POST");
        resp.addHeader("Access-Control-Allow-Headers", "Set-Cookie,X-Requested-With,Content-Type,X-Next-Token,x-sc-accesstoken,sw8");
        resp.addHeader("Access-Control-Allow-Credentials", "true");
    }

    protected void doResponse(HttpServletResponse resp, ServiceResponse sp) throws IOException {
        String json = sp.getResult();
        if (sp.getStatus() == NORMAL) {
            this.doWrite(resp, SC_OK, json);
        } else {
            this.doWrite(resp, SC_INTERNAL_SERVER_ERROR, json);
        }
    }

    protected void doWrite(HttpServletResponse resp, int code, String json) throws IOException {
        resp.setStatus(code);
        resp.setCharacterEncoding(CHARSET);
        resp.setContentType("application/json");
        resp.setHeader("Cache-Control", "no-cache");
        resp.getWriter().print(json);
    }

    protected String doRead(HttpServletRequest req) throws IOException {
        if ("GET".equalsIgnoreCase(req.getMethod())) {
            // read from get url
            String data = req.getQueryString();
            if (!super.isBlank(data)) {
                return this.doReadQueryString(data);
            }
            return data;
        } else {
            // read from post stream
            return super.read(req.getInputStream(), CHARSET);
        }
    }

    private String doReadQueryString(String qs) throws UnsupportedEncodingException {
        JSONObject json = JSON.parseObject("{}");

        String[] parts = qs.split("&");
        for (String part : parts) {
            if (super.isBlank(part)) {
                continue;
            }
            String[] kv = part.split("=");
            if (kv.length != 2) {
                continue;
            }
            if (!json.exists(kv[0].trim())) {
                json.put(kv[0].trim(), URLDecoder.decode(kv[1], CHARSET).trim());
            }
        }

        return json.toString();
    }

    protected void doHeaderThrough(HttpServletRequest req, List<String> throughHeaderPrefixList) throws IOException {
        Enumeration<String> hns = req.getHeaderNames();
        if (hns == null || !hns.hasMoreElements()) {
            return;
        }

        Map<String, String> throughHeader = RestfulHeaderManager.get().getOnceHead();
        while (hns.hasMoreElements()) {
            String name = hns.nextElement();
            if (isBlank(name)) {
                continue;
            }

            String head = name.trim().toLowerCase();
            if (!this.matchHeaderPrefix(head, throughHeaderPrefixList)) {
                continue;
            }

            String value = req.getHeader(name);
            if (isBlank(value)) {
                continue;
            }

            throughHeader.put(name.trim(), value.trim());
        }
    }

    private boolean matchHeaderPrefix(String head, List<String> throughHeaderPrefixList) {
        for (String headerPrefix : throughHeaderPrefixList) {
            if (head.startsWith(headerPrefix)) {
                return true;
            }
        }
        return false;
    }

    protected String getRemoteHost(HttpServletRequest req) {
        String host = req.getHeader("Host");
        if (!super.isBlank(host)) {
            return host;
        }
        return "-";
    }

    protected String getRemoteAddress(HttpServletRequest req) {
        String ip = req.getHeader("X-Forwarded-For");
        // proxy mode (slb|nginx)
        if (!isBlank(ip)) {
            int pos = ip.indexOf(",");
            if (pos != -1) {
                return ip.substring(0, pos).trim();
            } else {
                return ip.trim();
            }
        }
        // not proxy mode
        return req.getRemoteAddr();
    }
}
