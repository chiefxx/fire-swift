package next.fire.boot.swift.openx.server.helper;


public class StringUtils {

    public static String checkAndAppendPath(String path,String api){
        if (path == null){
            path = "";
        }
        if (org.springframework.util.StringUtils.isEmpty(api)){
            return path;
        }
        if (!api.endsWith(";")){
            api=api+";";
        }
        if (!path.contains(api)){
            path = api+path;
        }
        return path;
    }
}
