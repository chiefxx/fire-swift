package next.fire.boot.swift.openx.server.helper;

import com.alibaba.druid.util.JdbcUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by daibing on 2019/3/20.
 */
public class DruidJdbcExecutor {
    private static final ObjectMapper mapper = JsonMapper.builder()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .serializationInclusion(JsonInclude.Include.NON_NULL)
            .enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
            .build();

    public static List<Map<String, Object>> select(DataSource ds, String sql, Object... params) {
        try {
            return JdbcUtils.executeQuery(ds, sql, params);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T selectOne(DataSource ds, String sql, Class<T> clazz, Object... params) {
        List<Map<String, Object>> rows = select(ds, sql, params);
        if (rows.size() > 1) {
            throw new RuntimeException("selectOne return 2 or more row: " + Arrays.toString(params));
        }
        if (rows.size() == 0) {
            return null;
        }
        return mapper.convertValue(rows.get(0), clazz);
    }

    public static <T> List<T> select(DataSource ds, String sql, Class<T> clazz, Object... params) {
        List<Map<String, Object>> rows = select(ds, sql, params);
        if (rows.size() == 0) {
            return Collections.emptyList();
        }
        return Lists.transform(rows, new Function<Map<String, Object>, T>() {
            @Override
            public T apply(Map<String, Object> input) {
                return mapper.convertValue(input, clazz);
            }
        });
    }

    public static int count(DataSource ds, String sql, Object... params) {
        List<Map<String, Object>> rows = select(ds, sql, params);
        if (rows.size() != 1) {
            throw new RuntimeException("count query rows is not 1, but " + rows.size());
        }
        Map<String, Object> row = rows.get(0);
        List<String> columns = new ArrayList<>(row.keySet());
        if (columns.size() != 1) {
            throw new RuntimeException("count query columns is not 1, but " + columns.size());
        }
        return Integer.parseInt(row.get(columns.get(0)).toString());
    }

    public static int insert(DataSource ds, String table, Map<String, Object> data) {
        try {
            JdbcUtils.insertToTable(ds, table, data);
            // temp value due to druid JdbcUtils does not return value
            return 1;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static int update(DataSource ds, String sql, Object... params) {
        try {
            return JdbcUtils.executeUpdate(ds, sql, params);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static int delete(DataSource ds, String sql, Object... params) {
        List<String> words = Splitter.on(" ").trimResults().splitToList(sql.toUpperCase());
        if (!words.containsAll(Arrays.asList("DELETE", "FROM", "WHERE"))) {
            throw new RuntimeException("sql is not safe: " + sql);
        }
        return update(ds, sql, params);
    }

    /**
     * 注意调用方负责数据批次大小，建议每个批次限制在200条数据
     * 建议在jdbc.url设置参数：rewriteBatchedStatements=true (启动批处理操作)
     * jdbc:mysql://127.0.0.1:3306/yf_mpt?rewriteBatchedStatements=true
     */
    public static int insertList(DataSource ds, String table, List<String> columns, List<List<Object>> rowList) {
        String sql = JdbcUtils.makeInsertToTableSql(table, columns);
        int[] counts = doCommit(ds, sql, rowList);
        return total(counts);
    }

    public static int[] updateList(DataSource ds, String sql, List<List<Object>> paramsList) {
        return doCommit(ds, sql, paramsList);
    }

    private static int[] doCommit(DataSource ds, String sql, List<List<Object>> paramsList) {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            int[] counts = doSave(conn, sql, paramsList);
            conn.commit();
            conn.setAutoCommit(true);
            return counts;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            JdbcUtils.close(conn);
        }
    }

    private static int[] doSave(Connection conn, String sql, List<List<Object>> paramsList) throws SQLException {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement(sql);
            for (List<Object> params : paramsList) {
                setParameters(stmt, params);
                stmt.addBatch();
            }
            return stmt.executeBatch();
        } finally {
            JdbcUtils.close(stmt);
        }
    }

    private static void setParameters(PreparedStatement stmt, List<Object> params) throws SQLException {
        for (int i = 0, size = params.size(); i < size; ++i) {
            Object param = params.get(i);
            stmt.setObject(i + 1, param);
        }
    }

    private static int total(int[] counts) {
        int total = 0;
        for (int count : counts) {
            total += count;
        }
        return total;
    }
}
