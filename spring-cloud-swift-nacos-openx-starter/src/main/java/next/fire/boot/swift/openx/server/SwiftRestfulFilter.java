package next.fire.boot.swift.openx.server;

import com.talkyun.openx.ocean.base.BaseHttpFilter;
import next.fire.boot.swift.openx.server.autoconfigure.OpenxProperties;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * SwiftRestfulFilter的来源：
 * 1、参考Openx-Server-2.6.5的OceanRestfulFilter直接继承BaseHttpFilter
 */
public class SwiftRestfulFilter extends BaseHttpFilter {
    private final OpenxProperties properties;
    private SwiftServletHandler handler;

    public SwiftRestfulFilter(OpenxProperties properties) {
        this.properties = properties;
    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
        super.init(fc);
        handler = new SwiftServletHandler(fc.getServletContext(), Arrays.asList(properties.getThroughHeaderPrefixes()));
        logger.info("-> openx {} is ready.", version);
        System.out.println("-------------------------------------------------------------------------------\n");
    }

    @Override
    protected void doRestFilter(ServletRequest req, ServletResponse resp, FilterChain fc) throws IOException {
        handler.handle((HttpServletRequest) req, (HttpServletResponse) resp);
    }
}
