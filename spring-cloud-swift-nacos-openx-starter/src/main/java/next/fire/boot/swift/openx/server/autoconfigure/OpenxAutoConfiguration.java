package next.fire.boot.swift.openx.server.autoconfigure;

import com.talkyun.openx.RestfulScanner;
import com.talkyun.openx.server.core.AbstractInterceptor;
import com.talkyun.openx.server.core.Interceptor;
import next.fire.boot.swift.openx.server.NacosConf;
import next.fire.boot.swift.openx.server.SwiftRestfulFilter;
import next.fire.boot.swift.openx.server.SwiftRestfulFilterV2;
import next.fire.boot.swift.openx.server.helper.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.servlet.Filter;
import javax.servlet.Servlet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@EnableConfigurationProperties(OpenxProperties.class)
@ConditionalOnClass({Servlet.class, OpenxProperties.class})
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnProperty(prefix = "openx.server", name = "enabled", matchIfMissing = true)
public class OpenxAutoConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenxAutoConfiguration.class);
    private final OpenxProperties openxProperties;
    private final List<Interceptor> interceptors;

    public OpenxAutoConfiguration(OpenxProperties openxProperties, List<Interceptor> interceptors) {
        this.openxProperties = openxProperties;
        this.interceptors = interceptors;
    }

    @Bean
    public RestfulScanner restfulScanner() {
        String path = openxProperties.getPath();
        path = StringUtils.checkAndAppendPath(path, "com.talkyun.**.service.*;");
        path = StringUtils.checkAndAppendPath(path, "com.talkyun.**.api.*;");
        List<String> existInterceptorNameList = this.existInterceptorName(this.interceptors);
        if (openxProperties.getInterceptorTypes() != null) {
            for (Class<? extends Interceptor> interceptorType : openxProperties.getInterceptorTypes()) {
                if (existInterceptorNameList.contains(interceptorType.getName())) {
                    continue;
                }
                try {
                    Interceptor instance = interceptorType.newInstance();
                    if (instance instanceof AbstractInterceptor && interceptorType.getName().startsWith("com.talkyun.openx.interceptor")) {
                        ((AbstractInterceptor) instance).setMatchPattern(Arrays.asList("*.*"));
                    }
                    this.interceptors.add(instance);
                } catch (Throwable t) {
                    LOGGER.warn("initial openx server interceptor instance failed: clazz={}, error=", interceptorType, t);
                }
            }
        }
        RestfulScanner scanner = new RestfulScanner();
        scanner.setPath(path);
        scanner.setBeanNamePrefix(openxProperties.getBeanNamePrefix());
        scanner.setInterceptor(interceptors);
        return scanner;
    }

    @Bean
    public FilterRegistrationBean<Filter> openxFilterRegistration() {
        Filter filter = openxProperties.getEnableSwiftRestfulFilter() ? new SwiftRestfulFilter(openxProperties) : new SwiftRestfulFilterV2(openxProperties);
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>();
        registration.setFilter(filter);
        registration.addUrlPatterns(openxProperties.getUrlPatterns());
        registration.addInitParameter("openxSessionTimeout", openxProperties.getSessionTimeout());
        registration.addInitParameter("excludeURI", openxProperties.getExcludeUri());
        OpenxProperties.CompressionConfig compressionConfig = openxProperties.getCompression();
        if (compressionConfig != null && compressionConfig.isEnabled()) {
            registration.addInitParameter("compressionEnable", Boolean.toString(compressionConfig.isEnabled()));
            registration.addInitParameter("compressionSize", String.valueOf(compressionConfig.getMinResponseSize()));
        }
        registration.setName("OpenxFilter");
        registration.setOrder(Ordered.LOWEST_PRECEDENCE);
        return registration;
    }

    @Bean(name = "nacos", initMethod = "init")
    public NacosConf nacosConf() {
        return new NacosConf();
    }

    private List<String> existInterceptorName(List<Interceptor> interceptors) {
        List<String> existInterceptorNameList = new ArrayList<>(interceptors.size());
        for (Interceptor interceptor : interceptors) {
            existInterceptorNameList.add(interceptor.getClass().getName());
        }
        return existInterceptorNameList;
    }

    private boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }
}
