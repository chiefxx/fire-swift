package next.fire.boot.swift.openx.server.autoconfigure;

import com.talkyun.openx.server.core.Interceptor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "openx.server")
public class OpenxProperties {

    private Boolean enabled;

    private String path;

    private String beanNamePrefix = "/";

    private String urlPatterns = "/*";

    private String sessionTimeout = "300";

    private String excludeUri = "/*Servlet";

    private CompressionConfig compression = new CompressionConfig();

    private List<Class<? extends Interceptor>> interceptorTypes;

    private String[] throughHeaderPrefixes = new String[]{"x-"};

    private Boolean enableSwiftRestfulFilter = false;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getBeanNamePrefix() {
        return beanNamePrefix;
    }

    public void setBeanNamePrefix(String beanNamePrefix) {
        this.beanNamePrefix = beanNamePrefix;
    }

    public String getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(String urlPatterns) {
        this.urlPatterns = urlPatterns;
    }

    public String getSessionTimeout() {
        return sessionTimeout;
    }

    public void setSessionTimeout(String sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public String getExcludeUri() {
        return excludeUri;
    }

    public void setExcludeUri(String excludeUri) {
        this.excludeUri = excludeUri;
    }

    public CompressionConfig getCompression() {
        return compression;
    }

    public void setCompression(CompressionConfig compression) {
        this.compression = compression;
    }

    public List<Class<? extends Interceptor>> getInterceptorTypes() {
        return interceptorTypes;
    }

    public void setInterceptorTypes(List<Class<? extends Interceptor>> interceptorTypes) {
        this.interceptorTypes = interceptorTypes;
    }

    public String[] getThroughHeaderPrefixes() {
        return throughHeaderPrefixes;
    }

    public void setThroughHeaderPrefixes(String[] throughHeaderPrefixes) {
        this.throughHeaderPrefixes = throughHeaderPrefixes;
    }

    public Boolean getEnableSwiftRestfulFilter() {
        return enableSwiftRestfulFilter;
    }

    public void setEnableSwiftRestfulFilter(Boolean enableSwiftRestfulFilter) {
        this.enableSwiftRestfulFilter = enableSwiftRestfulFilter;
    }

    public static class CompressionConfig {
        private boolean enabled = false;
        private Long minResponseSize = 500*1024L;

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public Long getMinResponseSize() {
            return minResponseSize;
        }

        public void setMinResponseSize(Long minResponseSize) {
            this.minResponseSize = minResponseSize;
        }
    }

}
