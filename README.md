#swift starter合集 fire-swift

-------
spring cloud openx swift 生态

## 一、自建模块

#### 1、starter清单

| 编号 | 名称 | 功能 | 说明 |
| :----: | :----:  | :----: | :---- |
| 1 | spring-cloud-swift-nacos-openx-starter | Nacos、Openx | 引入Nacos配置中心； <br/> 整合Spring cloud基础生态； <br/> 整合Openx框架。 |
| 2 | spring-cloud-swift-elasticsearch-starter | Elasticsearch client | 整合RestHighClient；<br/>提供最佳实践。 |
| 3 | spring-cloud-swift-openx-client-starter | Openx client | 注入Openx client到spring容器；<br/> 提供请求拦截器。 |
| 4 | spring-cloud-swift-seata-starter | Seata | 引入分布式事务，支持Openx项目接入 |
| 5 | spring-cloud-swift-protobuf-starter | Protobuf | 引入protobuf+lz4，支持Openx接口 |
| 6 | spring-cloud-swift-security-starter | Security | 引入安全权限控制 |

#### 2、使用说明
```
1、【待验证】spring-cloud-swift-openx-client-starter可以支持最新oss客户端的自动注入
2、Redis使用建议：首先考虑用本地缓存Guava，其次考虑用Elasticsearch
```

#### 3、注意事项
```
1、Openx通过BeanPostProcessor+filter的自定义实现类（RestfulScanner+RestfulFilter）托管了Service的暴露和处理，
   不再经过springboot的HandlerInterceptor，也就是说不支持springboot拦截器了，需要使用Openx拦截器
2、spring-cloud-swift-openx-client-starter当前有2个问题：
   一个是@Autowired注入自动注入对象的时候会提示Could not autowire. No beans of 'OrderService' type found. 
   二个是application.yml中配置列表对象内部的Class参数不能点击跳转给查找类的引用带来麻烦，需要设置一下xxxTypeRefList
3、补充说明：Mybatis注入Dao对象没有提示错误，因为Dao上有注解@Mapper
```


## 二、三方模块

#### 1、starter清单

| 编号 | 名称 | 功能 | 说明 |
| :----: | :----:  | :----: | :---- |
| 1 | dynamic-datasource-spring-boot-starter | 多数据源 | 苞米豆出品，持续维护更新 |
| 2 | spring-cloud-starter-alibaba-seata | 分布式事务 | 阿里出品，非Openx项目就可以使用这个starter |


#### 2、使用说明
```
1、多数据源 动态数据源 主从分离 读写分离 分布式事务
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
    <version>3.3.6</version>
</dependency>
https://github.com/baomidou/dynamic-datasource-spring-boot-starter
```

```
2、 分布式事务：支持Feign、RestTemplate、Springboot的HandlerInterceptor
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
    <version>2.1.4.RELEASE</version>
</dependency>
https://search.maven.org/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-seata/2.1.4.RELEASE/jar
```

## 三、业务接入

#### 1. POM依赖
```
        <dependency>
            <groupId>next.fire.boot</groupId>
            <artifactId>spring-cloud-swift-nacos-openx-starter</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
```


## 四、业务价值

#### 1. 定义规范标准：
* Springboot项目规范标准

#### 2. 简化业务接入：
* 每个业务拿来即用

#### 3. 沉淀技术组件：
* 将各个业务开发过程中做的好的组件公共发布出来，避免重复造轮子

## 五、附录

* 发布库：[Release](https://maven.talkyun.com/nexus/content/repositories/releases/)

* 快照库：[Snapshot](https://maven.talkyun.com/nexus/content/repositories/snapshots/)

* 私服仓库地址

```
    <!-- self repo -->
    <repositories>
        <repository>
            <id>talkyun-release</id>
            <name>Talkyun Nexus Release</name>
            <url>https://maven.talkyun.com/nexus/content/repositories/releases/</url>
        </repository>
        <repository>
            <id>talkyun-snapshot</id>
            <name>Talkyun Nexus Snapshot</name>
            <url>https://maven.talkyun.com/nexus/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
        </repository>
    </repositories>

```

