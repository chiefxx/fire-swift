package next.fire.boot.swift.elasticsearch.autoconfigure;

import next.fire.boot.swift.elasticsearch.SwiftElasticSearchClient;
import next.fire.boot.swift.elasticsearch.SwiftElasticSearchDataWorker;
import next.fire.boot.swift.elasticsearch.SwiftElasticSearchIndexWorker;
import next.fire.boot.swift.elasticsearch.SwiftElasticSearchQueryWorker;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * Created by daibing on 2021/5/17.
 */

@Configuration
@EnableConfigurationProperties(ElasticsearchProperties.class)
@ConditionalOnClass({ElasticsearchProperties.class})
public class ElasticsearchAutoConfiguration {
    private final ElasticsearchProperties properties;
    private SwiftElasticSearchClient client;

    public ElasticsearchAutoConfiguration(ElasticsearchProperties properties) {
        this.properties = properties;
    }

    @Bean(name = "client", initMethod = "init", destroyMethod = "destroy")
    public SwiftElasticSearchClient elasticSearchClient() {
        client = new SwiftElasticSearchClient(properties);
        return client;
    }

    @Bean
    @DependsOn("client")
    public SwiftElasticSearchIndexWorker elasticSearchIndexWorker() {
        return new SwiftElasticSearchIndexWorker(client);
    }

    @Bean
    @DependsOn("client")
    public SwiftElasticSearchDataWorker elasticSearchDataWorker() {
        return new SwiftElasticSearchDataWorker(client);
    }

    @Bean
    @DependsOn("client")
    public SwiftElasticSearchQueryWorker elasticSearchQueryWorker() {
        return new SwiftElasticSearchQueryWorker(client);
    }

}
