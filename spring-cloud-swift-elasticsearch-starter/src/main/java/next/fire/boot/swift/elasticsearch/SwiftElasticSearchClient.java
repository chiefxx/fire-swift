package next.fire.boot.swift.elasticsearch;

import next.fire.boot.swift.elasticsearch.autoconfigure.ElasticsearchProperties;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.nio.conn.ssl.SSLIOSessionStrategy;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by daibing on 2021/5/17.
 */
public class SwiftElasticSearchClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftElasticSearchClient.class);
    private final ElasticsearchProperties properties;
    private RestHighLevelClient client;

    public SwiftElasticSearchClient(ElasticsearchProperties properties) {
        this.properties = properties;
    }

    public void init() {
        HttpHost host = new HttpHost(properties.getHostname(), properties.getPort(), properties.getScheme());
        RestClientBuilder builder = RestClient.builder(host).setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
            @Override
            public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                requestConfigBuilder.setConnectTimeout(properties.getConnectTimeout());
                requestConfigBuilder.setSocketTimeout(properties.getSocketTimeout());
                requestConfigBuilder.setConnectionRequestTimeout(properties.getConnectionRequestTimeout());
                return requestConfigBuilder;
            }
        }).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                // httpClientBuilder.disableAuthCaching();
                httpClientBuilder.setMaxConnTotal(properties.getMaxConnTotal());
                httpClientBuilder.setMaxConnPerRoute(properties.getMaxConnPerRoute());
                if (!isBlank(properties.getUserName()) && !isBlank(properties.getPassword())) {
                    BasicCredentialsProvider provider = new BasicCredentialsProvider();
                    provider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(properties.getUserName(), properties.getPassword()));
                    httpClientBuilder.setDefaultCredentialsProvider(provider);
                }
                if (properties.getScheme().equalsIgnoreCase("https")) {
                    httpClientBuilder.setSSLStrategy(buildSSLStrategy());
                }
                return httpClientBuilder;
            }
        });
        client = new RestHighLevelClient(builder);
    }

    public void destroy() {
        try {
            client.close();
        } catch (Throwable t) {
            LOGGER.warn("destroy failed: ", t);
        }
    }

    public RestHighLevelClient getClient() {
        return this.client;
    }

    public ElasticsearchProperties getProperties() {
        return properties;
    }

    private SSLIOSessionStrategy buildSSLStrategy() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                }
        };
        HostnameVerifier trustAnyHostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        };
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            return new SSLIOSessionStrategy(sc, trustAnyHostnameVerifier);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }
}
