package next.fire.boot.swift.elasticsearch;

import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by daibing on 2021/5/18.
 */
public class SwiftElasticSearchIndexWorker {
    private static final Logger LOGGER = LoggerFactory.getLogger(SwiftElasticSearchIndexWorker.class);
    private final RestHighLevelClient esClient;


    public SwiftElasticSearchIndexWorker(SwiftElasticSearchClient client) {
        this.esClient = client.getClient();
    }

    public String create(String indexName, Map<String, Object> settings, Map<String, Object> mappings, Map<String, Object> aliases) {
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        try {
            if (settings != null && settings.size() > 0) {
                request.settings(settings);
            }
            if (mappings != null && mappings.size() > 0) {
                request.mapping(mappings);
            }
            if (aliases != null && aliases.size() > 0) {
                request.aliases(aliases);
            }
            CreateIndexResponse response = esClient.indices().create(request, RequestOptions.DEFAULT);
            boolean acknowledged = response.isAcknowledged();
            if (acknowledged) {
                return indexName;
            }
            throw new RuntimeException("create index " + indexName + " failed without acknowledged.");
        } catch (Throwable t) {
            LOGGER.warn("create index failed: indexName={}, error=", indexName, t);
            throw new RuntimeException("create index " + indexName + " failed, error: ", t);
        }
    }

    public String remove(String indexName) {
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        try {
            AcknowledgedResponse response = esClient.indices().delete(request, RequestOptions.DEFAULT);
            boolean acknowledged = response.isAcknowledged();
            if (acknowledged) {
                return indexName;
            }
            throw new RuntimeException("remove index " + indexName + " failed without acknowledged.");
        } catch (Throwable t) {
            LOGGER.warn("remove index failed: indexName={}, error=", indexName, t);
            throw new RuntimeException("remove index " + indexName + " failed, error: ", t);
        }
    }

    public boolean contain(String indexName) {
        GetIndexRequest request = new GetIndexRequest(indexName);
        try {
            return esClient.indices().exists(request, RequestOptions.DEFAULT);
        } catch (Throwable t) {
            LOGGER.warn("contain index failed: indexName={}, error=", indexName, t);
            throw new RuntimeException("contain index " + indexName + " failed, error: ", t);
        }
    }

    public Map<String, MappingMetaData> get(String indexName) {
        GetIndexRequest request = new GetIndexRequest(indexName);
        try {
            GetIndexResponse response = esClient.indices().get(request, RequestOptions.DEFAULT);
            return response.getMappings();
        } catch (Throwable t) {
            LOGGER.warn("get index failed: indexName={}, error=", indexName, t);
            throw new RuntimeException("get index " + indexName + " failed, error: ", t);
        }
    }

    public String alias(String indexName, String aliasName) {
        IndicesAliasesRequest request = new IndicesAliasesRequest();
        try {
            request.addAliasAction(new IndicesAliasesRequest.AliasActions(IndicesAliasesRequest.AliasActions.Type.ADD)
                    .index(indexName)
                    .alias(aliasName));
            AcknowledgedResponse response = esClient.indices().updateAliases(request, RequestOptions.DEFAULT);
            boolean acknowledged = response.isAcknowledged();
            if (acknowledged) {
                return indexName;
            }
            throw new RuntimeException("alias index " + indexName + " to " + aliasName + " failed without acknowledged.");
        } catch (Throwable t) {
            LOGGER.warn("alias index failed: indexName={}, aliasName={}, error=", indexName, aliasName, t);
            throw new RuntimeException("alias index " + indexName + " to " + aliasName + " failed, error: ", t);
        }
    }
}
